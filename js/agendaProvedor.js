$(document).ready(function () {
    datosCitas();
});

function datosCitas() {
    var idUser = localStorage.getItem('id');
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/providerappointments/' + idUser,
        type: 'GET',
        success: function (respuesta) {
            var events = [];
            respuesta.data.forEach(item => {
                
                events.push({
                    title: item.service.name,
                    start: item.from_date,
                    end: item.due_date,
                    url: "detalle-proveedor.html?idservice=" + item.service._id
                });
            });
            var token = localStorage.getItem('token');
            $.ajax({
                url: 'https://www.concita.com.mx:3000/api/v1/events',
                headers: { 'Authorization': token },
                type: 'GET',
                success: function (respuesta2) {
                    var provider = localStorage.getItem('id');
                    respuesta2.data.forEach(item2 => {
                        if (provider == item2.provider) {
                            events.push({
                                title: item2.name,
                                start: item2.from_date,
                                end: item2.due_date
                            });   
                        }
                    });
                    initCalendar(events);
                },
            });
        },
    });
}

function initCalendar(eventos) {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        height: '100%',
        expandRows: true,
        slotMinTime: '08:00',
        slotMaxTime: '20:00',
        locale: 'es',
        headerToolbar: {
            left: "dayGridMonth,dayGridWeek,dayGridDay",
            center: "title",
            right: "today prev,next"
        },
        buttonText: {
            today: 'Hoy',
            month: 'Mes',
            week: 'Semana',
            day: 'Día'
        },
        initialView: 'dayGridMonth',
        initialDate: Date.now(),
        navLinks: true,
        editable: true,
        selectable: true,
        nowIndicator: true,
        dayMaxEvents: true,
        displayEventEnd: true,
        events: eventos,
        eventClick: function informacion(info) {
            
        }
    });
    calendar.render();

};

$(".fc-button-group .fc-month-button").hm("Hola");
