var servicio = "";
var availableSpace = 0;
var arregloServicios = [];
var tipo = "";
var servicioDias = "";
var idServicio = "";
var costo = "";

var infoService = {};
var horario = [];
var fechaSeleccionada = new Date();
var fechaHoy = new Date();
var horaActual = 0;
var minutoActual = 0;
var tiempoActual = 0;
var cat_auto = false;
var cat_otra = false;
let availableArray;

$(document).ready(function () {
  horaActual = new Date().getHours();
  minutoActual = new Date().getMinutes() / 60;
  tiempoActual = horaActual + minutoActual;
  fechaSeleccionada.setHours(0, 0, 0);
  fechaHoy.setHours(0, 0, 0);
  datosCitas();
  //initializeCalendar();
  getInfoCita();
});
let monthsArray = [
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Noviembre",
  "Diciembre",
];
let actualYear;
let actualMonth;
let selectedYear;
let selectedMonth;
function getInfoCita(){
  let urlParams = new URLSearchParams(window.location.search);
  let idCita = urlParams.get("idCita");
  idCita=idCita.replace('blue', '');
  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/appointmentdetail/"+idCita,
    type: "GET",
    headers: {
      Authorization: localStorage.getItem("token"),
    },
    success: function (respuesta) {
      console.log(respuesta);
      let schedule = moment(respuesta.data.from_date).tz("America/Mexico_City")
      let hoursAvailableToReschedule = respuesta.data.timeAllowedToReschedule || 0;
      if (!validarHoraPermitidaParaReagendar(schedule, hoursAvailableToReschedule)) {
        location.href = 'services.html'
      }
      $("#reason").val(respuesta.data.reason);
      $("#plate").val(respuesta.data.plate);
      $("#mileage").val(respuesta.data.mileage);
      $("#model").val(respuesta.data.model);
      $("#year").val(respuesta.data.year);
      $("#phoneInput").val(respuesta.data.clientNumber);
      let fecha=respuesta.data.from_date;
      fecha=fecha.split("T");
      let date=new Date(fecha[0].split("-")[0],parseInt(fecha[0].split("-")[1])-1,fecha[0].split("-")[2]);
      fechaSeleccionada=date;
      initializeCalendar(date);
      
      //$("#selectedHour").val(fecha[1].split(":")[0]);
    },
    error: function (er) {
      ////
      var msg = "";
      if (er.responseJSON.message === "space.full") {
       
      } else {
        msg = er.responseJSON.message;
      }
      Swal.fire({
        icon: "error",
        text: msg,
      });
    },
  });
}
function prevYear() {
  selectedYear = selectedYear - 1;
  drawCalendar();
}

function nextYear() {
  selectedYear = selectedYear + 1;
  drawCalendar();
}
function nextMonth() {
  if (selectedMonth < 11) {
    selectedMonth = selectedMonth + 1;
  } else {
    selectedMonth = 0;
    selectedYear++;
  }
  drawCalendar();
}
function prevMonth(changeYear = false) {
  if (selectedMonth > 0) {
    selectedMonth = selectedMonth - 1;
  } else {
    selectedMonth = 11;
    selectedYear--;
  }
  drawCalendar();
}
function drawCalendar(changeCalendar = true) {
  //alert("Aqui");
  $(".datepicker-switch").hide();
  $(".next").hide();
  $(".prev").hide();
  $("#prevYear")
    .empty()
    .append(selectedYear - 1);
  $("#nextYear")
    .empty()
    .append(selectedYear + 1);
  $("#actualYear").empty().append(selectedYear);
  if (selectedMonth > 0) {
    $("#prevMonth")
      .empty()
      .append(monthsArray[selectedMonth - 1]);
  } else {
    $("#prevMonth").empty().append(monthsArray[11]);
  }
  if (selectedMonth < 11) {
    $("#nextMonth")
      .empty()
      .append(monthsArray[selectedMonth + 1]);
  } else {
    $("#nextMonth").empty().append(monthsArray[0]);
  }
  $("#actualMonth").empty().append(monthsArray[selectedMonth]);
  if (selectedYear <= actualYear) {
    $("#prevYear").prop("disabled", true);
  } else {
    $("#prevYear").prop("disabled", false);
  }
  if (selectedMonth <= actualMonth && selectedYear <= actualYear) {
    $("#prevMonth").prop("disabled", true);
  } else {
    $("#prevMonth").prop("disabled", false);
  }
  if (changeCalendar) {
    let dateTemp = new Date(
      selectedYear +
        "-" +
        (selectedMonth + 1) +
        "-" +
        fechaSeleccionada.getDate()
    );
    if (dateTemp > fechaHoy) {
      $("#calendar-date-service").datepicker("setDate", dateTemp);
    } else {
      $("#calendar-date-service").datepicker("setDate", fechaHoy);
    }
    fechaSeleccionada = new Date(
      $("#calendar-date-service").datepicker("getDate")
    );
    opcionesSelectHorario();
    $("#apointment-btn").hide();
    $("#error-message-select").show();
  }
}
function initializeCalendar(fecha=null) {
  $(".datepicker-switch").hide();
  let date = new Date();
  if(fecha!=null){
    date=fecha;
  }
  actualYear = date.getFullYear();
  actualMonth = date.getMonth();
  selectedYear = actualYear;
  selectedMonth = actualMonth;
  drawCalendar();
}

function datosCitas() {
  let urlParams = new URLSearchParams(window.location.search);
  let idService = urlParams.get("idservice");
  var servicioDomicilio = "";
  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/service/" + idService,
    type: "GET",
    success: function (respuesta) {
      ////
      infoService = ajustarHorarioXDia(respuesta.data);
      infoService.categories.forEach((categories) => {
        if (categories === "Servicio de mantenimiento automotriz") {
          cat_auto = true;
        }
        if (categories === "Nissan Guadalajara") {
          cat_otra = true;
        }
      });
      availableSpace = respuesta.data.space;

      ////
      ////

      if (cat_auto || cat_otra) {
        if (!cat_auto) {
          $("#form-auto").empty();
        }
        if (!cat_otra) {
          $("#form-nissan").empty();
        }
      } else {
        $("#form-extra").empty();
      }
      opcionesSelectHorario();

      document.getElementById("imagenServicio").src =
        "https://www.concita.com.mx:3000/api/v1/image?id=" +
        respuesta.data.image;
      $("#pnombreProvedor").text(respuesta.data.provider.data.name);
      $("#propietario").text(respuesta.data.lender);
      $("#nombreServicio").text(respuesta.data.name);
      $("#unicacion").text(respuesta.data.provider.data.address);
      $("#phone").text(respuesta.data.provider.data.phone);
      $("#categoria").text(respuesta.data.categories);
      $("#descripcionServicio").text(respuesta.data.description);

      if (respuesta.data.home_service == false) {
        servicioDomicilio = "No";
      } else {
        servicioDomicilio = "Si";
      }
      $("#domicilio").text(servicioDomicilio);

      var tiempo = 0;
      var duracion = "";
      if (parseInt(respuesta.data.duration) % 60 == 0) {
        tiempo = parseInt(respuesta.data.duration) / 60;
        if (tiempo != 1) {
          duracion = tiempo + " horas";
        } else {
          duracion = tiempo + " hora";
        }
      } else {
        tiempo = parseInt(respuesta.data.duration) % 60;
        duracion = tiempo + " minutos";
      }
      $("#duracionServicio").text(duracion);

      if (respuesta.data.priceType == "quotation") {
        costo = "Sujeto a cotización.";
      }
      if (respuesta.data.priceType == "free") {
        costo = "Gratuito.";
      }
      if (respuesta.data.priceType == "price") {
        costo = "$" + respuesta.data.cost + ".00";
      }
      availableArray=respuesta.data.available;
      respuesta.data.available.forEach((element) => {
        let days = "";
        element.days.forEach((dias) => {
          if (days != "") {
            days += ", ";
          }
          days += dias;
        });
        let horaInicio = element.start;

        let horaFin = element.end;
        if (horaInicio > 12) {
          horaInicio = horaInicio - 12 + ":00 pm";
        } else {
          horaInicio = horaInicio + ":00 am";
        }
        if (horaFin > 12) {
          horaFin = horaFin - 12 + ":00 pm";
        } else {
          horaFin = horaFin + ":00 am";
        }
        $("#horarioServicio").append(days + " " + horaInicio + " - " + horaFin);
        $("#horarioServicio").each(function () {
                    var text = $(this).text();
                    text = text.replace("Mo", "Lu");
                    text = text.replace("Tu", " Ma");
                    text = text.replace("We", " Mi");
                    text = text.replace("Th", " Ju");
                    text = text.replace("Fr", " Vi");
                    text = text.replace("Sa", " Sa");
                    text = text.replace("Su", " Do");
                    $(this).text(text);
                });
      });
      
      $("#pago").text(costo);
    },
  });
}

function crearCita() {
  var horarioSelected = $("#selectedHour").val();
  horarioSelected = horario[horarioSelected];
  var uid = localStorage.getItem("id");
  var fechaInicio = new Date(
    fechaSeleccionada.setHours(
      parseInt(horarioSelected.horas),
      parseInt(horarioSelected.mins),
      0
    )
  );
  var format = 'YYYY/MM/DD HH:mm:ss ZZ';
  //fechaInicio=moment(fechaInicio, format).tz("America/Los_Angeles").format(format);
  var fechaFin = new Date(
    fechaSeleccionada.setHours(
      parseInt(horarioSelected.horas),
      parseInt(horarioSelected.mins) + infoService.duration,
      0
    )
  );

  let jsonData = {
    client: uid,
    service: infoService._id,
    provider: infoService.provider,
    from_date: fechaInicio + "",
    due_date: fechaFin + "",
    hour: horarioSelected.horas,
    minutes: horarioSelected.mins,
  };
  ////

  var jsonUnir = $("#form-extra").serializeToJSON({
    associativeArrays: true,
    parseBooleans: true,
    parseFloat: {
      condition: undefined,
      nanToZero: true,
      getInputValue: function ($input) {
        return $input.val().split(",").join("");
      },
    },
  });
  let obj_unidos = Object.assign(jsonData, jsonUnir);
  ////
  let urlParams = new URLSearchParams(window.location.search);
  let idCita = urlParams.get("idCita");
  idCita=idCita.replace('blue', '');
  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/appointment/"+idCita,
    data: jsonData,
    type: "PUT",
    headers: {
      Authorization: localStorage.getItem("token"),
    },
    success: function (respuesta) {
      ////
      Swal.mixin({
        customClass: {
          confirmButton: "btn btn-primary",
          cancelButton: "btn btn-primary",
        },
        buttonsStyling: false,
      })
        .fire({
          icon: "success",
          title: "Cita actualizada con éxito",
          text: "Tu cita se ha actualizado correctamente.",
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText: "Ir al inicio",
          cancelButtonText: "Ver agenda",
        })
        .then((result) => {
          if (result.value) {
            location.href = "services.html";
          } else {
            location.href = "agenda.html";
          }
        });
    },
    error: function (er) {
      ////
      var msg = "";
      if (er.responseJSON.message === "space.full") {
        msg =
          "El servicio no cuenta con disponibilidad de horario. Te recomendamos seleccionar otro horario o día.";
      } else {
        msg = er.responseJSON.message;
      }
      Swal.fire({
        icon: "error",
        text: msg,
      });
    },
  });
}

function ajustarHorarioXDia(servicioAReservar) {
  var forEach = Array.prototype.forEach;
  servicioAReservar.Lunes = [];
  servicioAReservar.Martes = [];
  servicioAReservar.Miercoles = [];
  servicioAReservar.Jueves = [];
  servicioAReservar.Viernes = [];
  servicioAReservar.Sabado = [];
  servicioAReservar.Domingo = [];

  forEach.call(servicioAReservar.available, function (dispo) {
    var i;
    for (i = dispo.start; i <= dispo.end; i++) {
      var x;
      var minutos = 0;
      var diviciones = 60 / servicioAReservar.duration;
      if (i === dispo.end) {
      } else {
        for (x = 0; x <= diviciones - 1; x++) {
          forEach.call(dispo.days, function (day) {
            var minsPrecent = minutos / 60;
            var tempo = i + minsPrecent;
            if (day === "Mo") {
              servicioAReservar.Lunes.push({
                horas: i,
                mins: minutos,
                tiempo: tempo,
              });
            }
            if (day === "Tu") {
              servicioAReservar.Martes.push({
                horas: i,
                mins: minutos,
                tiempo: tempo,
              });
            }
            if (day === "We") {
              servicioAReservar.Miercoles.push({
                horas: i,
                mins: minutos,
                tiempo: tempo,
              });
            }
            if (day === "Th") {
              servicioAReservar.Jueves.push({
                horas: i,
                mins: minutos,
                tiempo: tempo,
              });
            }
            if (day === "Fr") {
              servicioAReservar.Viernes.push({
                horas: i,
                mins: minutos,
                tiempo: tempo,
              });
            }
            if (day === "Sa") {
              servicioAReservar.Sabado.push({
                horas: i,
                mins: minutos,
                tiempo: tempo,
              });
            }
            if (day === "Su") {
              servicioAReservar.Domingo.push({
                horas: i,
                mins: minutos,
                tiempo: tempo,
              });
            }
          });
          minutos += servicioAReservar.duration;
        }
      }
    }
  });
  ////
  return servicioAReservar;
}

function opcionesSelectHorario() {
  var forEach = Array.prototype.forEach;
  var opcion = fechaSeleccionada.getDay();
  ////

  horario = [];
  switch (opcion) {
    case 1:
      if (infoService.Lunes) {
        horario = infoService.Lunes;
      }
      break;
    case 2:
      if (infoService.Martes) {
        horario = infoService.Martes;
      }
      break;
    case 3:
      if (infoService.Miercoles) {
        horario = infoService.Miercoles;
      }
      break;
    case 4:
      if (infoService.Jueves) {
        horario = infoService.Jueves;
      }
      break;
    case 5:
      if (infoService.Viernes) {
        horario = infoService.Viernes;
      }
      break;
    case 6:
      if (infoService.Sabado) {
        horario = infoService.Sabado;
      }
      break;
    case 0:
      if (infoService.Domingo) {
        horario = infoService.Domingo;
      }
      break;
  }
  verDisponibilidad();
}

function pintarSelectHoras() {
  var options = `<option disabled="disabled" selected value="Selecciona un horario disponible">Selecciona un horario disponible</option>`;
  horario.forEach(function (item, index) {
    ////
    var label =
      ("0" + item.horas).slice(-2) + ":" + ("0" + item.mins).slice(-2);
    if (fechaSeleccionada + "" === fechaHoy + "") {
      if (item.tiempo > tiempoActual) {
        options += `<option value="${index}" class="ng-binding ng-scope">${label}</option>`;
      }
    } else {
      options += `<option value="${index}" class="ng-binding ng-scope">${label}</option>`;
    }
  });
  $("#selectedHour").empty().append(options);
  if ($("#selectedHour option").length > 1) {
    $("#error-message-select").text("Selecciona un horario disponible");
    $("#selectedHour").show();
    $("#form-extra").show();
  } else {
    $("#selectedHour").hide();
    $("#form-extra").hide();
    $("#error-message-select").text(
      "El servicio no cuenta con disponibilidad de horario. Te recomendamos seleccionar otro horario o día."
    );
  }
}

function verDisponibilidad() {
  var forEach = Array.prototype.forEach;

  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/events2",
    data: {},
    type: "GET",
    success: function (data) {
      ////
      var eventosBloqueo = [];
      forEach.call(data.data, function (evento) {
        if (infoService.provider._id === evento.provider) {
          eventosBloqueo.push(evento);
        }
      });
      if (eventosBloqueo.length > 0) {
        var actualDate = new Date(fechaSeleccionada);
        forEach.call(eventosBloqueo, function (bloqueo) {
          ////
          var fromBloqDate = new Date(bloqueo.from_date);
          var fromBloqDateWhitOutTime = new Date(bloqueo.from_date);
          fromBloqDateWhitOutTime.setHours(0, 0, 0, 0);
          var dueBloqDate = new Date(bloqueo.due_date);
          var dueBloqDateWhitOutTime = new Date(bloqueo.due_date);
          dueBloqDateWhitOutTime.setHours(0, 0, 0, 0);
          var dueBloqDateLess1 = new Date(bloqueo.due_date);
          dueBloqDateLess1.setDate(dueBloqDateLess1.getDate());
          if (actualDate > fromBloqDate && actualDate < dueBloqDateLess1) {
            horario = [];
          }
          if (
            actualDate.toString() === fromBloqDateWhitOutTime.toString() &&
            actualDate.toString() === dueBloqDateWhitOutTime.toString()
          ) {
            var nuevoHorario = [];
            var timefom =
              fromBloqDate.getHours() + fromBloqDate.getMinutes() / 60;
            var timedue =
              dueBloqDate.getHours() + dueBloqDate.getMinutes() / 60;
            forEach.call(horario, function (horarioCheck) {
              if (
                horarioCheck.tiempo < timefom ||
                horarioCheck.tiempo > timedue
              ) {
                nuevoHorario.push(horarioCheck);
              }
            });
            horario = nuevoHorario;
          } else {
            if (actualDate.toString() === fromBloqDateWhitOutTime.toString()) {
              var nuevoHorario = [];
              var time =
                fromBloqDate.getHours() + fromBloqDate.getMinutes() / 60;
              forEach.call(horario, function (horarioCheck) {
                if (horarioCheck.tiempo < time) {
                  nuevoHorario.push(horarioCheck);
                }
              });
              horario = nuevoHorario;
            }
            if (actualDate.toString() === dueBloqDateWhitOutTime.toString()) {
              ////

              var nuevoHorario = [];
              var time = dueBloqDate.getHours() + dueBloqDate.getMinutes() / 60;
              forEach.call(horario, function (horarioCheck) {
                ////

                if (horarioCheck.tiempo > time) {
                  nuevoHorario.push(horarioCheck);
                }
              });
              ////

              horario = nuevoHorario;
            }
          }
        });
        ////
      }
      //
      let urlParams = new URLSearchParams(window.location.search);
      let idService = urlParams.get("idservice");
      $.ajax({
        type: "GET",
        dataType: "json",
        url: "https://www.concita.com.mx:3000/api/v1/serviceapts/"+idService,
        success: function (respuestaApts) {
          filtrarCitasAgendadas(fechaSeleccionada, respuestaApts.data.docs)
          pintarSelectHoras();
        }
      })
    },
    error: function (er) {
      ////
    },
  });
}

function selectHour() {
  $("#apointment-btn").show();
  $("#error-message-select").hide();
  if (cat_auto || cat_otra) {
    $("#apointment-btn").prop("disabled", true);
  } else {
    $("#apointment-btn").prop("disabled", false);
  }
  validarCamposObligatorios();
}

function validarCamposObligatorios() {
  if (cat_auto && cat_otra) {
    if (
      $("#reason").val() != "" &&
      $("#plate").val() != "" &&
      $("#mileage").val() != "" &&
      $("#model").val() != "" &&
      $("#year").val() != "" &&
      $("#phoneInput").val() != "" &&
      $("#otraCalle").val() != "" &&
      $("#otraColonia").val() != "" &&
      $("#otraMunicipio").val() != ""
    ) {
      $("#apointment-btn").prop("disabled", false);
    } else {
      $("#apointment-btn").prop("disabled", true);
    }
  } else {
    if (cat_auto) {
      if (
        $("#reason").val() != "" &&
        $("#plate").val() != "" &&
        $("#mileage").val() != "" &&
        $("#model").val() != "" &&
        $("#year").val() != "" &&
        $("#phoneInput").val() != ""
      ) {
        $("#apointment-btn").prop("disabled", false);
      } else {
        $("#apointment-btn").prop("disabled", true);
      }
    }
    if (cat_otra) {
      if (
        $("#otraCalle").val() != "" &&
        $("#otraColonia").val() != "" &&
        $("#otraMunicipio").val() != "" &&
        $("#phoneInput").val() != ""
      ) {
        $("#apointment-btn").prop("disabled", false);
      } else {
        $("#apointment-btn").prop("disabled", true);
      }
    }
  }
}

function filtrarCitasAgendadas(fecha, arreglo) {
  var events =arreglo.filter(item => moment(item.from_date).format('YYYY-MM-DD') == moment(fecha).format('YYYY-MM-DD'));
  var events2 = _.uniqBy(events,item => item.from_date)
  var events3 = _.groupBy(events,item => item.from_date)
  var events4 = [];
  console.log(events2);
  console.log(events3);
  for (let index = 0; index < events2.length; index++) {
    const element = events2[index];
    const element2 = events3[element.from_date].length;
    if(element2 >= availableSpace){
      events4.push(events2[index]);
    }
  }
  events = events4.map(event => {
    var horas= +moment(event.from_date).format('HH');
    var min = +moment(event.from_date).format('mm');
    var minsPrecent = min / 60;
    var tiempo = horas + minsPrecent;
    return {horas: horas, mins:min,tiempo:tiempo} 
  })
  horario = compareArrays(horario,events);
}

function customComparator(a, b) {
  return _.difference(_.keys(a), _.keys(b)).length === 0 &&
         _.difference(_.values(a), _.values(b)).length === 0;
}
function compareArrays(a = [], b = []) {
  return _.differenceWith(a, b, customComparator);
}

function validarHoraPermitidaParaReagendar(from_date, hoursAvailableToReschedule = 0) {
  let schedule = from_date;
  let today = moment().tz("America/Mexico_City")
  var duration = moment.duration(schedule.diff(today));
  var hours = duration.asHours();
  if (hoursAvailableToReschedule !== 0 && hours <= hoursAvailableToReschedule) {
      return false;
  }
  return true;
}