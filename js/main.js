var app = angular.module("base", [
  "ui.router",
  "ui.bootstrap",
  "angular-img-cropper",
  "infinite-scroll",
  "blockUI",
  "ui.calendar",
  "ui.bootstrap.datetimepicker",
]);
app.constant("CONFIG", {
  env: "development",
  port: 3000,
  backend_url: "https://www.concita.com.mx:3000/api/v1/",
  client_url: "https://www.concita.com.mx:3000/api/v1/",
  mongodb_url: "mongodb://localhost:27017/concita_dev",
  free_code: "free001",
  facebookAppId: "815037555372021",
  facebookVersion: "v2.12",
  securePort: false,
  smtp_host: "server132.neubox.net",
  smtp_port: 465,
  smtp_secure: true,
  smtp_auth: {
    user: "testing@feraxinc.com",
    pass: "dft160614jk7",
  },
  openpay: {
    merchant_id: "mppjvaydveftfzpybktj",
    api_key: "pk_02742720ce58439292026bd9fecc075f",
    is_sandbox: false,
  },
});
app.constant("VERSION", {
  NO: "18.12.6.10.0.0",
});
app.config(function ($stateProvider, $urlRouterProvider, $qProvider) {
  $qProvider.errorOnUnhandledRejections(false);
  $urlRouterProvider.otherwise("/login");
  $stateProvider
    .state("login", {
      url: "/login",
      templateUrl: "views/main/login.html",
      controller: "LoginCtrl as vm",
      data: {
        requireLogin: false,
      },
    })
    .state("signin", {
      url: "/signin?name&email&membership",
      templateUrl: "views/main/signinProvider.html",
      controller: "SigninProviderC as vm",
      data: {
        requireLogin: false,
      },
    })
    .state("login-admin", {
      url: "/login-admin",
      templateUrl: "views/main/login-admin.html",
      controller: "LoginCtrl as vm",
      data: {
        requireLogin: false,
      },
    })
    .state("signin-admin", {
      url: "/signin-admin",
      templateUrl: "views/main/signinProvider-admin.html",
      controller: "SigninProviderC as vm",
      data: {
        requireLogin: false,
      },
    })
    .state("notifications", {
      url: "/notifications",
      templateUrl: "views/admin/notifications.html",
      controller: "SigninProviderC as vm",
      data: {
        requireLogin: false,
      },
    })
    .state("new-admin", {
      url: "/new-admin",
      templateUrl: "views/admin/registeradmin.html",
      controller: "listProvider as vm",
      data: {
        requireLogin: false,
      },
    })
    .state("activateclient", {
      url: "/activate",
      templateUrl: "views/main/activateClient.html",
      controller: "ClientActiveC as vm",
      data: {
        requireLogin: false,
      },
    })
    .state("success", {
      url: "/success/:id",
      templateUrl: "views/main/success.html",
      controller: "SuccessCtrl as vm",
      data: {
        requireLogin: false,
      },
    })
    .state("user", {
      url: "/user",
      templateUrl: "views/main/index.html",
      controller: "MainCtrl as mn",
      abstract: true,
      data: {
        requireLogin: true,
      },
    })
    .state("provider", {
      url: "/provider",
      templateUrl: "views/main/index.html",
      controller: "MainCtrl as mn",
      abstract: true,
      data: {
        requireLogin: true,
      },
    })
    .state("admin", {
      url: "/admin",
      templateUrl: "views/main/index.html",
      controller: "MainCtrl as mn",
      abstract: true,
      data: {
        requireLogin: true,
      },
    })
    .state("user.services", {
      url: "/services",
      views: {
        content: {
          templateUrl: "views/main/services.html",
          controller: "UserServicesC as vm",
        },
      },
      data: {
        role: "client",
      },
    })
    .state("user.profile", {
      url: "/profile",
      views: {
        content: {
          templateUrl: "views/main/profile.html",
          controller: "UserProfileC as vm",
        },
      },
      data: {
        role: "client",
      },
    })
    .state("user.schedule", {
      url: "/schedule",
      views: {
        content: {
          templateUrl: "views/main/schedule.html",
          controller: "UserScheduleC as vm",
        },
      },
      data: {
        role: "client",
      },
    })
    .state("user.appointment", {
      url: "/appointment/:serviceId",
      views: {
        content: {
          templateUrl: "views/user/appointment.html",
          controller: "UserAppointmentC as vm",
        },
      },
      data: {
        role: "client",
      },
    })
    .state("user.appointmentDetail", {
      url: "/appointmentdetail/:apptId",
      views: {
        content: {
          templateUrl: "views/main/appointmentDetail.html",
          controller: "UserAppointmentDetailC as vm",
        },
      },
      data: {
        role: "client",
      },
    })
    .state("provider.services", {
      url: "/services",
      views: {
        content: {
          templateUrl: "views/main/services.html",
          controller: "ProviderServicesC as vm",
        },
      },
      data: {
        role: "provider",
      },
    })
    .state("provider.subscribe", {
      url: "/providers/subscribe",
      views: {
        content: {
          templateUrl: "views/main/subscribe.html",
          controller: "SubscribeCtrl as vm",
        },
      },
      data: {
        requireLogin: true,
      },
    })
    .state("provider.servicesCreate", {
      url: "/services/create",
      views: {
        content: {
          templateUrl: "views/provider/servicesCreate.html",
          controller: "ProviderServicesCreateC as vm",
        },
      },
      data: {
        role: "provider",
      },
    })
    .state("provider.servicesDetail", {
      url: "/services/detail/:serviceId",
      views: {
        content: {
          templateUrl: "views/provider/servicesDetail.html",
          controller: "ProviderServicesDetailC as vm",
        },
      },
      data: {
        role: "provider",
      },
    })
    .state("provider.servicesEdit", {
      url: "/services/edit/:serviceId",
      views: {
        content: {
          templateUrl: "views/provider/servicesCreate.html",
          controller: "ProviderServicesEditC as vm",
        },
      },
    })
    .state("provider.profile", {
      url: "/profile",
      views: {
        content: {
          templateUrl: "views/main/profile.html",
          controller: "ProviderProfileC as vm",
        },
      },
      data: {
        role: "provider",
      },
    })
    .state("provider.newcreditcard", {
      url: "/profile/newcreditcard",
      views: {
        content: {
          templateUrl: "views/provider/newcreditcard.html",
          controller: "ProviderNewCardC as vm",
        },
      },
      data: {
        role: "provider",
      },
    })
    .state("provider.schedule", {
      url: "/schedule",
      views: {
        content: {
          templateUrl: "views/main/schedule.html",
          controller: "ProviderSheduleC as vm",
        },
      },
      data: {
        role: "provider",
      },
    })
    .state("provider.newEvent", {
      url: "/schedule/event",
      views: {
        content: {
          templateUrl: "views/provider/newEvent.html",
          controller: "ProviderNewEventC as vm",
        },
      },
      data: { role: "provider" },
    })
    .state("provider.newBlock", {
      url: "/schedule/bloqueo",
      views: {
        content: {
          templateUrl: "views/provider/newBlock.html",
          controller: "ProviderNewEventC as vm",
        },
      },
      data: { role: "provider" },
    })
    .state("admin.listProvider", {
      url: "/providers",
      views: {
        content: {
          templateUrl: "views/admin/listProvider.html",
          controller: "listProvider as vm",
        },
      },
      data: {
        requireLogin: false,
      },
    })
    .state("admin.listFacturas", {
      url: "/providers",
      views: {
        content: {
          templateUrl: "views/admin/listfacturas.html",
          controller: "listFacturas as vm",
        },
      },
      data: {
        requireLogin: false,
      },
    })
    .state("admin.createfactura", {
      url: "/providers",
      views: {
        content: {
          templateUrl: "views/admin/createfactura.html",
          controller: "createFacturas as vm",
        },
      },
      data: {
        requireLogin: false,
      },
    })
    .state("admin.listadmins", {
      url: "/listadmins",
      views: {
        content: {
          templateUrl: "views/admin/listadmins.html",
          controller: "listProvider as vm",
        },
      },
      data: {
        requireLogin: false,
      },
    })
    .state("admin.updatepayment", {
      url: "/payment",
      views: {
        content: {
          templateUrl: "views/admin/updatepayment.html",
          controller: "updatepayment as vm",
        },
      },
      data: {
        role: "admin",
      },
    });
});
app.run(function ($rootScope, $state, $http, AuthService, blockUI) {
  $rootScope.isLoading = function () {
    return $http.pendingRequests.length > 0;
  };
  $rootScope.$watch($rootScope.isLoading, function (v) {
    if (v) {
      blockUI.start();
    } else {
      blockUI.stop();
    }
  });
  $rootScope.$on("$stateChangeStart", function (event, toState, toStateParams) {
    var requireLogin = toState.data.requireLogin;
    var stateRole = toState.data.role;
    if (requireLogin && !AuthService.isLoggedIn()) {
      $state.go("login");
      event.preventDefault();
    }
    if (stateRole) {
      if (stateRole !== AuthService.getRole()) {
        $state.go("login");
        event.preventDefault();
      }
    }
    if (!requireLogin) {
      AuthService.logout();
    }
  });
});
app
  .controller("LoginCtrl", function (
    $scope,
    $state,
    Alert,
    AuthService,
    Admins,
    VERSION,
    $uibModal,
    RandomString,
    Store,
    UiOpenpay,
    $rootScope
  ) {
    var vm = this;
    vm.app_name = "Concita";
    vm.showLoginForm = true;
    vm.getVersion = function () {
      return VERSION.NO;
    };
    vm.closeAll = function () {
      vm.loginShow = true;
      vm.email = true;
      vm.registerShow = true;
    };
    vm.toggleLogin = function () {
      vm.registerShow = true;
      vm.email = true;
      vm.loginShow = !vm.loginShow;
      vm.isLogin = !vm.isLogin;
    };
    vm.toggleRegister = function () {
      vm.loginShow = true;
      vm.email = true;
      vm.registerShow = !vm.registerShow;
    };
    vm.toggleEmail = function () {
      vm.email = !vm.email;
      vm.loginShow = true;
      vm.registerShow = true;
    };
    vm.prueba = function (member) {
      vm.membership = member;
    };
    vm.showTerms = function () {
      $uibModal.open({
        templateUrl: "views/main/modals/terms.html",
        controller: "TermsInstance as md",
        size: "lg",
      });
    };
    vm.errors = {
      email: false,
      pass: false,
    };
    vm.onFacebookLoginClick = function (role) {
      if (role === void 0) {
        role = false;
      }
      FB.login(
        function (response) {
          if ((response.status = "connected")) {
            FB.api("/me", { fields: "id,email,name" }, function (userInfo) {
              if (!userInfo.email) {
                Alert.info(
                  "Su cuenta de Facebook no permite el acceso. Por favor regístrese y acceda mediante correo electronico"
                );
              }
              vm.userInfoFacebook = {
                id: userInfo.id,
                email: userInfo.email,
                name: userInfo.name,
              };
            });
          } else {
            Alert.info(
              "Su cuenta de Facebook no permite el acceso. Por favor regístrese y acceda mediante correo electronico"
            );
          }
        },
        {
          scope: "public_profile,email",
        }
      );
    };
    vm.checkRoleUser = function () {
      var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
      if (!testEmail.test(vm.emailReset)) {
        return Alert.error("Ingrese la dirección de correo electrónico valida");
      }
      if (vm.emailReset.length > 100) {
        return Alert.error(
          "La dirección correo electrónico excede los 100 caracteres"
        );
      }
      AuthService.checkRoleUser({ email: vm.emailReset }, function (err, resp) {
        if (err) {
          return Alert.error(
            resp.message ||
              "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
          );
        }
        if (resp.roles.length == 2) {
          Alert.confirm(
            "Atención",
            "Su correo está vinculado a más de un perfil<h5>¿Cuál desea restaurar?</h5>",
            {
              user: Alert.getBasicButton("Usuario", function () {
                AuthService.forgotPassword(
                  { role: "client", email: vm.emailReset },
                  function (err, resp) {
                    if (err) {
                      return Alert.error(
                        resp.message ||
                          "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
                      );
                    }
                    vm.pin1 = "";
                    vm.pin2 = "";
                    vm.pin3 = "";
                    vm.pin4 = "";
                    vm.confirmPin = true;
                  }
                );
              }),
              provider: Alert.getBasicButton("Proveedor", function () {
                AuthService.forgotPassword(
                  { role: "provider", email: vm.emailReset },
                  function (err, resp) {
                    if (err) {
                      return Alert.error(
                        resp.message ||
                          "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
                      );
                    }
                    vm.pin1 = "";
                    vm.pin2 = "";
                    vm.pin3 = "";
                    vm.pin4 = "";
                    vm.confirmPin = true;
                  }
                );
              }),
              cancel: Alert.getCancelButton(function () {}),
            }
          );
          vm.forgotPassword = false;
          return;
        }
        if (resp.roles.length == 1) {
          vm.forgotPassword = false;
          AuthService.forgotPassword(
            { role: resp.roles[0], email: vm.emailReset },
            function (err, resp) {
              if (err) {
                return Alert.error(
                  resp.message ||
                    "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
                );
              }
              vm.pin1 = "";
              vm.pin2 = "";
              vm.pin3 = "";
              vm.pin4 = "";
              vm.confirmPin = true;
            }
          );
          return;
        }
        Alert.error("El correo electrónico introducido no ha sido registrado.");
      });
    };
    vm.confirmCode = function () {
      var code = "" + vm.pin1 + vm.pin2 + vm.pin3 + vm.pin4;
      if (!code) {
        return Alert.error("Ingrese el código de confirmación");
      }
      if (code.length > 4) {
        return Alert.error("El código excede los 4 caracteres");
      }
      if (code.length < 4) {
        return Alert.error("El código debe tener 4 caracteres");
      }
      var testCode = /^[0-9]{4}$/i;
      if (!testCode.test(code)) {
        return Alert.error("Este código es invalido");
      }
      localStorage.clear();
      localStorage.setItem("code", code);
      AuthService.confirmCode({ code: code }, function (err, resp) {
        if (err) {
          if (resp.message == "bad.code.expired") {
            resp.message = "Código incorrecto.";
          }
          return Alert.error(
            resp.message ||
              "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
          );
        }
        vm.passReset = "";
        vm.confirmPass = "";
        vm.confirmPin = false;
        vm.resetPasswordForm = true;
      });
    };
    vm.resetPassword = function () {
      if (!vm.passReset) {
        return Alert.error("Ingrese contraseña");
      }
      if (vm.passReset.replace(" ", "").length < 8) {
        return Alert.error("La contraseña debe tener al menos 8 caracteres");
      }
      if (vm.passReset.replace(" ", "").length > 320) {
        return Alert.error("La contraseña excede los 320 caracteres");
      }
      if (vm.passReset != vm.confirmPass) {
        return Alert.error("Las contraseñas deben ser iguales");
      }
      var code = localStorage.getItem("code");
      AuthService.resetPassword(
        { password: md5(vm.passReset), code: code },
        function (err, resp) {
          if (err) {
            return Alert.error(
              resp.message ||
                "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
            );
          }
          Alert.info("Su contraseña ha sido restablecida correctamente");
          vm.resetPasswordForm = false;
        }
      );
    };
    vm.loginAdmin = function (role) {
      if (role === void 0) {
        role = false;
      }
      vm.errors.email = false;
      vm.errors.pass = false;
      var log = {
        email: vm.log.email.toLowerCase(),
        pass: md5(vm.log.pass),
      };
      var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
      if (!testEmail.test(vm.log.email)) {
        return Alert.error("Ingrese la dirección de correo electrónico valida");
      }
      if (vm.log.email.length > 100) {
        return Alert.error(
          "La dirección correo electrónico excede los 100 caracteres"
        );
      }
      if (!vm.log.pass) {
        return Alert.error("Ingrese contraseña");
      }
      if (vm.log.pass.replace(" ", "").length < 8) {
        return Alert.error("La contraseña debe tener al menos 8 caracteres");
      }
      if (vm.log.pass.replace(" ", "").length > 100) {
        return Alert.error("La contraseña excede los 100 caracteres");
      }
      if (role) log.role = role;
      AuthService.login(log, function (err, resp) {
        
        if (err) {
          if (resp.type === "inactive") {
            Alert.confirm("Atención", resp.message, {
              send: Alert.getBasicButton(
                "Enviar nuevo correo de activación",
                function () {
                  AuthService.sendMailClient({
                    email: vm.log.email.toLowerCase(),
                  });
                }
              ),
              cancel: Alert.getCancelButton(function () {}),
            });
            return;
          } else {
            return Alert.error(resp.message);
          }
        }
        vm.errors.email = true;
        vm.errors.pass = true;
        if (resp.token) {
          
          AuthService.checkRoleUser({ email: vm.log.email }, function (
            err,
            resp
          ) {
            if (err) {
              return Alert.error(
                resp.message ||
                  "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
              );
            }
            if (resp.roles) {
              
              
              resp.roles.forEach(function (value) {
                
                if (value == "admin") {
                  
                  var roles = {
                    admin: "admin.listProvider",
                  };
                  $scope.$watch("inicio", function (newValue) {
                    $rootScope.inicio = vm.log.email;
                  });
                  $state.go(roles[value]);
                  return;
                }
              });
            }
          });
        }
      });
    };
    vm.parseDate = function (str) {
      return new Date(str);
    };
    vm.datediff = function (first, second) {
      return Math.round((second - first) / (1000 * 60 * 60 * 24));
    };
    vm.login = function (role) {
      if (role === void 0) {
        role = "no";
      }
      vm.errors.email = false;
      vm.errors.pass = false;
      var log = {
        email: vm.log.email.toLowerCase(),
        pass: md5(vm.log.pass),
      };
      var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
      if (!testEmail.test(vm.log.email)) {
        return Alert.error("Ingrese la dirección de correo electrónico valida");
      }
      if (vm.log.email.length > 100) {
        return Alert.error(
          "La dirección correo electrónico excede los 100 caracteres"
        );
      }
      if (!vm.log.pass) {
        return Alert.error("Ingrese contraseña");
      }
      if (vm.log.pass.replace(" ", "").length < 8) {
        return Alert.error("La contraseña debe tener al menos 8 caracteres");
      }
      if (vm.log.pass.replace(" ", "").length > 100) {
        return Alert.error("La contraseña excede los 100 caracteres");
      }
      if (role != "no") log.role = role;
      
      
      AuthService.login(log, function (err, resp) {
        
        if (err) {
          if (resp.type === "inactive") {
            Alert.confirm("Atención", resp.message, {
              send: Alert.getBasicButton(
                "Enviar nuevo correo de activación",
                function () {
                  AuthService.sendMailClient({
                    email: vm.log.email.toLowerCase(),
                  });
                }
              ),
              cancel: Alert.getCancelButton(function () {}),
            });
            return;
          } else {
            return Alert.error(resp.message);
          }
        }
        vm.errors.email = true;
        vm.errors.pass = true;
        if (resp.token) {
          var roles_1 = {
            client: "user.services",
            provider: "provider.services",
            subscribe: "provider.subscribe",
          };
          if (resp.data.role == "provider") {
            
            
            Admins.getProvider(resp.data.id, function (err, res) {
              if (!err) {
                
                
                
                
                
                  vm.datediff(
                    vm.parseDate(resp.data.data.registration_date),
                    vm.parseDate(new Date())
                  )
                );
                if (
                  vm.datediff(
                    vm.parseDate(resp.data.data.registration_date),
                    vm.parseDate(new Date())
                  ) >= 90
                ) {
                  
                  
                  if (res.credit_card.length > 0) {
                    
                    $state.go(roles_1[AuthService.getRole()]);
                    return;
                  } else {
                    
                    $state.go(roles_1["subscribe"]);
                    return;
                  }
                } else {
                  $state.go(roles_1[AuthService.getRole()]);
                  return;
                }
              }
            });
          }
          if (resp.data.role == "client") {
            
            $state.go(roles_1[AuthService.getRole()]);
            return;
          }
        } else {
          Alert.confirm(
            "Atención",
            "Su correo está vinculado a más de un perfil<h5>¿Cuál desea utilizar?</h5>",
            {
              user: Alert.getBasicButton("Usuario", function () {
                vm.login("client");
              }),
              provider: Alert.getBasicButton("Proveedor", function () {
                vm.login("provider");
              }),
              cancel: Alert.getCancelButton(function () {}),
            }
          );
        }
      });
    };
    vm.signinClient = function () {
      if (!vm.log.name) {
        return Alert.error("Ingrese su nombre completo");
      }
      var testName = /^[a-zA-ZÀ-ü\s]*$/;
      if (!testName.test(vm.log.name)) {
        return Alert.error("El nombre debe contener solo letras.");
      }
      if (vm.log.name.replace(" ", "").length < 8) {
        return Alert.error(
          "Su nombre completo debe tener al menos 8 caracteres"
        );
      }
      if (vm.log.name.length > 100) {
        return Alert.error("Su nombre completo excede los 100 caracteres");
      }
      if (!vm.log.email) {
        return Alert.error("Ingrese la dirección correo electrónico");
      }
      var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
      if (!testEmail.test(vm.log.email)) {
        return Alert.error("Ingrese la dirección de correo electrónico valida");
      }
      if (vm.log.email.length > 100) {
        return Alert.error(
          "La dirección correo electrónico excede los 100 caracteres"
        );
      }
      if (!vm.log.phone) {
        return Alert.error("Ingrese teléfono");
      }
      var testName = /^[0-9\s]*$/;
      if (!testName.test(vm.log.phone)) {
        return Alert.error("El teléfono debe contener solo números.");
      }
      if (!vm.log.pass) {
        return Alert.error("Ingrese contraseña");
      }
      if (vm.log.pass.replace(" ", "").length < 8) {
        return Alert.error("La contraseña debe tener al menos 8 caracteres");
      }
      if (vm.log.pass.replace(" ", "").length > 100) {
        return Alert.error("La contraseña excede los 100 caracteres");
      }
      AuthService.signinClient(
        {
          email: vm.log.email.toLowerCase(),
          pass: md5(vm.log.pass),
          name: vm.log.name,
          phone: vm.log.phone,
        },
        function (error, response) {
          if (!error) {
            var rand = RandomString.generate(10, "l");
            Store.put(
              rand,
              "Hemos enviado un correo de activación a su cuenta"
            );
            $state.go("success", {
              id: rand,
            });
          } else {
            Alert.error(response);
          }
        }
      );
    };
    vm.signinProvider = function () {
      var memberTex = /^\s*$/;
      if (memberTex.test(vm.membership)) {
        return Alert.error("Selecciona una membresía");
      }
      if (!vm.log.name) {
        return Alert.error("Ingrese su nombre completo");
      }
      var testName = /^[a-zA-ZÀ-ü\s]*$/;
      if (!testName.test(vm.log.name)) {
        return Alert.error("El nombre debe contener solo letras.");
      }
      if (vm.log.name.replace(" ", "").length < 8) {
        return Alert.error(
          "Su nombre completo debe tener al menos 8 caracteres"
        );
      }
      if (vm.log.name.length > 100) {
        return Alert.error("Su nombre completo excede los 100 caracteres");
      }
      if (!vm.log.email) {
        return Alert.error("Ingrese la dirección correo electrónico");
      }
      var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
      if (!testEmail.test(vm.log.email)) {
        return Alert.error("Ingrese la dirección de correo electrónico valida");
      }
      if (vm.log.email.length > 100) {
        return Alert.error(
          "La dirección correo electrónico excede los 100 caracteres"
        );
      }
      AuthService.checkIfExistProvider(
        {
          email: vm.log.email.toLowerCase(),
        },
        function (error, response) {
          if (error) {
            return Alert.error(response);
          }
          $state.go("signin", {
            action: "GET",
            name: vm.log.name,
            email: vm.log.email,
            membership: vm.membership,
          });
        }
      );
    };
    vm.init = function () {
      vm.loginShow = true;
      vm.email = true;
      vm.userRole = false;
      vm.registerShow = true;
      vm.registerForm = false;
      vm.proveedor = false;
      vm.isLogin = false;
      vm.isClientSignin = false;
      vm.forgotPassword = false;
      vm.resetType = "client";
      var deviceId = UiOpenpay.getDeviceSessionId();
    };
    $scope.$on("$viewContentLoaded", function () {
      vm.init();
    });
  })
  .controller("TermsInstance", function ($uibModalInstance) {
    var vm = this;
    vm.close = function () {
      $uibModalInstance.dismiss("close");
    };
  });
app.controller("MainCtrl", function (
  $scope,
  $uibModal,
  $rootScope,
  AuthService,
  $state,
  $log,
  Main,
  Store,
  Service,
  Alert,
  CONFIG,
  Admins
) {
  var vm = this;
  vm.a = Alert;
  vm.collapse = true;
  vm.shadow = false;
  vm.filters = true;
  vm.current = null;
  vm.client = false;
  vm.notifications = [];
  vm.reconnectPromise = null;
  vm.reconnect = true;
  vm.reconnectIntents = 0;
  vm.justNumbers = "/d|{1}.|^+-e/";
  vm.currentUrl = CONFIG.backend_url + "image?id=";
  vm.isClient = function () {
    return vm.client;
  };
  vm.filtrosServicios = [];
  vm.categories = [];
  Main.order = "registration_date";
  vm.order = "registration_date";
  vm.logout = function () {
    AuthService.logout();
  };
  vm.getNotifications = function () {
    Admins.getNotifications(
      function (err, response) {
        if (!err) {
          
          vm.notifications = response.data;
          
        } else {
          Alert.setInfo("Error de conexión", false);
          setTimeout(function () {
            Alert.showInfo();
          }, 500);
        }
      }
    );
  };
  vm.getCurrentGeolocation = function () {
    if (vm.isClient()) {
      vm.ordernarServicios = ["Más cercanos"];
      vm.sortOptions = ["Más cercanos", "Más solicitados", "Mostrar todo"];
      var lat = localStorage.getItem("latCurrent");
      if (lat == undefined) {
        vm.msnLocation = "(Obteniendo ubicación)...";
        vm.ordernarServicios = ["Mostrar todo"];
        vm.sortOptions = ["Más solicitados", "Mostrar todo"];
      }
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          vm.successHandler,
          vm.errorHanler,
          { timeout: 5000 }
        );
      } else {
        vm.msnLocation = "(La ubicación no esta habilitada)";
        vm.ordernarServicios = ["Mostrar todo"];
        vm.sortOptions = ["Más solicitados", "Mostrar todo"];
      }
    }
  };
  vm.successHandler = function (position) {
    vm.msnLocation = "(Obteniendo ubicación)...";
    Main.loc = true;
    Main.lat = position.coords.latitude;
    Main.lng = position.coords.longitude;
    vm.ordernarServicios = ["Mostrar todo"];
    vm.sortOptions = ["Más cercanos", "Más solicitados", "Mostrar todo"];
    localStorage.setItem("latCurrent", Main.lat);
    localStorage.setItem("lngCurrent", Main.lng);
    vm.msnLocation = "";
  };
  vm.errorHanler = function (err) {
    if (err.code == err.PERMISSION_DENIED) {
      vm.msnLocation = "(Permiso de ubicación deshabilitada)";
    }
    if (err.code == err.TIMEOUT) {
      vm.msnLocation = "(Error al encontrar la ubicación, intente de nuevo)";
    }
  };
  vm.edit = function (id) {
    $state.go("provider.servicesEdit", {
      serviceId: id,
    });
  };
  vm.goIndex = function () {
    if (vm.isClient()) {
      $state.go("user.services");
    } else {
      $state.go("provider.services");
    }
  };
  vm.deleteService = function (_item) {
    Alert.setConfirm(
      "¿Desea eliminar el servicio?",
      _item.name,
      {
        name: "Eliminar",
        action: function () {
          Service.delete(_item, function (err, data) {
            if (!err) {
              Alert.setInfo("Servicio eliminado", true, function () {
                //$state.reload();
              });
            } else {
              Alert.setInfo(data || "No se pudo eliminar el servicio", false);
            }
            setTimeout(function () {
              Alert.hideConfirm();
              Alert.showInfo2();
            }, 1000);
          });
        },
      },
      {
        name: "Cancelar",
        action: function () {
          Alert.hideConfirm();
        },
      }
    );
    setTimeout(function () {
      Alert.showConfirm();
    }, 500);
  };
  vm.toggleMenu = function () {
    vm.shadow = vm.collapse;
    vm.collapse = !vm.collapse;
    vm.filters = true;
  };
  vm.toggleFilters = function () {
    vm.shadow = vm.filters;
    vm.filters = !vm.filters;
    vm.collapse = true;
  };
  vm.closeShadow = function () {
    vm.shadow = false;
    vm.filters = true;
    vm.collapse = true;
  };
  vm.getModuleName = function () {
    return Main.getModule();
  };
  vm.getUserName = function () {
    if (AuthService.getUser()) {
      return AuthService.getUser().data.full_name;
    } else {
      return false;
    }
  };
  vm.getRole = function () {
    return AuthService.getRole();
  };
  vm.select = function (_service) {
    vm.current = _service;
    if (vm.isClient()) {
      $("#showServiceDetail").click();
    } else {
      $state.go("provider.servicesDetail", {
        serviceId: vm.current._id,
      });
    }
  };
  vm.selectProvider = function (_Provider) {
    $rootScope.provider = _Provider._id;
    $state.go("admin.updatepayment", {
      ProviderId: _Provider._id,
    });
  };
  vm.selectOrder = function (_order) {
    vm.order = _order;
  };
  vm.applyFilter = function () {
    Main.order = vm.order;
    vm.toggleFilters();
  };
  vm.getAppts = function () {
    return Main.appts;
  };
  vm.translateDur = function (duration) {
    return Main.translateDuration(duration);
  };
  vm.selectFilter = function (filter) {
    if (vm.filtrosServicios.includes(filter)) {
      vm.filtrosServicios.splice(vm.filtrosServicios.indexOf(filter), 1);
    } else {
      vm.filtrosServicios = [];
      vm.filtrosServicios.push(filter);
    }
  };
  vm.selectSort = function (sort) {
    vm.ordernarServicios = [];
    vm.ordernarServicios.push(sort);
  };
  vm.applyFilterClient = function () {
    Main.type = vm.filtrosServicios.join(",");
    Main.sort = vm.ordernarServicios.join(",");
    vm.toggleFilters();
  };
  vm.getFilter = function (_item) {
    if (vm.filtrosServicios && vm.filtrosServicios.length) {
      var hasOne = false;
      for (var i in vm.filtrosServicios) {
        hasOne = _item.categories.includes(vm.filtrosServicios[i]);
        if (hasOne) {
          break;
        }
      }
      return hasOne;
    } else {
      return true;
    }
  };
  vm.getImage = function (_id) {
    return CONFIG.backend_url + "image?id=" + _id;
  };
  vm.getCurrent = function () {
    return vm.current;
  };
  vm.makeAppointment = function () {
    $state.go("user.appointment", {
      serviceId: vm.current._id,
    });
  };
  vm.hardDisconnect = function () {
    vm.reconnect = false;
    clearInterval(vm.reconnectPromise);
    Alert.info(
      "Su sesión ha expirado, verifique su conexión e intente de nuevo"
    );
    $state.go("login");
  };
  vm.init = function () {
    vm.getNotifications()
    
    vm.myLocal = JSON.parse(localStorage.getItem("session.user"));
    vm.client = vm.getRole() === "client";
    vm.getCurrentGeolocation();
    Service.getCategories(function (err, data) {
      if (!err) {
        let cont = 0;
        data = data.map(cat => {
          cont = cont + 1;
          if (cont === 9) {
            cont = 1;
          }
          cat.index = cont;
          return cat; 
        })
        
        vm.categories = data;
      }
    });
  };
  $scope.$on("$destroy", function () {
    vm.reconnect = false;
  });
  $scope.$on("$viewContentLoaded", function (ev, data) {
    if (data === "@") {
      vm.init();
    }
  });
});
app.controller("SuccessCtrl", function (
  $scope,
  $uibModal,
  $state,
  $stateParams,
  RandomString,
  Store
) {
  var vm = this;
  vm.success = false;
  vm.message = "";
  vm.isSuccess = function () {
    return vm.success;
  };
  vm.getMessage = function () {
    return vm.message;
  };
  vm.init = function () {
    vm.message = Store.pop($stateParams.id);
    vm.success = vm.message;
  };
  vm.showTerms = function () {
    $uibModal.open({
      templateUrl: "views/main/modals/terms.html",
      controller: "TermsInstance as md",
      size: "lg",
    });
  };
  vm.goLogin = function () {
    $state.go("login");
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.service("Alert", function ($uibModal) {
  var serv = {};
  serv.information = {
    success: false,
    message: "",
    callback: null,
  };
  serv.loading = {
    message: "",
  };
  serv.confirmation = {
    message: "",
    bigMessage: "",
    confirm: {
      action: null,
      name: "",
    },
    cancel: {
      action: null,
      name: name,
    },
  };
  serv.appt = null;
  serv.getInformation = function () {
    return serv.information;
  };
  serv.getLoading = function () {
    return serv.loading;
  };
  serv.getConfirmation = function () {
    return serv.confirmation;
  };
  serv.showAlert = function (title, content, type) {
    $.alert({
      title: title,
      content: content,
      type: type,
      animation: "zoom",
      animationSpeed: 500,
      buttons: {
        ok: {
          text: "Ok",
          keys: ["enter"],
          action: function () {},
        },
      },
    });
  };
  serv.error = function (message) {
    serv.showAlert("Error", message, "red");
  };
  serv.info = function (message) {
    serv.showAlert("Concita", message, "");
  };
  serv.confirm = function (title, message, buttons) {
    $.confirm({
      title: title,
      icon: "fa fa-exclamation-triangle",
      content: message,
      type: "",
      animation: "zoom",
      animationSpeed: 500,
      buttons: buttons,
    });
  };
  serv.getButton = function (text, btnClass, keys, action) {
    return {
      text: text,
      btnClass: btnClass,
      keys: keys,
      action: action,
    };
  };
  serv.getBasicButton = function (text, action) {
    var keys = [];
    var key = text[0];
    keys.push(key.toLowerCase());
    return serv.getButton(text, "", keys, action);
  };
  serv.getCancelButton = function (action) {
    return serv.getButton("Cancelar", "btn-red", ["esc", "c"], action);
  };
  serv.getOkButton = function (action) {
    return serv.getButton("Aceptar", "btn-success", ["a", "enter"], action);
  };
  serv.setInfo = function (message, success, callback) {
    serv.information.message = message;
    serv.information.success = success;
    serv.information.callback = callback;
  };
  serv.showInfo = function () {
    $("#serviceNotification").modal({
      backdrop: "static",
      keyboard: false,
    });
    setTimeout(function () {
      $("#showServiceNotification").click();
      if (serv.information.callback) {
        serv.information.callback();
      }
    }, 2000);
  };

  serv.showInfo2 = () => {
   
    $('#serviceNotification').modal({
        backdrop : 'static',
        keyboard : true
    });

    setTimeout(() => {
    
     // $('#serviceNotification').click();
      $('#showServiceNotification').click();
        if(serv.information.callback) {
            serv.information.callback();
        }
        
     location.reload();
    }, 2000);

    
};
  serv.setLoading = function (message) {
    serv.loading.message = message;
  };
  serv.showLoading = function () {
    $("#serviceCreating").modal({
      backdrop: "static",
      keyboard: false,
    });
  };
  serv.hideLoading = function () {
    setTimeout(function () {
      $("#showServiceCreating").click();
    }, 500);
  };
  serv.setConfirm = function (message, bigMessage, confirm, cancel) {
    serv.confirmation.message = message;
    serv.confirmation.bigMessage = bigMessage;
    serv.confirmation.confirm = confirm;
    serv.confirmation.cancel = cancel;
  };
  serv.showConfirm = function () {
    $("#serviceDeleteConfirm").modal({
      backdrop: "static",
      keyboard: false,
    });
  };
  serv.hideConfirm = function () {
    $("#showServiceDeleteConfirm").click();
  };
  serv.showAppointment = function () {
    $("#serviceSolicitedConfirm").modal({
      backdrop: "static",
      keyboard: false,
    });
  };
  serv.setConfirmAppt = function (_appt) {
    serv.appt = _appt;
  };
  serv.showConfirmAppt = function () {
    $("#serviceConfirm").modal({});
  };
  serv.showConfirmAppt2 = () => {
    $('#serviceCancel').modal({});
};
  return serv;
});
app
  .filter("titlecase", function () {
    return function (input) {
      var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;
      input = input.toLowerCase();
      return input.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function (
        match,
        index,
        title
      ) {
        if (
          index > 0 &&
          index + match.length !== title.length &&
          match.search(smallWords) > -1 &&
          title.charAt(index - 2) !== ":" &&
          (title.charAt(index + match.length) !== "-" ||
            title.charAt(index - 1) === "-") &&
          title.charAt(index - 1).search(/[^\s-]/) < 0
        ) {
          return match.toLowerCase();
        }
        if (match.substr(1).search(/[A-Z]|\../) > -1) {
          return match;
        }
        return match.charAt(0).toUpperCase() + match.substr(1);
      });
    };
  })
  .filter("cut", function () {
    return function (value, wordwise, max, tail) {
      if (!value) return "";
      max = parseInt(max, 10);
      if (!max) return value;
      if (value.length <= max) return value;
      value = value.substr(0, max);
      if (wordwise) {
        var lastspace = value.lastIndexOf(" ");
        if (lastspace !== -1) {
          if (
            value.charAt(lastspace - 1) === "." ||
            value.charAt(lastspace - 1) === ","
          ) {
            lastspace = lastspace - 1;
          }
          value = value.substr(0, lastspace);
        }
      }
      return value + (tail || " …");
    };
  })
  .filter("getIcon", function () {
    return function (value) {
      switch (value) {
        case "Citas Médicas":
          return "fa-briefcase-medical";
        case "Cine y video":
          return "fa-video";
        case "Asesoría legal":
          return "fa-gavel";
        case "Arquitectura y arte":
          return "fa-paint-brush";
        case "Cuidado Infantil":
          return "fa-child";
        case "Deportes":
          return "fa-futbol";
        case "Tecnología":
          return "fa-mobile-alt";
        case "Veterinario":
          return "fa-paw";
        case "Mercadotecnia y diseño":
          return "fa-magic";
        case "Estilista y cultura de belleza":
          return "fa-heart";
        case "Cuidado de enfermería":
          return "fa-stethoscope";
        case "Otra":
          return "fa-star";
        case "Servicios domésticos":
          return "fa-couch";
        case "Servicio de mantenimiento automotriz":
          return "fa-car";
        case "Laboratorio de análisis clínico":
          return "fa-syringe";
        case "Taller Mecánico":
          return "fa-wrench";
      }
    };
  })
  .filter("priceType", function () {
    return function (value) {
      switch (value) {
        case "free":
          return "Gratuito";
        case "price":
          return "Costo";
        case "quotation":
          return "Sujeto a cotización";
      }
    };
  })
  .filter("getDays", function () {
    return function (arr) {
      var days = arr.join("-");
      days = days.replace("Su", "Do");
      days = days.replace("Mo", "Lu");
      days = days.replace("Tu", "Ma");
      days = days.replace("We", "Mi");
      days = days.replace("Th", "Ju");
      days = days.replace("Fr", "Vi");
      return days;
    };
  })
  .filter("getDay", function () {
    return function (data) {
      data = data.replace("Su", "Do");
      data = data.replace("Mo", "Lu");
      data = data.replace("Tu", "Ma");
      data = data.replace("We", "Mi");
      data = data.replace("Th", "Ju");
      data = data.replace("Fr", "Vi");
      return data;
    };
  })
  .filter("time", function () {
    return function (data) {
      var end = data < 12 ? " am" : " pm";
      if (data == 24) {
        end = " am";
      }
      if (data > 12) {
        data = data - 12;
      }
      var hour = Math.floor(data);
      var start = hour <= 9 ? "0" : "";
      var decimalPart = data - hour;
      var minutes;
      minutes = Math.round(60 * decimalPart);
      switch (minutes.toString()) {
        case "0":
          minutes = "00";
          break;
        case "5":
          minutes = "05";
          break;
        case "60":
          minutes = "00";
          hour += 1;
          if (hour == 10) {
            start = "";
          }
          break;
      }
      if (hour == 0) {
        hour = 12;
        start = "";
      }
      return "" + start + hour + ":" + minutes + end;
    };
  })
  .filter("creditcard", function () {
    return function (input) {
      var result = "";
      for (var i = 1; i <= input.length; i++) {
        result += input[i - 1];
        if (i % 4 === 0 && i != input.length) {
          result += " - ";
        }
      }
      return result;
    };
  })
  .filter("openstatus", function () {
    return function (input) {
      switch (input) {
        case "trial":
          return "PRUEBA";
        case "past_due":
          return "VENCIDA";
        case "unpaid":
          return "SIN PAGAR";
        case "cancelled":
          return "CANCELADA";
        case "active":
          return "ACTIVA";
      }
    };
  })
  .directive("justNumbers", function () {
    return {
      restrict: "A",
      link: function (scope, element, attrs) {
        element.on("keypress", function (event) {
          var numbers = "1234567890";
          var isIntegerChar = function () {
            var entrance = event.which;
            return entrance === 101 || entrance === 69;
          };
          if (isIntegerChar()) {
            event.preventDefault();
          }
        });
      },
    };
  })
  .directive("justChars", function (RandomString) {
    return {
      restrict: "A",
      link: function (scope, element, attrs) {
        element.on("keypress", function (event) {
          var validChars =
            RandomString.getValidChars() +
            RandomString.getValidChars().toUpperCase() +
            " ";
          var isValidChar = function () {
            var entrance = event.key;
            return validChars.indexOf(entrance) !== -1;
          };
          if (!isValidChar()) {
            event.preventDefault();
          }
        });
      },
    };
  });
app.service("Main", function () {
  var serv = {};
  var module = "";
  serv.appts = 0;
  serv.loc = false;
  serv.lat = "";
  serv.lng = "";
  serv.setter = function (object) {
    return JSON.parse(JSON.stringify(object));
  };
  serv.getModule = function () {
    return module;
  };
  serv.setModule = function (name) {
    module = name;
  };
  serv.getAppts = function () {
    return serv.appts;
  };
  serv.setAppts = function (_appts) {
    serv.appts = _appts;
  };
  serv.parseDaysSeleted = function (days) {
    var daysSelected = ["", "", "", "", "", "", ""];
    for (var d = 0; d < days.length; d++) {
      var day = days[d];
      switch (day) {
        case "Mo":
          daysSelected[0] = day;
          break;
        case "Tu":
          daysSelected[1] = day;
          break;
        case "We":
          daysSelected[2] = day;
          break;
        case "Th":
          daysSelected[3] = day;
          break;
        case "Fr":
          daysSelected[4] = day;
          break;
        case "Sa":
          daysSelected[5] = day;
          break;
        case "Su":
          daysSelected[6] = day;
          break;
      }
    }
    return daysSelected;
  };
  serv.order = "registration_date";
  serv.translateDuration = function (duration) {
    switch (duration) {
      case 5:
        return "5 minutos";
      case 10:
        return "10 minutos";
      case 15:
        return "15 minutos";
      case 20:
        return "20 minutos";
      case 30:
        return "30 minutos";
      case 60:
        return "1 hora";
      case 60 * 1.5:
        return "1.5 horas";
      case 60 * 2:
        return "2 horas";
      case 60 * 3:
        return "3 horas";
    }
  };
  serv.translateDurationToHours = function (duration) {
    switch (duration) {
      case 5:
        return 0.08333333333333332;
      case 10:
        return 0.16666666666666664;
      case 15:
        return 0.25;
      case 20:
        return 0.3333333333333333;
      case 30:
        return 0.5;
      case 60:
        return 1;
      case 60 * 1.5:
        return 1.5;
      case 60 * 2:
        return 2;
      case 60 * 3:
        return 3;
    }
  };
  serv.translateDecimalPart = function (decimalPart) {
    return Math.round(decimalPart * 60);
  };
  serv.loading = function () {};
  serv.unloading = function () {};
  return serv;
});
app.factory("UiOpenpay", function (CONFIG, $q) {
  var UiOpenpay = {};
  UiOpenpay.init = function () {
    OpenPay.setId(CONFIG.openpay.merchant_id);
    OpenPay.setApiKey(CONFIG.openpay.api_key);
    OpenPay.setSandboxMode(CONFIG.openpay.is_sandbox);
  };
  UiOpenpay.getDeviceSessionId = function () {
    return OpenPay.deviceData.setup();
  };
  UiOpenpay.getTokencard = function (formName) {
    return $q(function (resolve, reject) {
      OpenPay.token.extractFormAndCreate(
        formName,
        function (token) {
          var deviseSessionId = UiOpenpay.getDeviceSessionId();
          var response = {
            token_id: token.data.id,
            device_session_id: deviseSessionId,
          };
          resolve(response);
        },
        function (error) {
          reject(error);
        }
      );
    });
  };
  UiOpenpay.getTokencardReference = function (formName) {
    return $q(function (resolve, reject) {
      var deviseSessionId = UiOpenpay.getDeviceSessionId();
      var response = {
        token_id: "referencia_bancaria",
        device_session_id: deviseSessionId,
      };
      resolve(response);
    });
  };
  return UiOpenpay;
});
app.directive("googleplace", function () {
  return {
    require: "ngModel",
    link: function (scope, element, attrs, model) {
      var options = {
        types: [],
        componentRestrictions: { country: "MX" },
      };
      scope.gPlace = new google.maps.places.Autocomplete(element[0], options);
      google.maps.event.addListener(scope.gPlace, "place_changed", function () {
        model.$setViewValue(element.val());
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ address: element.val() }, function (
          results,
          status
        ) {
          if (status == "OK") {
            var lat = results[0].geometry.location.lat();
            var lng = results[0].geometry.location.lng();
            localStorage.setItem("lat", lat);
            localStorage.setItem("lng", lng);
            localStorage.removeItem("msn");
          } else {
            localStorage.removeItem("lat");
            localStorage.removeItem("lng");
            localStorage.setItem(
              "msn",
              "Tu dirección no se ha encontrado en Google maps. Es necesario que ingreses una dirección valida."
            );
          }
        });
      });
    },
  };
});
app.service("RandomString", function () {
  var vowels = "aeiou";
  var numbers = "1234567890";
  var sweets = "rtpsdfjklmncvb";
  var hards = "qwyghzx";
  var consonants = sweets + hards;
  var maxlength = 1000;
  var service = {};
  service.generate = function (lenth, type) {
    if (lenth === void 0) {
      lenth = 10;
    }
    if (type === void 0) {
      type = "a";
    }
    if (lenth > maxlength) lenth = maxlength;
    var source = getSource(type);
    var prev = -1;
    var newString = "";
    for (var i = 0; i < lenth; i++) {
      var rand = 0;
      do {
        rand = service.randomNumber(0, source.length - 1);
      } while (rand === prev);
      prev = rand;
      newString += source[rand];
    }
    return newString;
  };
  service.randomNumber = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  };
  var getSource = function (type) {
    switch (type.toLowerCase()) {
      case "a":
        return consonants + vowels + numbers;
      case "v":
        return vowels;
      case "c":
        return consonants;
      case "n":
        return numbers;
      case "vc":
      case "cv":
      case "l":
        return vowels + consonants;
      case "vn":
      case "nv":
        return vowels + numbers;
      case "nc":
      case "cn":
        return numbers + consonants;
      case "s":
        return sweets;
      case "h":
        return hards;
      default:
        return consonants + vowels + numbers;
    }
  };
  var getValidChars = function () {
    return vowels + sweets + hards;
  };
  service.getValidChars = getValidChars;
  return service;
});
app.service("Request", function ($http, Store, CONFIG, Main) {
  var serv = CONFIG.backend_url;
  var headers = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  };
  var req = {};
  req.get = function (route, token) {
    if (token === void 0) {
      token = false;
    }
    if (token) headers.headers["Authorization"] = token;
    return $http.get(serv + route, headers);
  };
  req.post = function (route, params, token) {
    if (token === void 0) {
      token = false;
    }
    if (token) headers.headers["Authorization"] = token;
    return $http.post(serv + route, $.param(params), headers);
  };
  req.put = function (route, params, token) {
    if (token === void 0) {
      token = false;
    }
    if (token) headers.headers["Authorization"] = token;
    return $http.put(serv + route, $.param(params), headers);
  };
  req.delete = function (route, token) {
    if (token === void 0) {
      token = false;
    }
    if (token) headers.headers["Authorization"] = token;
    return $http.delete(serv + route, headers);
  };
  return req;
});
app.service("Store", function () {
  var serv = {};
  var temp = [];
  serv.set = function (name, object) {
    object = JSON.stringify(object);
    localStorage.setItem(name, object);
  };
  serv.get = function (name) {
    if (localStorage.getItem(name)) {
      return JSON.parse(localStorage.getItem(name));
    } else {
      return false;
    }
  };
  serv.remove = function (name) {
    if (localStorage.getItem(name)) {
      localStorage.removeItem(name);
    }
  };
  serv.clear = function () {
    localStorage.clear();
  };
  serv.put = function (name, object) {
    temp[name] = object;
  };
  serv.delete = function (name) {
    if (temp[name]) {
      delete temp[name];
    }
  };
  serv.pop = function (name) {
    return temp[name] || false;
  };
  serv.clearTemp = function () {
    temp = [];
  };
  return serv;
});
app.controller("createFacturas", function (
  $http,
  Main,
  AuthService,
  CONFIG,
  Service,
  Admins,
  Alert,
  $state,
  $scope,
  $rootScope
) {
  var vm = this;
  vm.query = "";
  vm.offset = 1;
  vm.page = 1;
  vm.providerselect = null;
  vm.showdetail = false;
  vm.admins = null;
  vm.admin = null;
  vm.facturas = null;
  vm.guardar = false;
  vm.passfactura = "Y29uY2l0YTpjb25jaXRhNzg5";
  vm.newClient = {
    Email: "pruebas@gmail.com",
    Address: {
      Street: "Av Seguridad Soc",
      ExteriorNumber: "123",
      InteriorNumber: "",
      Neighborhood: "Fidel Velazquez",
      ZipCode: "78436",
      Locality: "",
      Municipality: "Soledad de Graciano Sánchez",
      State: "San Luis Potosí",
      Country: "Mex",
    },
    Rfc: "ROAM861021459",
    Name: "Manuel Romero Alva",
    CfdiUse: "P01",
  };
  vm.newCfdi = {
    Serie: "",
    Currency: "MXN",
    ExpeditionPlace: "44150",
    PaymentConditions: "CREDITO A SIETE DIAS",
    CfdiType: "I",
    PaymentForm: "03",
    PaymentMethod: "PUE",
    Receiver: {
      Rfc: "XAXX010101000",
      Name: "RADIAL SOFTWARE SOLUTIONS",
      CfdiUse: "P01",
    },
    Items: [
      {
        ProductCode: "10101504",
        IdentificationNumber: "EDL",
        Description: "Estudios de viabilidad",
        Unit: "NO APLICA",
        UnitCode: "MTS",
        UnitPrice: 50.0,
        Quantity: 2.0,
        Subtotal: 100.0,
        Taxes: [
          {
            Total: 16.0,
            Name: "IVA",
            Base: 100.0,
            Rate: 0.16,
            IsRetention: false,
          },
        ],
        Total: 116.0,
      },
    ],
  };
  vm.valuesFacturama = {
    token: vm.passfactura,
    url: "https://apisandbox.facturama.mx/",
  };
  vm.proveedor = {};
  vm.myLocal = JSON.parse(localStorage.getItem("session.user"));
  vm.currentDate = new Date();
  vm.checkDate = function (dateC) {
    dateC = new Date(dateC);
    if (dateC < vm.currentDate) {
      return true;
    }
    return false;
  };
  vm.calcularTotales = function () {
    vm.newCfdi.Items[0].Subtotal =
      vm.newCfdi.Items[0].UnitPrice * vm.newCfdi.Items[0].Quantity;
    vm.newCfdi.Items[0].Taxes[0].Base = vm.newCfdi.Items[0].Subtotal;
    vm.newCfdi.Items[0].Taxes[0].Total =
      vm.newCfdi.Items[0].Taxes[0].Base * vm.newCfdi.Items[0].Taxes[0].Rate;
    vm.newCfdi.Items[0].Total =
      vm.newCfdi.Items[0].Subtotal + vm.newCfdi.Items[0].Taxes[0].Total;
  };
  vm.getFacturas = function () {
    var rfc = "XEXX010101000";
    $http({
      method: "GET",
      headers: { Authorization: "Basic " + vm.passfactura },
      url: "https://apisandbox.facturama.mx/cfdi?type=issued&keyword=" + rfc,
    }).then(
      function successCallback(response) {
        
        vm.facturas = response.data;
      },
      function errorCallback(response) {
        
      }
    );
  };
  vm.downloadFactura = function (i) {
    Facturama.Cfdi.Download("pdf", "issued", i, function (result) {
      
      var blob = converBase64toBlob(result.Content, "application/pdf");
      var blobURL = URL.createObjectURL(blob);
      window.open(blobURL);
    });
  };
  vm.saveFactura = function (i) {
    vm.guardar = true;
    vm.newCfdi.Receiver.Rfc = vm.proveedor.rfc;
    vm.newCfdi.Receiver.Name = vm.proveedor.namefiscal;
    
    vm.newClient = {
      Email: vm.proveedor.emails[0],
      Address: {
        Street: vm.proveedor.address,
        ExteriorNumber: "123",
        InteriorNumber: "",
        Neighborhood: vm.proveedor.address,
        ZipCode: "",
        Locality: "",
        Municipality: vm.proveedor.address.substring(0, 30),
        State: "Jalisco",
        Country: "Mex",
      },
      Rfc: vm.proveedor.rfc,
      Name: vm.proveedor.namefiscal,
      CfdiUse: "P01",
    };
    Facturama.Clients.Create(vm.newClient, function (result) {
      
      Facturama.Cfdi.Create(
        vm.newCfdi,
        function (result) {
          var cfdi = result;
          
          var email = vm.proveedor.emails[0];
          var type = "issued";
          Facturama.Cfdi.Send(
            "?cfdiType=" + type + "&cfdiId=" + cfdi.Id + "&email=" + email,
            function (result) {
              
            }
          );
          Facturama.Cfdi.Download("pdf", "issued", cfdi.Id, function (result) {
            
            blob = converBase64toBlob(result.Content, "application/pdf");
            var blobURL = URL.createObjectURL(blob);
            window.open(blobURL);
            $state.go("admin.listFacturas");
          });
        },
        function (error) {
          if (error && error.responseJSON) {
            Alert.setInfo("Error de factura:" + error.responseJSON, false);
            
            vm.guardar = false;
          }
        }
      );
    });
  };
  vm.goProveedores = function () {
    $state.go("admin.listProvider");
  };
  vm.init = function () {
    Main.setModule(vm.module);
    $rootScope.$watch("shared", function (newValue) {
      vm.proveedor = JSON.parse($rootScope.shared);
      
    });
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.controller("listFacturas", function (
  $http,
  Main,
  AuthService,
  CONFIG,
  Service,
  Admins,
  Alert,
  $state,
  $scope,
  $timeout
) {
  var vm = this;
  vm.query = "";
  vm.offset = 1;
  vm.page = 1;
  vm.providerselect = null;
  vm.showdetail = false;
  vm.admins = null;
  vm.admin = null;
  vm.facturas = null;
  vm.passfactura = "Y29uY2l0YTpjb25jaXRhNzg5";
  vm.valuesFacturama = {
    token: vm.passfactura,
    url: "https://apisandbox.facturama.mx/",
  };
  vm.myLocal = JSON.parse(localStorage.getItem("session.user"));
  vm.currentDate = new Date();
  vm.checkDate = function (dateC) {
    dateC = new Date(dateC);
    if (dateC < vm.currentDate) {
      return true;
    }
    return false;
  };
  vm.searchfacturas = function (i) {
    if (i) {
      var rfc = i;
      $http({
        method: "GET",
        headers: { Authorization: "Basic " + vm.passfactura },
        url: "https://apisandbox.facturama.mx/cfdi?type=issued&keyword=" + rfc,
      }).then(
        function successCallback(response) {
          
          vm.facturas = response.data;
        },
        function errorCallback(response) {
          
        }
      );
    } else {
      vm.getFacturas();
    }
  };
  vm.getFacturas = function () {
    var rfc = "XEXX010101000";
    $http({
      method: "GET",
      headers: { Authorization: "Basic " + vm.passfactura },
      url: "https://apisandbox.facturama.mx/cfdi?type=issued",
    }).then(
      function successCallback(response) {
        
        vm.facturas = response.data;
      },
      function errorCallback(response) {
        
      }
    );
  };
  vm.downloadFactura = function (i) {
    Facturama.Cfdi.Download("pdf", "issued", i, function (result) {
      
      var blob = converBase64toBlob(result.Content, "application/pdf");
      var blobURL = URL.createObjectURL(blob);
      window.open(blobURL);
    });
  };
  vm.goProveedores = function () {
    $state.go("admin.listProvider");
  };
  vm.init = function () {
    Main.setModule(vm.module);
    vm.getFacturas();
    
    
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.controller("listProvider", function (
  Main,
  AuthService,
  CONFIG,
  Service,
  Admins,
  Alert,
  $state,
  $scope,
  $rootScope
) {
  var vm = this;
  vm.query = "";
  vm.offset = 1;
  vm.page = 1;
  vm.providerselect = null;
  vm.showdetail = false;
  vm.admins = null;
  vm.admin = null;
  vm.facturas = null;
  vm.inicio = false;
  vm.myLocal = JSON.parse(localStorage.getItem("session.user"));
  vm.currentDate = new Date();
  vm.checkDate = function (dateC) {
    dateC = new Date(dateC);
    if (dateC < vm.currentDate) {
      return true;
    }
    return false;
  };
  vm.Providerslist = function (query) {
    
    if (vm.offset === undefined || vm.offset < 1) {
      vm.offset = 1;
    }
    vm.arr = [];
    Admins.list(
      {
        offset: vm.offset,
        query: query || "",
      },
      function (err, response) {
        if (!err) {
          
          vm.providers = response.docs;
          for (var i = 1; i <= Math.ceil(response.total / 10); i++) {
            vm.arr.push(i);
          }
        } else {
          Alert.setInfo("Error de conexión", false);
          setTimeout(function () {
            Alert.showInfo();
          }, 500);
        }
      }
    );
  };
  vm.showDetail = function (i) {
    vm.showdetail = i;
  };
  vm.loadProvider = function (i) {
    vm.showdetail = true;
    
    vm.providerselect = i;
    
  };
  vm.deleteAdmin = function (i) {
    if (confirm("Deseas realmente eliminar a " + i.name)) {
      Admins.deleteadmin(
        {
          id: i._id,
        },
        function (err, response) {
          if (!err) {
            
            location.reload();
          } else {
            Alert.setInfo("Error de conexión", false);
            setTimeout(function () {
              Alert.showInfo();
            }, 500);
          }
        }
      );
    }
  };
  vm.saveAdmin = function () {
    
    
    Admins.saveadmin(
      {
        name: vm.admin.name,
        email: vm.admin.email,
        password: md5(vm.admin.password),
      },
      function (err, response) {
        if (!err) {
          
          $state.go("admin.listadmins");
        } else {
          Alert.setInfo("Error de conexión", false);
          setTimeout(function () {
            Alert.showInfo();
          }, 500);
        }
      }
    );
  };//sendNotification
  vm.loadAdmins = function () {
    Admins.listadmin(function (err, response) {
      if (!err) {
        
        vm.admins = response;
        
        
      } else {
        Alert.setInfo("Error de conexión", false);
        setTimeout(function () {
          Alert.showInfo();
        }, 500);
      }
    });
  };
  vm.cancel = function () {
    vm.showdetail = false;
  };
  vm.pagemore = function (i) {
    vm.page = i;
    vm.offset = i;
    vm.Providerslist(vm.query);
  };
  vm.addProvider = function () {
    $state.go("signin-admin");
  };
  vm.sendPush = function () {
    $state.go("notifications");
    window.emojiPicker.discover();
  };
  vm.addAdmin = function () {
    $state.go("new-admin");
  };
  vm.goUsuarios = function () {
    $state.go("admin.listadmins");
  };
  vm.goProveedores = function () {
    $state.go("admin.listProvider");
  };
  vm.goFacturas = function () {
    $state.go("admin.listFacturas");
  };
  vm.activarReferencia = function (i) {
    
    
    if (confirm("Deseas realmente activar a " + i.data.name)) {
      Admins.activateprovider(
        {
          id: i._id,
        },
        function (err, response) {
          if (!err) {
            
            location.reload();
          } else {
            Alert.setInfo("Error de conexión", false);
            setTimeout(function () {
              Alert.showInfo();
            }, 500);
          }
        }
      );
    }
  };
  vm.generateFactura = function (i) {
    $scope.$watch("shared", function (newValue) {
      $rootScope.shared = JSON.stringify(i.data);
    });
    $state.go("admin.createfactura");
  };
  vm.updatepayment = function () {
    $state.go("admin.updatepayment");
  };
  vm.init = function () {
    Main.setModule(vm.module);
    vm.Providerslist();
    vm.loadAdmins();
    $rootScope.$watch("inicio", function (newValue) {
      if ($rootScope.inicio == "admin@concita.com") vm.inicio = true;
      
      
    });
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app
  .controller("SubscribeCtrl", function (
    $scope,
    $state,
    Alert,
    AuthService,
    Admins,
    VERSION,
    $uibModal,
    RandomString,
    Store,
    UiOpenpay,
    $rootScope
  ) {
    var vm = this;
    vm.isTarjeta = true;
    vm.isReferencia = false;
    vm.membershiptype = "";
    vm.app_name = "Concita";
    vm.showLoginForm = true;
    vm.getVersion = function () {
      return VERSION.NO;
    };
    vm.tarjeta = function () {
      
      if (vm.isTarjeta) {
        document.getElementsByName("referenciabank1").checked = true;
        document.getElementsByName("referenciabank2").checked = false;
        vm.isTarjeta = true;
        vm.isReferencia = false;
      } else {
        document.getElementsByName("referenciabank1").checked = false;
        document.getElementsByName("referenciabank2").checked = true;
        vm.isTarjeta = false;
        vm.isReferencia = true;
      }
    };
    vm.referencia = function () {
      
      if (vm.isReferencia) {
        document.getElementsByName("referenciabank1").checked = false;
        document.getElementsByName("referenciabank2").checked = true;
        vm.isTarjeta = false;
        vm.isReferencia = true;
      } else {
        document.getElementsByName("referenciabank1").checked = true;
        document.getElementsByName("referenciabank2").checked = false;
        vm.isTarjeta = true;
        vm.isReferencia = false;
      }
    };
    vm.closeAll = function () {
      vm.loginShow = true;
      vm.email = true;
      vm.registerShow = true;
    };
    vm.toggleLogin = function () {
      vm.registerShow = true;
      vm.email = true;
      vm.loginShow = !vm.loginShow;
      vm.isLogin = !vm.isLogin;
    };
    vm.toggleRegister = function () {
      vm.loginShow = true;
      vm.email = true;
      vm.registerShow = !vm.registerShow;
    };
    vm.toggleEmail = function () {
      vm.email = !vm.email;
      vm.loginShow = true;
      vm.registerShow = true;
    };
    vm.prueba = function (member) {
      vm.membership = member;
    };
    vm.showTerms = function () {
      $uibModal.open({
        templateUrl: "views/main/modals/terms.html",
        controller: "TermsInstance as md",
        size: "lg",
      });
    };
    vm.parseDate = function (str) {
      return new Date(str);
    };
    vm.datediff = function (first, second) {
      return Math.round((second - first) / (1000 * 60 * 60 * 24));
    };
    vm.init = function () {
      vm.loginShow = true;
      vm.email = true;
      vm.userRole = false;
      vm.registerShow = true;
      vm.registerForm = false;
      vm.proveedor = false;
      vm.isLogin = false;
      vm.isClientSignin = false;
      vm.forgotPassword = false;
      vm.resetType = "client";
      var deviceId = UiOpenpay.getDeviceSessionId();
    };
    $scope.$on("$viewContentLoaded", function () {
      vm.init();
    });
  })
  .controller("TermsInstance", function ($uibModalInstance) {
    var vm = this;
    vm.close = function () {
      $uibModalInstance.dismiss("close");
    };
  });
app.controller("updatepayment", function (
  Main,
  AuthService,
  CONFIG,
  $rootScope,
  Service,
  Admins,
  Alert,
  $state,
  $stateParams,
  $scope
) {
  var vm = this;
  vm.query = "";
  vm.offset = 1;
  vm.page = 1;
  vm.positionDays = 0;
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth() + 1;
  var yyyy = hoy.getFullYear();
  vm.today = (yyyy + "-" + mm + "-" + dd).toString();
  vm.myLocal = JSON.parse(localStorage.getItem("session.user"));
  vm.loadprovider = function () {
    if (!$rootScope.provider) {
      $state.go("admin.listProvider");
    } else {
      Admins.getProvider($rootScope.provider, function (err, data) {
        if (!err) {
          vm.details = data;
        } else {
          Alert.setInfo(
            "El proveedor no se encuentra disponible",
            false,
            function () {
              $state.go("admin.listProvider");
            }
          );
          setTimeout(function () {
            Alert.showInfo();
          }, 500);
        }
      });
    }
  };
  vm.loadMemebership = function () {
    Admins.getMemberships(function (err, data) {
      if (!err) {
        vm.Memebership = data;
      } else {
      }
    });
  };
  vm.payment = function () {
    var pagos = {
      provider: vm.details._id,
      membership: vm.membership,
      amount: vm.amount,
      positionDays: vm.positionDays,
      registrationDate: null,
      fechaexpration: vm.fechaexpration,
      observations: vm.observations,
    };
    Admins.paymet(pagos, function (err, data) {
      if (!err) {
        Alert.setInfo("Pago exitoso", true, function () {
          $state.go("admin.listProvider");
        });
      } else {
        Alert.setInfo("Error actualizar pago", false);
      }
      setTimeout(function () {
        Alert.showInfo();
      }, 1000);
    });
  };
  vm.pagemore = function (i) {
    vm.page = i;
    vm.offset = i;
    vm.Providerslist(vm.query);
  };
  vm.init = function () {
    Main.setModule(vm.module);
    vm.loadprovider();
    vm.loadMemebership();
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.controller("ProviderNewCardC", function (
  $scope,
  $state,
  Provider,
  Alert,
  UiOpenpay
) {
  var vm = this;
  vm.module = "Agregar nueva tarjeta";
  vm.credit_card = {};
  vm.saveCreditCard = function () {
    UiOpenpay.init();
    UiOpenpay.getTokencard("newcard-form")
      .then(function (response) {
        Provider.saveCreditCard(response, function (
          updateError,
          updateResponse
        ) {
          if (!updateError) {
            Alert.info("Registro exitoso");
            setTimeout(function () {
              $state.go("provider.profile");
            });
          } else {
            Alert.error(updateResponse);
          }
        });
      })
      .catch(function (errorResponse) {
        var descrption = errorResponse.data.description
          ? errorResponse.data.description
          : errorResponse.message;
        var errorMessage =
          "ERROR - " + errorResponse.status + " - " + descrption;
        Alert.error(errorMessage);
      });
  };
  vm.init = function () {};
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app
  .controller("ProviderNewEventC", function (
    $scope,
    $state,
    Appointment,
    EventService,
    Main,
    Alert
  ) {
    var vm = this;
    vm.appointments = [];
    vm.module = "Agregar Evento";
    vm.isOpenFromDate = false;
    vm.isOpenDueDate = false;
    vm.openFromDate = function () {
      vm.isOpenFromDate = true;
    };
    vm.openDueDate = function () {
      vm.isOpenDueDate = true;
    };
    vm.saveEvent = function () {
      
      EventService.save(vm.event, function (error, response) {
        if (!error) {
          Alert.info("Evento creado correctamente");
          $state.go("provider.schedule");
        } else {
          Alert.error(response);
        }
      });
    };

    vm.saveEvent2 = () => {
      
      
      
      EventService.save(vm.event, (error, response) => {
          if (!error) {
              Alert.info('Se ha bloqueado ese horario');
              vm.loadAppointments2();
              $state.go('provider.schedule');
          } else {
              Alert.error(response);
          }
      });
      
  };

  vm.loadAppointments2 = () => {

    var now = new Date(vm.event.from_date); // Fri Feb 20 2015 19:29:31 GMT+0530 (India Standard Time) 
    Appointment.getAllFromProvider({
     
    }, (err, data) => {
        if (!err) {
            let apts = data;

            for (let i = 0; i < apts.length; i++) {
                let item = Main.setter(apts[i]);

                item.due_date = new Date(item.due_date);
                item.from_date = new Date(item.from_date);
                item.request_date = new Date(item.request_date);

                let year = item.from_date.getFullYear();
                let month = item.from_date.getMonth();
                let dia = item.from_date.getDate();
                let hora = item.from_date.getHours();
                let minutos= item.from_date.getMinutes();
                
                
                
                
                if(year==now.getFullYear() && month==now.getMonth() && dia==now.getDate() && hora==now.getHours() && minutos==now.getMinutes()){

                
                  let upd = {
                      status : 'cancelled',
                      _id : item._id
                  };
                  
                  Appointment.update(upd, (err, data) => {
                      if(!err) {
                          Alert.setConfirmAppt(item);
                          Alert.showConfirmAppt2();
                         item.status = 'cancelled';
                      }
                  });
              


                
              }

                

                
            }
        } else {
            // Alert.setInfo('No se pudieron obtener los datos', false, () => {});
            //
            // setTimeout(() => {
            //     Alert.showInfo();
            // }, 700);
            Alert.error(data);
        }
    });
};


    vm.init = function () {
      
    };
    $scope.$on("$viewContentLoaded", function () {
      vm.init();
      Main.setModule(vm.module);
    });
  })
  .constant("uiDatetimePickerConfig", {
    dateFormat: "dd-MM-yyyy HH:mm",
    html5Types: {
      date: "yyyy-MM-dd",
    },
    initialPicker: "date",
    reOpenDefault: "date",
    enableDate: true,
    enableTime: true,
    buttonBar: {
      show: true,
      now: {
        show: false,
      },
      today: {
        show: true,
        text: "Hoy",
        cls: "btn-sm btn-clear",
      },
      clear: {
        show: true,
        text: "Limpiar",
        cls: "btn-sm btn-clear",
      },
      date: {
        show: true,
        text: "Fecha",
        cls: "btn-sm btn-clear",
      },
      time: {
        show: true,
        text: "Hora",
        cls: "btn-sm btn-clear",
      },
      close: {
        show: true,
        text: "Cerrar",
        cls: "btn-sm btn-clear",
      },
      cancel: {
        show: false,
        text: "Cancelar",
        cls: "btn-sm btn-clear",
      },
    },
    timepickerOptions: {
      showmeridian: false,
    },
  });
app.controller("ProviderProfileC", function (
  Alert,
  Provider,
  CONFIG,
  Session,
  Main,
  AuthService,
  $scope,
  $state
) {
  var vm = this;
  vm.bounds = {};
  vm.bounds.left = 0;
  vm.bounds.right = 0;
  vm.bounds.top = 0;
  vm.bounds.bottom = 0;
  vm.firstAddress = "";
  vm.subscription = {};
  vm.cropper = {};
  vm.cropper.sourceImage = null;
  vm.cropper.croppedImage = null;
  vm.imageChanged = false;
  vm.credit_cards = [];
  vm.isImageChanged = function () {
    return vm.imageChanged;
  };
  vm.getRole = function () {
    return AuthService.getRole();
  };
  vm.module = "Perfil";
  vm.isEditable = function () {
    return vm.editable;
  };
  vm.edit = function () {
    vm.firstAddress = vm.editUser.data.address + "";
    vm.editable = true;
  };
  vm.getImage = function (_id) {
    if (_id == undefined) {
      return "img/user-default.png";
    }
    return CONFIG.backend_url + "image?id=" + _id;
  };
  vm.openImage = function () {
    if (vm.isEditable()) {
      $("#ImageFile").click();
      vm.imageChanged = true;
    }
  };
  vm.save = function () {
    if (!vm.editUser.data.name) {
      return Alert.error("Ingrese su nombre completo");
    }
    var testName = /^[a-zA-ZÀ-ü\s]*$/;
    if (!testName.test(vm.editUser.data.name)) {
      return Alert.error("El nombre debe contener solo letras.");
    }
    if (vm.editUser.data.name.replace(" ", "").length < 8) {
      return Alert.error("Su nombre completo debe tener al menos 8 caracteres");
    }
    if (vm.editUser.data.name.length > 100) {
      return Alert.error("Su nombre completo excede los 100 caracteres");
    }
    if (!vm.editUser.data.phone) {
      return Alert.error("Ingrese teléfono");
    }
    var testName = /^[0-9\s]*$/;
    if (!testName.test(vm.editUser.data.phone)) {
      return Alert.error("El teléfono debe contener solo números.");
    }
    var lat = localStorage.getItem("lat");
    var lng = localStorage.getItem("lng");
    var msn = localStorage.getItem("msn");
    if (lat === undefined || lng == undefined) {
      if (vm.firstAddress != AuthService.getUser().data.address) {
        return Alert.error(
          "Tu dirección no se ha encontrado en Google maps. Es necesario que ingreses una dirección valida."
        );
      }
    }
    if (msn != undefined) {
      Alert.setInfo(msn, false, function () {});
      Alert.showInfo();
      return;
    }
    vm.editUser.data.loc = [];
    vm.editUser.data.loc.push(lng);
    vm.editUser.data.loc.push(lat);
    Provider.update(vm.editUser, function (err, resp) {
      if (!err) {
        vm.editable = false;
        vm.imageChanged = false;
        localStorage.removeItem("lat");
        localStorage.removeItem("lng");
        localStorage.removeItem("msn");
        vm.editUser.data = resp.data;
        Alert.setInfo("Datos actualizados correctamente", true, function () {
          Session.setUser(vm.editUser);
        });
        setTimeout(function () {
          vm.imageChanged = false;
          Alert.showInfo();
          vm.init();
        }, 700);
      } else {
        Alert.error(resp);
      }
    });
  };
  vm.deleteCreditCard = function (_card) {
    Alert.confirm(
      "Atención",
      "\u00BFDesea eliminar la tarjeta " +
        _card.card_number +
        " como forma de pago? (Esta acci\u00F3n no se puede revertir)",
      {
        ok: Alert.getBasicButton("Aceptar", function () {
          Provider.deleteCreditCard(_card, function (err, deletedCardData) {
            if (!err) {
              Alert.info(
                "La tarjeta fue eliminada de manera permantente del sistema"
              );
              vm.init();
            } else {
              Alert.error(
                "No se pudo eliinar la tarjeta, intente de nuevo más tarde"
              );
            }
          });
        }),
        cancel: Alert.getCancelButton(function () {}),
      }
    );
  };
  vm.showDeleteCard = function () {
    return vm.editUser.credit_card.length === 1;
  };
  vm.newCreditCard = function () {
    $state.go("provider.newcreditcard");
  };
  vm.cancel = function () {
    vm.init();
  };
  vm.getSubscription = function () {
    return vm.subscription;
  };
  vm.init = function () {
    Main.setModule(vm.module);
    vm.editable = false;
    vm.imageChanged = false;
    vm.userId = AuthService.getUser().id;
    Provider.get(vm.userId, function (err, providerData) {
      if (!err) {
        vm.editUser = providerData.data;
        vm.subscription = providerData.subscription;
      } else {
        Alert.error("No se pudo conectar al servidor");
      }
    });
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.controller("ProviderServicesC", function (
  Main,
  Service,
  Alert,
  Appointment,
  $state,
  $scope
) {
  var vm = this;
  vm.module = "Servicios";
  vm.search = "";
  vm.cc_services = [];
  vm.appointments = [];
  vm.auxServices = [];
  vm.MoreServices = true;
  var timer = null;
  vm.order = "-registration_date";
  vm.getServices = function () {
    return vm.cc_services;
  };
  vm.countServices = function () {
    if (vm.getServices()) {
      return vm.getServices().length;
    } else {
      return 0;
    }
  };
  vm.myLocal = JSON.parse(localStorage.getItem("session.user"));
  vm.createService = function () {
    $state.go("provider.servicesCreate");
  };
  vm.loadServices = function (search, order) {
    vm.servicesPage = 1;
    vm.search = "";
    vm.cc_services = [];
    vm.counterLoaded = [];
    Service.getMyServices(
      {
        page: vm.servicesPage,
        search: search || "",
        order: order || "",
      },
      function (err, data) {
        var nServic = data.length;
        switch (vm.myLocal.membership) {
          case "free001":
            if (nServic >= 1) {
              vm.MoreServices = false;
            }
            break;
          case "plus001":
            if (nServic >= 10) {
              vm.MoreServices = false;
            }
            break;
          case "business001":
            if (nServic >= 20) {
              vm.MoreServices = false;
            }
            break;
          case "enterprises001":
            if (nServic >= 30) {
              vm.MoreServices = false;
            }
            break;
        }
        if (!err) {
          vm.cc_services = data;
        } else {
          Alert.setInfo(data.message, false);
          setTimeout(function () {
            Alert.showInfo();
          }, 500);
        }
      }
    );
  };
  vm.checkNextServices = function () {
    if (vm.counterLoaded.includes(vm.servicesPage)) {
      return;
    }
    vm.servicesPage++;
    vm.counterLoaded.push(vm.servicesPage);
    Service.getMyServices(
      {
        page: vm.servicesPage,
        search: vm.search || "",
        type: vm.type || "",
        sort: vm.sort || "",
      },
      function (err, response) {
        if (!err) {
          vm.cc_services = vm.cc_services.concat(response);
        } else {
          Alert.setInfo(response, false);
          setTimeout(function () {
            Alert.showInfo();
          }, 500);
        }
      }
    );
  };
  vm.searchServices = function (search) {
    vm.search = search;
    if (timer) {
      clearTimeout(timer);
      timer = null;
    }
    timer = setTimeout(function () {
      vm.loadServices(vm.search, vm.sort);
      vm.loadAppointments();
    }, 500);
  };
  vm.loadAppointments = function () {
    Appointment.getGroupFromProvider(
      {
        status: "accepted",
      },
      function (err, data) {
        if (!err) {
          vm.appointments = [];
          _.forEach(data, function (_item) {
            vm.appointments[_item._id] = _item.count;
          });
        }
      }
    );
  };
  vm.getAppointments = function (itemId) {
    var numberAppoinments = vm.appointments[itemId] || 0;
    if (!vm.auxServices.includes(itemId) && numberAppoinments != 0) {
      var appts = Main.appts;
      Main.appts = appts + numberAppoinments;
      vm.auxServices.push(itemId);
    }
    return numberAppoinments;
  };
  vm.init = function () {
    vm.search = "";
    vm.loadServices();
    vm.loadAppointments();
    Main.setModule(vm.module);
    Main.appts = 0;
  };
  $scope.$watch(
    function () {
      return Main.order;
    },
    function (newValue) {
      vm.order = newValue;
      vm.loadServices(vm.search, vm.order);
      vm.loadAppointments();
    }
  );
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
  $scope.$on("$destroy", function () {});
});
app.controller("ProviderServicesCreateC", function (
  Main,
  AuthService,
  CONFIG,
  Service,
  Alert,
  $state,
  $scope
) {
  var vm = this;
  vm.module = "Nuevo servicio";
  vm.bounds = {};
  vm.bounds.left = 0;
  vm.bounds.right = 0;
  vm.bounds.top = 0;
  vm.bounds.bottom = 0;
  vm.cropper = {};
  vm.cropper.sourceImage = null;
  vm.cropper.croppedImage = null;
  vm.editMode = false;
  vm.duration;
  vm.myLocal = JSON.parse(localStorage.getItem("session.user"));
  vm.service = {
    name: "",
    description: "",
    available: [],
    cost: null,
    payment_methods: [],
    priceType: undefined,
    categories: [],
    home_service: null,
    lender: null,
    space: null,
    provider_id: AuthService.getUser().id,
    minStart: 0,
    maxEnd: 0,
    home_servicedisable: false,
    spacedisable: false,
  };
  vm.validation = function () {
    switch (vm.myLocal.membership) {
      case "free001":
        vm.service.space = 1;
        vm.service.home_service = true;
        vm.service.home_servicedisable = true;
        vm.service.spacedisable;
        break;
      case "plus001":
        vm.service.space = 1;
        break;
      case "business001":
        break;
      case "enterprises001":
        break;
    }
  };
  vm.isImageChanged = function () {
    return vm.imageChanged;
  };
  var available = {
    days: [],
    start: null,
    end: null,
    removable: false,
    hours: [],
    valid: false,
  };
  vm.days = ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"];
  vm.getImage = function (_id) {
    if (_id == undefined) {
      return "img/user-default.png";
    }
    return CONFIG.backend_url + "image?id=" + _id;
  };
  vm.spaces = [1, 2, 4, 8, 10, 25, 50, 100, 200];
  vm.isEditable = function () {
    return vm.edit;
  };
  vm.validateAvailable = function () {
    var success = false;
    for (var i = 0; i < vm.service.available.length; i++) {
      var item = vm.service.available[i];
      if (item.start == null || item.end == null || item.days.length == 0)
        return false;
    }
    return true;
  };
  vm.validService = function () {
    return vm.validateAvailable() && vm.service.priceType != undefined
      ? vm.service.priceType == "price"
        ? vm.service.payment_methods.length > 0
        : true
      : false &&
          vm.service.categories.length > 0 &&
          vm.service.space > 0 &&
          vm.service.duration > 0 &&
          vm.service.image;
  };
  vm.hours = function () {
    var available = [];
    var min = 1;
    var max = 24;
    for (var i = min; i <= max; i++) available.push(i);
    return available;
  };
  vm.durationAvailable = function () {
    var available = [];
    available.push("5 minutos");
    available.push("10 minutos");
    available.push("15 minutos");
    available.push("20 minutos");
    available.push("30 minutos");
    available.push("1 hora");
    available.push("1.5 horas");
    available.push("2 horas");
    available.push("3 horas");
    return available;
  };
  vm.openImage = function () {
    $("#ImageFile").click();
  };
  $scope.$watch(
    function () {
      return vm.service.image;
    },
    function (newValue) {
      if (newValue && newValue.length > 300) {
        vm.imageChanged = true;
      }
    }
  );
  vm.selectCategory = function (filter) {
    if (vm.service.categories.includes(filter)) {
      vm.service.categories.splice(vm.service.categories.indexOf(filter), 1);
    } else {
      vm.service.categories.push(filter);
      if (vm.service.categories.length > 3) {
        vm.service.categories.splice(0, 1);
      }
    }
  };
  vm.selectSpace = function (space) {
    if (vm.service.space == space) {
      vm.service.space = 0;
    } else {
      vm.service.space = space;
    }
  };
  vm.selectDay = function (day, time) {
    var daysSelected = Main.parseDaysSeleted(time.days);
    if (daysSelected.includes(day)) {
      switch (day) {
        case "Mo":
          daysSelected[0] = "";
          break;
        case "Tu":
          daysSelected[1] = "";
          break;
        case "We":
          daysSelected[2] = "";
          break;
        case "Th":
          daysSelected[3] = "";
          break;
        case "Fr":
          daysSelected[4] = "";
          break;
        case "Sa":
          daysSelected[5] = "";
          break;
        case "Su":
          daysSelected[6] = "";
          break;
      }
    } else {
      switch (day) {
        case "Mo":
          daysSelected[0] = day;
          break;
        case "Tu":
          daysSelected[1] = day;
          break;
        case "We":
          daysSelected[2] = day;
          break;
        case "Th":
          daysSelected[3] = day;
          break;
        case "Fr":
          daysSelected[4] = day;
          break;
        case "Sa":
          daysSelected[5] = day;
          break;
        case "Su":
          daysSelected[6] = day;
          break;
      }
    }
    time.days = daysSelected;
    time.days = time.days.filter(function (x) {
      return x != "";
    });
  };
  vm.selectHour = function (hour, time) {
    if (time.hours.includes(hour)) {
      time.hours.splice(time.hours.indexOf(hour), 1);
    } else {
      if (time.hours.length === 2) {
        var shift = time.hours.shift();
      }
      time.hours.push(hour);
    }
    var theHours = Main.setter(time.hours);
    theHours = theHours.sort(function (a, b) {
      return a - b;
    });
    time.start = theHours[0] ? theHours[0] : null;
    time.end = theHours[0] ? theHours[1] : null;
    if (time.end > vm.service.maxEnd) {
      vm.service.maxEnd = time.end;
    }
    if (time.start < vm.service.minStart || vm.service.minStart === 0) {
      vm.service.minStart = time.start;
    }
  };
  vm.selectDuration = function () {
    var _hour = $("#select-duracion").val();
    if (_hour === undefined) {
      vm.service.duration = 0;
      vm.invalidServDur = true;
      vm.errorMessage = "Selecciona un horario disponible";
    } else {
      switch (_hour) {
        case "5 minutos":
          vm.service.duration = 5;
          break;
        case "10 minutos":
          vm.service.duration = 10;
          break;
        case "15 minutos":
          vm.service.duration = 15;
          break;
        case "20 minutos":
          vm.service.duration = 20;
          break;
        case "30 minutos":
          vm.service.duration = 30;
          break;
        case "1 hora":
          vm.service.duration = 60;
          break;
        case "1.5 horas":
          vm.service.duration = 60 * 1.5;
          break;
        case "2 horas":
          vm.service.duration = 60 * 2;
          break;
        case "3 horas":
          vm.service.duration = 60 * 3;
          break;
      }
    }
  };
  vm.selectTipoPago = function (filter) {
    if (vm.service.payment_methods.includes(filter)) {
      vm.service.payment_methods.splice(
        vm.service.payment_methods.indexOf(filter),
        1
      );
    } else {
      vm.service.payment_methods.push(filter);
    }
  };
  vm.selectTipoCosto = function (filter) {
    if (vm.service.priceType == filter) {
      vm.service.priceType = undefined;
    } else {
      vm.service.priceType = filter;
    }
  };
  vm.addAvailable = function (removable) {
    if (removable === void 0) {
      removable = false;
    }
    available.removable = removable;
    vm.service.available.push(Main.setter(available));
  };
  vm.removeAvailable = function (index) {
    vm.service.available.splice(index, 1);
  };
  vm.selectPeriod = function (per) {
    if (vm.service.period === per) {
      vm.service.period = null;
    } else {
      vm.service.period = per;
    }
  };
  vm.saveService = function () {
    vm.edit = false;
    var invalidDuration = false;
    var duration = vm.service.duration;
    for (var a = 0; a < vm.service.available.length; a++) {
      var av = vm.service.available[a];
      var durationAv = (av.end - av.start) * 60;
      if (durationAv < duration) {
        invalidDuration = true;
      }
    }
    if (invalidDuration) {
      Alert.error(
        "El horario de atención no cumple con la duración del servicio  seleccionada"
      );
      return;
    }
    Alert.setLoading("Guardando servicio");
    Alert.showLoading();
    Service.create(vm.service, function (err, data) {
      Alert.hideLoading();
      if (!err) {
        Alert.setInfo("Servicio guardado con éxito", true, function () {
          $state.go("provider.services");
        });
      } else {
        Alert.setInfo("No se pudo guardar el servicio", false);
      }
      setTimeout(function () {
        Alert.showInfo();
      }, 1000);
    });
  };
  vm.init = function () {
    Main.setModule(vm.module);
    vm.validation();
    vm.addAvailable();
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.controller("ProviderServicesDetailC", function (
  Appointment,
  Main,
  Service,
  $state,
  $stateParams,
  Alert,
  $scope
) {
  var vm = this;
  vm.module = "Detalle del servicio";
  vm.confirms = [];
  vm.service = null;
  vm.appointments = [];
  vm.datelocked = null;
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth() + 1;
  var yyyy = hoy.getFullYear();
  vm.today = (yyyy + "-" + mm + "-" + dd).toString();
  vm.observations = null;
  vm.edit = function () {
    $state.go("provider.servicesEdit", {
      serviceId: vm.service._id,
    });
  };
  vm.confirmDelete = function () {
    Alert.setConfirm(
      "¿Desea eliminar el servicio?",
      vm.service.name,
      {
        name: "Eliminar",
        action: function () {
          Service.delete(vm.service, function (err, data) {
            if (err) {
              Alert.setInfo(data || "No se pudo eliminar el servicio", false);
            } else {
              Alert.setInfo("Servicio eliminado", true, function () {
                $state.go("provider.services");
              });
            }
            setTimeout(function () {
              Alert.hideConfirm();
              Alert.showInfo();
            }, 1000);
          });
        },
      },
      {
        name: "Cancelar",
        action: function () {
          Alert.hideConfirm();
        },
      }
    );
    setTimeout(function () {
      Alert.showConfirm();
    }, 500);
  };
  vm.confirmCancel = function (_appt) {
    Alert.setConfirm(
      "¿Desea cancelar la cita?",
      vm.service.name,
      {
        name: "Si",
        action: function () {
          var upd = {
            status: "cancelled",
            _id: _appt._id,
          };
          Alert.hideConfirm();
          Appointment.update(upd, function (err, data) {
            if (!err) {
              Alert.setConfirmAppt(_appt);
              Alert.showConfirmAppt();
              _appt.status = "cancelled";
            }
          });
        },
      },
      {
        name: "No",
        action: function () {
          Alert.hideConfirm();
        },
      }
    );
    setTimeout(function () {
      Alert.showConfirm();
    }, 500);
  };
  vm.accept = function (_appt) {
    var upd = {
      status: "accepted",
      _id: _appt._id,
    };
    Appointment.update(upd, function (err, data) {
      if (!err) {
        Alert.setConfirmAppt(_appt);
        Alert.showConfirmAppt();
        _appt.status = "accepted";
      }
      Alert.hideConfirm();
    });
  };
  vm.loadService = function () {
    Service.getService($stateParams.serviceId, function (err, data) {
      if (!err) {
        vm.service = data;
        vm.service.translateDuration = Main.translateDuration(
          vm.service.duration
        );
      } else {
        Alert.setInfo(
          "El servicio no se encuentra disponible",
          false,
          function () {
            $state.go("provider.services");
          }
        );
        setTimeout(function () {
          Alert.showInfo();
        }, 500);
      }
    });
  };
  vm.changedays = function ($event) {
    var curDay = vm.datelocked.toString().substring(0, 2);
    vm.av = [];
    vm.hoursConfig = [];
    vm.availableHours = [];
    for (var i = 0; i < vm.service.available.length; i++) {
      if (vm.service.available[i].days.includes(curDay)) {
        vm.av.push(vm.service.available[i]);
      }
    }
    for (var l = 0; l < vm.av.length; l++) {
      vm.hourAux = [];
      var av = vm.av[l];
      var avStart = av.start;
      var avEnd = av.end;
      for (var i = avStart; i <= avEnd; i++) {
        if (!vm.hourAux.includes(i)) vm.hourAux.push(i);
      }
      vm.hoursConfig.push(vm.hourAux);
    }
    for (var horarios = 0; horarios < vm.hoursConfig.length; horarios++) {
      var horario = vm.hoursConfig[horarios];
      var allDurationMinutes = (horario.length - 1) * 60;
      var numberOptionsDisponibility = Math.floor(
        allDurationMinutes / vm.service.duration
      );
      var hourAux = horario[0];
      var increaseTime = Main.translateDurationToHours(vm.service.duration);
      for (var h = 0; h < numberOptionsDisponibility; h++) {
        if (!vm.availableHours.includes(hourAux))
          vm.availableHours.push(hourAux);
        hourAux += increaseTime;
      }
    }
  };
  vm.hourSelect = function (fecha) {
    var checks = document.getElementsByTagName("INPUT");
    var checkOP = [];
    var appointmentj = {
      status: "locked",
      client: null,
      service: null,
      provider: null,
      from_date: null,
      due_date: null,
      request_date: null,
      observations: null,
    };
    var citabloqueada;
    for (var i = 0; checks[i]; ++i) {
      appointmentj = {
        status: "locked",
        client: null,
        service: null,
        provider: null,
        from_date: null,
        due_date: null,
        request_date: null,
        observations: null,
      };
      appointmentj.service = vm.service._id;
      appointmentj.request_date = new Date();
      appointmentj.provider = vm.service.provider._id;
      appointmentj.observations = vm.observations;
      if (checks[i].checked) {
        var valueCheck = +checks[i].value;
        if (valueCheck < 25) {
          citabloqueada = new Date(vm.datelocked);
          var hour = Math.floor(valueCheck);
          var minutes = (valueCheck - hour) * 60;
          citabloqueada.setMinutes(minutes);
          citabloqueada.setHours(hour);
          appointmentj.from_date = citabloqueada;
          appointmentj.due_date = citabloqueada;
          checkOP.push(appointmentj);
        }
      }
    }
    for (i = 0; i < checkOP.length; i++) {
      Appointment.blockAppointment(checkOP[i], function (err, data) {
        if (!err) {
        }
      });
      var nArray = checkOP.length - 1;
      if (nArray == i) {
        location.reload();
      }
    }
  };
  vm.loadAppointments = function () {
    Appointment.getFromService($stateParams.serviceId, function (err, data) {
      if (!err) {
        vm.appointments = data;
        for (var app_1 = 0; vm.appointments.docs.length; app_1++) {
          vm.appointments.docs[app_1].isExpired =
            new Date(vm.appointments.docs[app_1].from_date) < new Date();
        }
      } else {
        vm.appointments = [];
      }
    });
  };
  vm.selectall = function (div) {
    var checks = document.getElementsByTagName("INPUT");
    var checkOP = null;
    var saltar = checks[1].checked;
    if (saltar) {
      checkOP = true;
    } else {
      checkOP = false;
    }
    for (var i = 0; checks[i]; ++i) {
      if (checks[i].type == "checkbox") checks[i].checked = checkOP;
    }
  };
  vm.init = function () {
    Main.setModule(vm.module);
    if (!$stateParams.serviceId) {
      $state.go("provider.services");
    } else {
      vm.loadService();
      vm.loadAppointments();
    }
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
  $scope.$on("$destroy", function () {});
});
app.controller("ProviderServicesEditC", function (
  Main,
  AuthService,
  CONFIG,
  Service,
  Alert,
  $state,
  $stateParams,
  $scope
) {
  var vm = this;
  vm.module = "Editar servicio";
  vm.bounds = {};
  vm.bounds.left = 0;
  vm.bounds.right = 0;
  vm.bounds.top = 0;
  vm.bounds.bottom = 0;
  vm.cropper = {};
  vm.cropper.sourceImage = null;
  vm.cropper.croppedImage = null;
  vm.editMode = true;
  vm.getImage = function (_id) {
    if (_id == undefined) {
      return "img/user-default.png";
    }
    return CONFIG.backend_url + "image?id=" + _id;
  };
  vm.myLocal = JSON.parse(localStorage.getItem("session.user"));
  vm.service = {
    name: "",
    description: "",
    available: [],
    cost: null,
    payment_methods: [],
    priceType: undefined,
    categories: [],
    home_service: null,
    lender: null,
    space: 0,
    period: null,
    provider_id: AuthService.getUser().id,
    minStart: 0,
    maxEnd: 0,
    home_servicedisable: false,
    spacedisable: false,
  };
  vm.validation = function () {
    switch (vm.myLocal.membership) {
      case "free001":
        setTimeout(function () {
          vm.service.space = 1;
          vm.service.home_service = false;
          vm.service.home_servicedisable = true;
          vm.service.spacedisable = true;
          $("#DomFalse").prop("checked", true);
        }, 100);
        break;
      case "plus001":
        setTimeout(function () {
          vm.service.space = 1;
          vm.service.spacedisable = true;
        }, 100);
        break;
      case "business001":
        break;
      case "enterprises001":
        break;
    }
  };
  var available = {
    days: [],
    start: null,
    end: null,
    removable: false,
    hours: [],
    valid: false,
  };
  vm.days = ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"];
  vm.spaces = [1, 2, 4, 8, 10, 25, 50, 100, 200];
  vm.isEditable = function () {
    return vm.edit;
  };
  vm.isImageChanged = function () {
    return vm.imageChanged;
  };
  vm.hours = function () {
    var available = [];
    var min = 1;
    var max = 24;
    for (var i = min; i <= max; i++) available.push(i);
    return available;
  };
  vm.isEdit = true;
  vm.validateAvailable = function () {
    var success = false;
    for (var i = 0; i < vm.service.available.length; i++) {
      var item = vm.service.available[i];
      if (item.start == null || item.end == null || item.days.length == 0)
        return false;
    }
    return true;
  };
  vm.validService = function () {
    return vm.validateAvailable() && vm.service.priceType != undefined
      ? vm.service.priceType == "price"
        ? vm.service.payment_methods.length > 0
        : true
      : false &&
          vm.service.categories.length > 0 &&
          vm.service.space > 0 &&
          vm.service.duration > 0 &&
          vm.service.image;
  };
  vm.openImage = function () {
    $("#ImageFile").click();
  };
  $scope.$watch(
    function () {
      return vm.service.image;
    },
    function (newValue) {
      if (newValue && newValue.length > 300) {
        vm.imageChanged = true;
      }
    }
  );
  vm.durationAvailable = function () {
    var available = [];
    available.push("5 minutos");
    available.push("10 minutos");
    available.push("15 minutos");
    available.push("20 minutos");
    available.push("30 minutos");
    available.push("1 hora");
    available.push("1.5 horas");
    available.push("2 horas");
    available.push("3 horas");
    return available;
  };
  vm.selectCategory = function (filter) {
    if (vm.service.categories.includes(filter)) {
      vm.service.categories.splice(vm.service.categories.indexOf(filter), 1);
    } else {
      vm.service.categories.push(filter);
      if (vm.service.categories.length > 3) {
        vm.service.categories.splice(0, 1);
      }
    }
  };
  vm.selectSpace = function (space) {
    if (vm.service.space == space) {
      vm.service.space = 0;
    } else {
      vm.service.space = space;
    }
  };
  vm.selectDay = function (day, time) {
    var daysSelected = Main.parseDaysSeleted(time.days);
    if (daysSelected.includes(day)) {
      switch (day) {
        case "Mo":
          daysSelected[0] = "";
          break;
        case "Tu":
          daysSelected[1] = "";
          break;
        case "We":
          daysSelected[2] = "";
          break;
        case "Th":
          daysSelected[3] = "";
          break;
        case "Fr":
          daysSelected[4] = "";
          break;
        case "Sa":
          daysSelected[5] = "";
          break;
        case "Su":
          daysSelected[6] = "";
          break;
      }
    } else {
      switch (day) {
        case "Mo":
          daysSelected[0] = day;
          break;
        case "Tu":
          daysSelected[1] = day;
          break;
        case "We":
          daysSelected[2] = day;
          break;
        case "Th":
          daysSelected[3] = day;
          break;
        case "Fr":
          daysSelected[4] = day;
          break;
        case "Sa":
          daysSelected[5] = day;
          break;
        case "Su":
          daysSelected[6] = day;
          break;
      }
    }
    time.days = daysSelected;
    time.days = time.days.filter(function (x) {
      return x != "";
    });
  };
  vm.selectHour = function (hour, time) {
    if (time.hours.includes(hour)) {
      time.hours.splice(time.hours.indexOf(hour), 1);
    } else {
      if (time.hours.length === 2) {
        var shift = time.hours.shift();
      }
      time.hours.push(hour);
    }
    var theHours = Main.setter(time.hours);
    theHours = theHours.sort(function (a, b) {
      return a - b;
    });
    time.start = theHours[0] ? theHours[0] : null;
    time.end = theHours[0] ? theHours[1] : null;
  };
  vm.selectDuration = function () {
    var _hour = $("#select-duracion").val();
    if (_hour === undefined) {
      vm.service.duration = 0;
      vm.invalidServDur = true;
      vm.errorMessage = "Selecciona un horario disponible";
    } else {
      switch (_hour) {
        case "5 minutos":
          vm.service.duration = 5;
          break;
        case "10 minutos":
          vm.service.duration = 10;
          break;
        case "15 minutos":
          vm.service.duration = 15;
          break;
        case "20 minutos":
          vm.service.duration = 20;
          break;
        case "30 minutos":
          vm.service.duration = 30;
          break;
        case "1 hora":
          vm.service.duration = 60;
          break;
        case "1.5 horas":
          vm.service.duration = 60 * 1.5;
          break;
        case "2 horas":
          vm.service.duration = 60 * 2;
          break;
        case "3 horas":
          vm.service.duration = 60 * 3;
          break;
      }
    }
  };
  vm.selectTipoPago = function (filter) {
    if (vm.service.payment_methods.includes(filter)) {
      vm.service.payment_methods.splice(
        vm.service.payment_methods.indexOf(filter),
        1
      );
    } else {
      vm.service.payment_methods.push(filter);
    }
  };
  vm.selectTipoCosto = function (filter) {
    if (vm.service.priceMethods == filter) {
      vm.service.priceType = undefined;
    } else {
      vm.service.priceType = filter;
    }
  };
  vm.addAvailable = function (removable) {
    if (removable === void 0) {
      removable = false;
    }
    available.removable = removable;
    vm.service.available.push(Main.setter(available));
  };
  vm.removeAvailable = function (index) {
    vm.service.available.splice(index, 1);
  };
  vm.selectPeriod = function (per) {
    if (vm.service.period === per) {
      vm.service.period = null;
    } else {
      vm.service.period = per;
    }
  };
  vm.saveService = function () {
    var invalidDuration = false;
    var duration = vm.service.duration;
    for (var a = 0; a < vm.service.available.length; a++) {
      var av = vm.service.available[a];
      var durationAv = (av.end - av.start) * 60;
      if (durationAv < duration) {
        invalidDuration = true;
      }
    }
    if (invalidDuration) {
      Alert.error(
        "El horario de atención no cumple con la duración del servicio  seleccionada"
      );
      return;
    }
    Alert.setLoading("Guardando servicio");
    Alert.showLoading();
    Service.update(vm.service, function (err, response) {
      Alert.hideLoading();
      if (!err) {
        Alert.setInfo("Servicio guardado con éxito", true, function () {
          $state.go("provider.servicesDetail", {
            serviceId: $stateParams.serviceId,
          });
        });
      } else {
        Alert.setInfo("No se pudo guardar el servicio", false);
      }
      setTimeout(function () {
        Alert.showInfo();
      }, 1000);
    });
  };
  vm.init = function () {
    vm.edit = true;
    vm.imageChanged = false;
    if (!$stateParams.serviceId) {
      $state.go("provider.services");
    }
    Service.getService($stateParams.serviceId, function (err, data) {
      if (!err) {
        vm.service = data;
        var servResp = Main.setter(vm.service);
        var servAv = servResp.available;
        vm.service.available = [];
        for (var i = 0; i < servAv.length; i++) {
          var av = Main.setter(available);
          av.days = servAv[i].days;
          av.start = servAv[i].start;
          av.end = servAv[i].end;
          vm.service.available.push(av);
        }
        vm.duration = Main.translateDuration(vm.service.duration);
        setTimeout(function () {
          if (vm.service.home_service) {
            $("#DomTrue").prop("checked", true);
          } else {
            $("#DomFalse").prop("checked", true);
          }
        }, 500);
      } else {
        Alert.setInfo(
          "El servicio no se encuentra disponible",
          false,
          function () {
            $state.go("provider.services");
          }
        );
        setTimeout(function () {
          Alert.showInfo();
        }, 500);
      }
    });
    Main.setModule(vm.module);
    vm.validation();
    vm.addAvailable();
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.controller("ProviderSheduleC", function (
  Appointment,
  AuthService,
  Alert,
  EventService,
  Main,
  $scope,
  $state,
  uiCalendarConfig
) {
  var vm = this;
  vm.module = "Agenda";
  vm.appointments = [];
  vm.getAppointments = function () {
    return vm.appointments;
  };
  vm.dt = new Date();
  vm.monthOptions = {
    showWeeks: false,
    maxMode: "day",
  };
  vm.yearOptions = {
    showWeeks: false,
    datepickerMode: "month",
    minMode: "month",
  };
  var daysOfMonth = function (year, month) {
    return new Date(year, month + 1, 0).getDate();
  };
  vm.getStatusTranslate = function (status) {
    switch (status) {
      case "cancelled":
        return "Cancelada";
      case "accepted":
        return "Aceptada";
      case "locked":
        return "Bloqueada";
    }
  };
  vm.checkIfIsOpacity = function (date) {
    return date < new Date();
  };
  vm.setMonth = function (num) {
    var day = 1,
      month = num,
      year = vm.dt.getFullYear();
    vm.dt = new Date(year, month, day);
  };
  vm.prevMonth = function () {
    var prevMonth = new Date(vm.dt.getFullYear(), vm.dt.getMonth(), 0);
    prevMonth.setDate(1);
    vm.dt = prevMonth;
  };
  vm.nextMonth = function () {
    var nextMonth = new Date(vm.dt.getFullYear(), vm.dt.getMonth() + 2, 0);
    nextMonth.setDate(1);
    vm.dt = nextMonth;
  };
  vm.prevYear = function () {
    var year = vm.dt.getFullYear() - 1;
    var days = vm.dt.getDate();
    var month = vm.dt.getMonth();
    vm.dt = new Date(year, month, days);
  };
  vm.nextYear = function () {
    var year = vm.dt.getFullYear() + 1;
    var days = vm.dt.getDate();
    var month = vm.dt.getMonth();
    vm.dt = new Date(year, month, days);
  };
  vm.getPrevMonth = function () {
    var prevMonth = new Date(vm.dt);
    prevMonth.setDate(-1);
    return prevMonth;
  };
  vm.getNextMonth = function () {
    var nextMonth = new Date(vm.dt);
    var days =
      daysOfMonth(vm.dt.getFullYear(), vm.dt.getMonth()) - vm.dt.getDate() + 1;
    nextMonth.setDate(days + vm.dt.getDate());
    return nextMonth;
  };
  vm.getPrevYear = function () {
    return new Date(vm.dt.getFullYear(), 0, 0);
  };
  vm.getNextYear = function () {
    return new Date(vm.dt.getFullYear() + 2, 0, 0);
  };
  vm.getEventCount = function (mont) {
    var year = vm.dt.getFullYear();
    if (vm.appointments[year]) {
      if (vm.appointments[year][mont]) {
        return vm.appointments[year][mont].length;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  };
  vm.getCurrentAppointments = function () {
    var year = vm.dt.getFullYear();
    var mont = vm.dt.getMonth();
    if (vm.appointments[year]) {
      if (vm.appointments[year][mont]) {
        return vm.appointments[year][mont];
      } else {
        return [];
      }
    } else {
      return [];
    }
  };
  vm.getAppointmentDetail = function (_appt) {
    $state.go("provider.servicesDetail", {
      serviceId: _appt.service._id,
    });
  };
  vm.goNewEvent = function () {
    $state.go("provider.newEvent");
  };
  vm.goNewBlock = function () {
    $state.go("provider.newBlock");
  };
  vm.loadAppointments = function () {
    Appointment.getAllFromProvider(
      {
        status: "accepted",
      },
      function (err, data) {
        if (!err) {
          var apts = data;
          for (var i = 0; i < apts.length; i++) {
            var item = Main.setter(apts[i]);
            item.due_date = new Date(item.due_date);
            item.from_date = new Date(item.from_date);
            item.request_date = new Date(item.request_date);
            var year = item.from_date.getFullYear();
            var month = item.from_date.getMonth();
            var dia = item.from_date.getDate();
            if (!vm.appointments[year]) {
              vm.appointments[year] = [];
            }
            if (!vm.appointments[year][month]) {
              vm.appointments[year][month] = [];
            }
            vm.appointments[year][month].push(item);
            var eventItem = Main.setter(item);
            
            var eventClass = "appointment";
            switch (eventItem.status) {
              case "accepted":
                eventClass += " appointment-success";
                break;
              case "pending":
                eventClass += " appointment-warning";
                break;
              case "cancelled":
                eventClass += " appointment-danger";
                break;
            }
            var newEvent = {
              title: item.service.name,
              start: item.from_date,
              end: item.due_date,
              stick: true,
              className: [eventClass],
              url: "#!/provider/services/detail/" + item.service._id,
            };
            vm.events.push(newEvent);
            vm.activeCalendar();
          }
        } else {
          Alert.error(data);
        }
      }
    );
  };
  vm.loadEvents = function () {
    EventService.getAll(function (error, response) {
      if (!error) {
        var eventData = response.data.data;
        
        eventData.forEach(function (_event) {
          var newEvent = {
            title: _event.name,
            start: new Date(_event.from_date),
            end: new Date(_event.due_date),
            stick: true,
            className: ["appointment appointment-event"],
          };
          vm.events.push(newEvent);
        });
      } else {
        Alert.error(response);
      }
    });
  };
  vm.init = function () {
    Main.setModule(vm.module);
    vm.loadAppointments();
    vm.loadEvents();
  };
  vm.events = [];
  vm.eventSources = [vm.events];
  vm.uiConfig = {
    calendar: {
      height: 450,
      editable: false,
      header: {
        left: "month,agendaWeek,agendaDay",
        center: "title",
        right: "today prev,next",
      },
    },
  };
  vm.activeCalendar = function () {
    
    setTimeout(function () {
      if (uiCalendarConfig.calendars["calendarioUno"]) {
        uiCalendarConfig.calendars["calendarioUno"].fullCalendar("render");
      }
    });
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
    
  });
});
app.controller("SigninProviderC", function (
  $scope,
  $location,
  $state,
  Alert,
  Main,
  AuthService,
  Admins,
  Service,
  RandomString,
  UiOpenpay
) {
  window.emojiPicker.discover();
  var vm = this;
  vm.categories = [];
  vm.provider = {};
  vm.credit_card = {};
  vm.isTarjeta = true;
  vm.isReferencia = false;
  vm.membershiptype = "";
  vm.loadMemebership = function () {
    Admins.getMemberships(function (err, data) {
      if (!err) {
        vm.Memebership = data;
      } else {
      }
    });
  };
  vm.saveProviderAndServiceNoPay = function () {
    var testName = /^[a-zA-ZÀ-ü\s]*$/;
    var testPhone = /^[0-9\s]*$/;
    if (!vm.provider.name) {
      return Alert.error("Ingrese su nombre completo");
    }
    if (!testName.test(vm.provider.name)) {
      return Alert.error("El nombre debe contener solo letras.");
    }
    if (vm.provider.name.replace(" ", "").length < 8) {
      return Alert.error("Su nombre completo debe tener al menos 8 caracteres");
    }
    if (vm.provider.name.length > 100) {
      return Alert.error("Su nombre completo excede los 100 caracteres");
    }
    if (!vm.provider.phone) {
      return Alert.error("Ingrese teléfono");
    }
    if (!testPhone.test(vm.provider.phone)) {
      return Alert.error("El teléfono debe contener solo números.");
    }
    var lat = localStorage.getItem("lat");
    var lng = localStorage.getItem("lng");
    var msn = localStorage.getItem("msn");
    if (lat === undefined || lng === undefined) {
      return Alert.error(
        "Tu dirección no se ha encontrado en Google maps. Es necesario que ingreses una dirección valida."
      );
    }
    if (msn) {
      Alert.error(msn);
      return;
    }
    var provider = Main.setter(vm.provider);
    provider.pass = md5(provider.password);
    provider.email = provider.email.toLowerCase();
    UiOpenpay.init();
    AuthService.signinProviderNoPay(
      {
        provider: provider,
        creditCard: null,
      },
      function (err, response) {
        if (!err) {
          Alert.info("Registro exitoso");
          localStorage.removeItem("lat");
          localStorage.removeItem("lng");
          localStorage.removeItem("msn");
          setTimeout(function () {
            $state.go("login");
          }, 500);
        } else {
          Alert.error(response);
        }
      }
    );
  };
  vm.saveProviderAndService = function () {
    if (vm.isTarjeta) {
      var testName = /^[a-zA-ZÀ-ü\s]*$/;
      var testPhone = /^[0-9\s]*$/;
      if (!vm.provider.name) {
        return Alert.error("Ingrese su nombre completo");
      }
      if (!testName.test(vm.provider.name)) {
        return Alert.error("El nombre debe contener solo letras.");
      }
      if (vm.provider.name.replace(" ", "").length < 8) {
        return Alert.error(
          "Su nombre completo debe tener al menos 8 caracteres"
        );
      }
      if (vm.provider.name.length > 100) {
        return Alert.error("Su nombre completo excede los 100 caracteres");
      }
      if (!vm.provider.phone) {
        return Alert.error("Ingrese teléfono");
      }
      if (!testPhone.test(vm.provider.phone)) {
        return Alert.error("El teléfono debe contener solo números.");
      }
      var lat = localStorage.getItem("lat");
      var lng = localStorage.getItem("lng");
      var msn = localStorage.getItem("msn");
      if (lat === undefined || lng === undefined) {
        return Alert.error(
          "Tu dirección no se ha encontrado en Google maps. Es necesario que ingreses una dirección valida."
        );
      }
      if (msn) {
        Alert.error(msn);
        return;
      }
      var provider_1 = Main.setter(vm.provider);
      provider_1.pass = md5(provider_1.password);
      provider_1.email = provider_1.email.toLowerCase();
      UiOpenpay.init();
      UiOpenpay.getTokencard("siginin-form")
        .then(function (response) {
          AuthService.signinProvider(
            {
              provider: provider_1,
              creditCard: response,
            },
            function (err, response) {
              if (!err) {
                Alert.info("Registro exitoso");
                localStorage.removeItem("lat");
                localStorage.removeItem("lng");
                localStorage.removeItem("msn");
                setTimeout(function () {
                  $state.go("login");
                }, 500);
              } else {
                Alert.error(response);
              }
            }
          );
        })
        .catch(function (errorResponse) {
          var descrption = errorResponse.data.description
            ? errorResponse.data.description
            : errorResponse.message;
          var errorMessage =
            "ERROR - " + errorResponse.status + " - " + descrption;
          Alert.error(errorMessage);
        });
    } else {
      vm.saveProviderAndServiceReference(true);
    }
  };
  vm.saveProviderAndServiceReference = function (n) {
    var testName = /^[a-zA-ZÀ-ü\s]*$/;
    var testPhone = /^[0-9\s]*$/;
    if (!vm.provider.name) {
      return Alert.error("Ingrese su nombre completo");
    }
    if (!testName.test(vm.provider.name)) {
      return Alert.error("El nombre debe contener solo letras.");
    }
    if (vm.provider.name.replace(" ", "").length < 8) {
      return Alert.error("Su nombre completo debe tener al menos 8 caracteres");
    }
    if (vm.provider.name.length > 100) {
      return Alert.error("Su nombre completo excede los 100 caracteres");
    }
    if (!vm.provider.phone) {
      return Alert.error("Ingrese teléfono");
    }
    if (!vm.membershiptype) {
      
      return Alert.error("seleccione paquetee");
    }
    vm.provider.membershiptype = vm.membershiptype;
    
    if (!testPhone.test(vm.provider.phone)) {
      return Alert.error("El teléfono debe contener solo números.");
    }
    var lat = localStorage.getItem("lat");
    var lng = localStorage.getItem("lng");
    var msn = localStorage.getItem("msn");
    if (lat === undefined || lng === undefined) {
      return Alert.error(
        "Tu dirección no se ha encontrado en Google maps. Es necesario que ingreses una dirección valida."
      );
    }
    if (msn) {
      Alert.error(msn);
      return;
    }
    var provider = Main.setter(vm.provider);
    provider.pass = md5(provider.password);
    provider.email = provider.email.toLowerCase();
    UiOpenpay.init();
    UiOpenpay.getTokencardReference("siginin-form")
      .then(function (response) {
        AuthService.signinProviderReference(
          {
            provider: provider,
            creditCard: response,
          },
          function (err, response) {
            if (!err) {
              
              
              Alert.info(
                "Registro exitoso, recuerda imprimir tu ticket y realizar el pago"
              );
              localStorage.removeItem("lat");
              localStorage.removeItem("lng");
              localStorage.removeItem("msn");
              setTimeout(function () {
                if (n) {
                  $state.go("login");
                  window.open(
                    "https://dashboard.openpay.mx/spei-pdf/mppjvaydveftfzpybktj/" +
                      response.data.referencia.id,
                    "_blank"
                  );
                } else {
                  $state.go("admin.listProvider");
                  window.open(
                    "https://dashboard.openpay.mx/spei-pdf/mppjvaydveftfzpybktj/" +
                      response.data.referencia.id,
                    "_blank"
                  );
                }
              }, 500);
            } else {
              Alert.error(response.message);
            }
          }
        );
      })
      .catch(function (errorResponse) {
        var descrption = errorResponse.data.description
          ? errorResponse.data.description
          : errorResponse.message;
        var errorMessage =
          "ERROR - " + errorResponse.status + " - " + descrption;
        Alert.error(errorMessage);
      });
  };
  vm.saveProviderAndServiceAdmin = function () {
    if (vm.isTarjeta) {
      
      var testName = /^[a-zA-ZÀ-ü\s]*$/;
      var testPhone = /^[0-9\s]*$/;
      if (!vm.provider.name) {
        return Alert.error("Ingrese su nombre completo");
      }
      if (!testName.test(vm.provider.name)) {
        return Alert.error("El nombre debe contener solo letras.");
      }
      if (vm.provider.name.replace(" ", "").length < 8) {
        return Alert.error(
          "Su nombre completo debe tener al menos 8 caracteres"
        );
      }
      if (vm.provider.name.length > 100) {
        return Alert.error("Su nombre completo excede los 100 caracteres");
      }
      if (!vm.provider.phone) {
        return Alert.error("Ingrese teléfono");
      }
      if (!vm.membershiptype) {
        
        return Alert.error("seleccione paquetee");
      }
      vm.provider.membershiptype = vm.membershiptype;
      if (!vm.provider.membershiptype) {
        return Alert.error("seleccione paquete");
      }
      if (!testPhone.test(vm.provider.phone)) {
        return Alert.error("El teléfono debe contener solo números.");
      }
      var lat = localStorage.getItem("lat");
      var lng = localStorage.getItem("lng");
      var msn = localStorage.getItem("msn");
      if (lat === undefined || lng === undefined) {
        return Alert.error(
          "Tu dirección no se ha encontrado en Google maps. Es necesario que ingreses una dirección valida."
        );
      }
      if (msn) {
        Alert.error(msn);
        return;
      }
      var provider_2 = Main.setter(vm.provider);
      provider_2.pass = md5(provider_2.password);
      provider_2.email = provider_2.email.toLowerCase();
      UiOpenpay.init();
      UiOpenpay.getTokencard("siginin-form")
        .then(function (response) {
          AuthService.signinProviderCreditCard(
            {
              provider: provider_2,
              creditCard: response,
            },
            function (err, response) {
              if (!err) {
                Alert.info("Registro exitoso");
                localStorage.removeItem("lat");
                localStorage.removeItem("lng");
                localStorage.removeItem("msn");
                setTimeout(function () {
                  $state.go("admin.listProvider");
                }, 500);
              } else {
                Alert.error(response);
              }
            }
          );
        })
        .catch(function (errorResponse) {
          var descrption = errorResponse.data.description
            ? errorResponse.data.description
            : errorResponse.message;
          var errorMessage =
            "ERROR - " + errorResponse.status + " - " + descrption;
          Alert.error(errorMessage);
        });
    } else {
      
      vm.saveProviderAndServiceReference(false);
    }
  };
  vm.sendNotification = function () {
    let title = vm.notification.title || "";
    let body = vm.notification.message || "";
    let params = {
      title, body
    }
    
    
    vm.saveNotification(params);
  };
  vm.saveNotification = function (params = {}) {
    Admins.saveNotification(
      params,
      function (err, response) {
        if (!err) {
          
          $state.go("notifications");
        } else {
          Alert.setInfo("Error de conexión", false);
          setTimeout(function () {
            Alert.showInfo();
          }, 500);
        }
      }
    );
  };
  vm.tarjeta = function () {
    
    if (vm.isTarjeta) {
      document.getElementsByName("referenciabank1").checked = true;
      document.getElementsByName("referenciabank2").checked = false;
      vm.isTarjeta = true;
      vm.isReferencia = false;
    } else {
      document.getElementsByName("referenciabank1").checked = false;
      document.getElementsByName("referenciabank2").checked = true;
      vm.isTarjeta = false;
      vm.isReferencia = true;
    }
  };
  vm.referencia = function () {
    
    if (vm.isReferencia) {
      document.getElementsByName("referenciabank1").checked = false;
      document.getElementsByName("referenciabank2").checked = true;
      vm.isTarjeta = false;
      vm.isReferencia = true;
    } else {
      document.getElementsByName("referenciabank1").checked = true;
      document.getElementsByName("referenciabank2").checked = false;
      vm.isTarjeta = true;
      vm.isReferencia = false;
    }
  };
  vm.back = function () {
    
    $state.go("admin.listProvider");
  };
  vm.goLogin = function () {
    $state.go("login", {
      action: "GET",
      login: true,
    });
  };
  vm.getNotifications = function () {
    vm.notifications = [];
    Admins.getNotifications(
      function (err, response) {
        if (!err) {
          
          vm.notifications = response.data;
        } else {
          Alert.setInfo("Error de conexión", false);
          setTimeout(function () {
            Alert.showInfo();
          }, 500);
        }
      }
    );
  };
  vm.init = function () {
    vm.getNotifications();
    if ($location.search().name) {
      vm.provider.name = $location.search().name;
    }
    if ($location.search().email) {
      vm.provider.email = $location.search().email;
    }
    vm.loadMemebership();
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.controller("ClientActiveC", function (
  $location,
  $scope,
  $state,
  Client,
  $uibModal
) {
  var vm = this;
  vm.message = "Todo bien?";
  vm.activeClient = function (userId, code) {
    Client.activeClient(userId, code, function (err, response) {
      vm.message = response;
    });
  };
  vm.showTerms = function () {
    $uibModal.open({
      templateUrl: "views/main/modals/terms.html",
      controller: "TermsInstance as md",
      size: "lg",
    });
  };
  vm.goLogin = function () {
    $state.go("login");
  };
  vm.init = function () {
    var userId = $location.search().id;
    var code = $location.search().code;
    vm.activeClient(userId, code);
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.controller("UserAppointmentC", function (
  Alert,
  AuthService,
  Appointment,
  Main,
  Service,
  EventService,
  $state,
  $stateParams,
  $scope
) {
  var vm = this;
  vm.module = "Fecha de su cita ";
  var convertToMili = 24 * 60 * 60 * 1000;
  vm.appointment = {
    client_id: null,
    service_id: null,
    provider_id: null,
    from_date: null,
    due_date: null,
  };
  vm.selectedHour;
  vm.hourAux = [];
  vm.appointments = [];
  vm.availableHours = [];
  vm.invalidAppHour = true;
  vm.invalidAppDate = true;
  vm.errorMessage;
  vm.vDate = new Date();
  vm.dt = new Date();
  vm.monthOptions = {
    showWeeks: false,
    maxMode: "day",
    minDate: new Date(),
  };
  vm.av = null;
  vm.disableMonth = function (num) {
    var year = vm.dt.getFullYear();
    if (
      num < vm.monthOptions.minDate.getMonth() ||
      year < vm.monthOptions.minDate.getFullYear()
    ) {
      var compareDate = new Date(year, num, 1).getTime();
      return compareDate < vm.monthOptions.minDate.getTime();
    }
    return false;
  };
  vm.disableYear = function (num) {
    return num.getFullYear() < vm.monthOptions.minDate.getFullYear();
  };
  var daysOfMonth = function (year, month) {
    return new Date(year, month + 1, 0).getDate();
  };
  vm.setMonth = function (num) {
    var day = 1,
      month = num,
      year = vm.dt.getFullYear();
    vm.dt = new Date(year, month, day);
  };
  vm.prevMonth = function () {
    var prevMonth = new Date(vm.dt.getFullYear(), vm.dt.getMonth(), 0);
    prevMonth.setDate(1);
    vm.dt = prevMonth;
  };
  vm.nextMonth = function () {
    var nextMonth = new Date(vm.dt.getFullYear(), vm.dt.getMonth() + 2, 0);
    nextMonth.setDate(1);
    vm.dt = nextMonth;
  };
  vm.prevYear = function () {
    var year = vm.dt.getFullYear() - 1;
    var days = vm.dt.getDate();
    var month = vm.dt.getMonth();
    vm.dt = new Date(year, month, days);
  };
  vm.nextYear = function () {
    var year = vm.dt.getFullYear() + 1;
    var days = vm.dt.getDate();
    var month = vm.dt.getMonth();
    vm.dt = new Date(year, month, days);
  };
  vm.getPrevMonth = function () {
    var prevMonth = new Date(vm.dt);
    prevMonth.setDate(-1);
    return prevMonth;
  };
  vm.getNextMonth = function () {
    var nextMonth = new Date(vm.dt);
    var days =
      daysOfMonth(vm.dt.getFullYear(), vm.dt.getMonth()) - vm.dt.getDate() + 1;
    nextMonth.setDate(days + vm.dt.getDate());
    return nextMonth;
  };
  vm.getPrevYear = function () {
    return new Date(vm.dt.getFullYear(), 0, 0);
  };
  vm.getNextYear = function () {
    return new Date(vm.dt.getFullYear() + 2, 0, 0);
  };
  vm.changeDay = function () {
    vm.appointment.hour = 0;
    vm.invalidAppHour = true;
    vm.errorMessage = "Selecciona un horario disponible";
    vm.getAvailableHours();
    vm.validAppointment();
    vm.selectedHour = undefined;
  };
  vm.getDayOfYear = function (date) {
    var hoy = new Date();
    var month2 = hoy.getMonth();
    var year2 = hoy.getFullYear();
    var days2 = hoy.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    var days = date.getDate();
    
    if (year2 < year) {
      return false;
    }
    if (month2 < month) {
      return false;
    }
    if (days2 < days + 1) {
      return false;
    }
    return true;
  };
  vm.validAppointment = function () {
    vm.invalidAppDate = vm.getDayOfYear(vm.dt);
    if (vm.invalidAppDate) {
      vm.errorMessage =
        "El Servicio no está disponible en la fecha seleccionada";
    }
  };
  vm.days = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
  vm.getAvailableHours = function () {
    if (vm.service) {
      var curDay = vm.days[vm.dt.getDay()];
      vm.av = [];
      vm.hoursConfig = [];
      vm.availableHours = [];
      //
      let dataAux = [];
      //
      EventService.getAllUser((err, data) => {
        //

        //
        for (var i = 0; i < data.data.data.length; i++) {
          if (
            data.data.data[i].status === "active" &&
            data.data.data[i].provider === vm.appointment.provider._id
          ) {
            dataAux.push(data.data.data[i]);
          }
        }
        //
        if (dataAux.length > 0) {
          //Obtener las horas del servicio configurado
          for (let i = 0; i < vm.service.available.length; i++) {
            if (vm.service.available[i].days.includes(curDay)) {
              //if dia

              vm.av.push(vm.service.available[i]);
            }
          }

          //Obtener horario de atencion
          for (let l = 0; l < vm.av.length; l++) {
            vm.hourAux = [];
            let av = vm.av[l];
            let avStart = av.start;
            let avEnd = av.end;
            let mifecha = new Date(vm.dt);
            mifecha.setHours(0, 0, 0, 0);
            for (let i = avStart; i <= avEnd; i++) {
              //
              mifecha.setHours(i, 0, 0, 0);
              //
              //
              let entro = false;
              for (var i2 = 0; i2 < dataAux.length; i2++) {
                //
                let fecha1 = new Date(dataAux[i2].from_date);
                let time1 = fecha1.getTime();
                let fecha2 = new Date(dataAux[i2].due_date);
                let time2 = fecha2.getTime();
                if (mifecha.getTime() >= time1 && mifecha.getTime() <= time2) {
                  //
                  entro = true;
                } else {
                  //
                }
              }
              if (entro === false) {
                vm.hourAux.push(i);
              }
            }
            vm.hoursConfig.push(vm.hourAux);
          }
          
          //De acuerdo al numero de horas disponibles generar los horarios disponibles
          for (let horarios = 0; horarios < vm.hoursConfig.length; horarios++) {
            let horario = vm.hoursConfig[horarios];
            let allDurationMinutes = (horario.length - 1) * 60;
            let numberOptionsDisponibility = Math.floor(
              allDurationMinutes / vm.service.duration
            );
            let hourAux = horario[0];
            let increaseTime = Main.translateDurationToHours(
              vm.service.duration
            );
            for (let i = 0; i < horario.length; i++) {
              let aux = hourAux + 1;
              while (hourAux < aux) {
                
                if (!vm.availableHours.includes(hourAux)) {
                  vm.availableHours.push(hourAux);
                }
                hourAux += increaseTime;
              }
              hourAux = horario[i + 1];
            }
          }

          /**
                                 * 
                                 * for(let horarios=0;horarios<vm.hoursConfig.length;horarios++){
                let horario = vm.hoursConfig[horarios];
                let allDurationMinutes = (horario.length-1)*60;
                let numberOptionsDisponibility = Math.floor(allDurationMinutes/vm.service.duration);
                let hourAux = horario[0];
                let increaseTime = Main.translateDurationToHours(vm.service.duration);
                for(let value=0;value < vm.hoursConfig.length;value++){
                    
                    
                    hourAux = vm.hoursConfig[value];
                    if(!vm.availableHours.includes(hourAux)){
                            vm.availableHours.push(hourAux);
                    }
                    hourAux+=increaseTime;
                    
                    
                            
                            }
                    
                    
                    
                                }
                                 */

          let appoinmentsReservedBySelectDay = [];

          //revisar si hay citas reservadas ya

          for (let a = 0; a < vm.appointments.length; a++) {
            let ap = vm.appointments[a];
            if (
              vm.dt.getDay() == ap.getDay() &&
              vm.dt.getMonth() == ap.getMonth() &&
              vm.dt.getFullYear() == ap.getFullYear()
            ) {
              appoinmentsReservedBySelectDay.push(ap);
            }
          }

          //obtener el numero de disponitildad por cita
          let limitAppoinments = vm.service.space;
          if (appoinmentsReservedBySelectDay.length > 0) {
            for (let h = 0; h < vm.availableHours.length; h++) {
              let timeAvailable = vm.availableHours[h];
              let hourAvailable = Math.floor(timeAvailable);
              let decimalPart = timeAvailable - hourAvailable;
              let minutesAvailable = Math.round(60 * decimalPart);
              let count = 0;
              for (let a = 0; a < appoinmentsReservedBySelectDay.length; a++) {
                let ap = appoinmentsReservedBySelectDay[a];
                if (
                  hourAvailable == ap.getHours() &&
                  minutesAvailable == ap.getMinutes()
                ) {
                  count++;
                }
              }

              if (count >= limitAppoinments) {
                // 'remove hour '+hourAvailable
                vm.availableHours.splice(h, 1);
                h--;
              }
            }
          }
          //Quitar horas que ya pasaron en el dia actual
          let currentDay = new Date();
          let timeSelected = vm.dt;
          if (
            vm.dt.getDay() == currentDay.getDay() &&
            vm.dt.getMonth() == currentDay.getMonth() &&
            vm.dt.getFullYear() == currentDay.getFullYear()
          ) {
            for (let h = 0; h < vm.availableHours.length; h++) {
              let timeAvailable = vm.availableHours[h];
              let hour = Math.floor(timeAvailable);
              let decimalPart = timeAvailable - hour;
              let minutes = Math.round(60 * decimalPart);
              timeSelected.setHours(hour);
              timeSelected.setMinutes(minutes);
              if (timeSelected < currentDay) {
                vm.availableHours.splice(h, 1);
                h--;
              }
            }
          }

          //Ordenar las horas
          vm.availableHours.sort(function (a, b) {
            return a - b;
          });
        } else {
          /////////////////////////////////////////////////////////////////////////////
          for (var i = 0; i < vm.service.available.length; i++) {
            if (vm.service.available[i].days.includes(curDay)) {
              vm.av.push(vm.service.available[i]);
            }
          }
          
          for (var l = 0; l < vm.av.length; l++) {
            vm.hourAux = [];
            var av = vm.av[l];
            var avStart = av.start;
            var avEnd = av.end;
            for (var i = avStart; i <= avEnd; i++) {
              if (!vm.hourAux.includes(i)) vm.hourAux.push(i);
            }
            vm.hoursConfig.push(vm.hourAux);
          }
          

          for (var horarios = 0; horarios < vm.hoursConfig.length; horarios++) {
            var horario = vm.hoursConfig[horarios];
            var allDurationMinutes = (horario.length - 1) * 60;
            
            var numberOptionsDisponibility = Math.floor(
              allDurationMinutes / vm.service.duration
            );
            
            
            var hourAux = horario[0];
            
            
            var increaseTime = Main.translateDurationToHours(
              vm.service.duration
            );
            
            for (var h = 0; h < numberOptionsDisponibility; h++) {
              if (!vm.availableHours.includes(hourAux))
                vm.availableHours.push(hourAux);
              hourAux += increaseTime;
            }
          }

          
          var appoinmentsReservedBySelectDay = [];
          for (var a = 0; a < vm.appointments.length; a++) {
            var ap = vm.appointments[a];
            if (
              vm.dt.getDay() == ap.getDay() &&
              vm.dt.getMonth() == ap.getMonth() &&
              vm.dt.getFullYear() == ap.getFullYear()
            ) {
              appoinmentsReservedBySelectDay.push(ap);
            }
          }

          
          var limitAppoinments = vm.service.space;
          if (appoinmentsReservedBySelectDay.length > 0) {
            for (var h = 0; h < vm.availableHours.length; h++) {
              var timeAvailable = vm.availableHours[h];
              var hourAvailable = Math.floor(timeAvailable);
              var decimalPart = timeAvailable - hourAvailable;
              var minutesAvailable = Math.round(60 * decimalPart);
              var count = 0;
              for (var a = 0; a < appoinmentsReservedBySelectDay.length; a++) {
                var ap = appoinmentsReservedBySelectDay[a];
                if (
                  hourAvailable == ap.getHours() &&
                  minutesAvailable == ap.getMinutes()
                ) {
                  count++;
                }
              }
              if (count >= limitAppoinments) {
                vm.availableHours.splice(h, 1);
                h--;
              }
            }
          }
          
          var currentDay = new Date();
          var timeSelected = vm.dt;
          if (
            vm.dt.getDay() == currentDay.getDay() &&
            vm.dt.getMonth() == currentDay.getMonth() &&
            vm.dt.getFullYear() == currentDay.getFullYear()
          ) {
            for (var h = 0; h < vm.availableHours.length; h++) {
              var timeAvailable = vm.availableHours[h];
              var hour = Math.floor(timeAvailable);
              var decimalPart = timeAvailable - hour;
              var minutes = Math.round(60 * decimalPart);
              timeSelected.setHours(hour);
              timeSelected.setMinutes(minutes);
              if (timeSelected < currentDay) {
                vm.availableHours.splice(h, 1);
                h--;
              }
            }
          }
          vm.availableHours.sort(function (a, b) {
            return a - b;
          });
        }
      });
    }
    return;
  };
  vm.selectHour = function () {
    vm.errorMessage = undefined;
    if (vm.selectedHour === undefined) {
      vm.appointment.hour = 0;
      vm.invalidAppHour = true;
      vm.errorMessage = "Selecciona un horario disponible";
      return;
    }
    var hour = Math.floor(vm.selectedHour);
    var decimalPart = vm.selectedHour - hour;
    var minutes = Main.translateDecimalPart(decimalPart);
    var timeSelected = new Date();
    timeSelected.setHours(hour);
    timeSelected.setMinutes(minutes);
    var timeCurrent = new Date();
    if (vm.dt <= vm.monthOptions.minDate) {
      if (timeSelected < timeCurrent) {
        vm.invalidAppHour = true;
        vm.errorMessage =
          "El Servicio no está disponible en la hora seleccionada";
        return;
      }
    }
    vm.invalidAppHour = false;
    vm.appointment.hour = hour;
    vm.appointment.minutes = minutes;
    return;
  };
  vm.saveAppointment = function () {
    Alert.setLoading("Generando solicitud");
    Alert.showLoading();
    var from_date = new Date(Main.setter(vm.dt));
    var due_date = new Date(Main.setter(vm.dt));
    from_date.setHours(vm.appointment.hour);
    from_date.setMinutes(vm.appointment.minutes);
    from_date.setSeconds(0);
    vm.appointment.from_date = from_date;
    due_date.setHours(vm.appointment.hour);
    due_date.setMinutes(vm.appointment.minutes + vm.service.duration);
    due_date.setSeconds(0);
    vm.appointment.due_date = due_date;
    Appointment.create(vm.appointment, function (err, data) {
      Alert.hideLoading();
      if (err) {
        var messageError = "No pudo  generarse la cita";
        if (data == "space.full") {
          messageError =
            "El servicio no cuenta con disponibilidad de horario. Te recomendamos seleccionar otro horario o día.";
        }
        Alert.setInfo(messageError, false, function () {});
        Alert.showInfo();
        return;
      }
      Alert.showAppointment();
    });
  };
  vm.getEventCount = function (mont) {
    var year = vm.dt.getFullYear();
    if (vm.appointments[year]) {
      if (vm.appointments[year][mont]) {
        return vm.appointments[year][mont].length;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  };
  vm.loadService = function () {
    Service.getService($stateParams.serviceId, function (err, data) {
      if (err) {
        Alert.setInfo(
          "El servicio no se encuentra disponible",
          false,
          function () {
            $state.go("user.services");
          }
        );
        setTimeout(function () {
          Alert.showInfo();
        });
        return;
      }
      vm.service = data;
      var element = "Servicio de mantenimiento automotriz";
      var elementO = "Nissan Guadalajara";
      vm.idx = vm.service.categories.indexOf(element);
      vm.idO = vm.service.categories.indexOf(elementO);
      vm.service.translateDuration = Main.translateDuration(
        vm.service.duration
      );
      vm.changeDay();
      vm.appointment = {
        client: AuthService.getUser().id,
        service: vm.service._id,
        provider: vm.service.provider,
        from_date: null,
        due_date: null,
      };
    });
  };
  vm.loadAppointments = function () {
    vm.appointments = [];
    Appointment.get(function (err, data) {
      if (err) {
        Alert.setInfo(
          "No se pudieron obtener los datos",
          false,
          function () {}
        );
        setTimeout(function () {
          Alert.showInfo();
        }, 700);
        return;
      }
      var apts = data;
      for (var i = 0; i < apts.length; i++) {
        var item = Main.setter(apts[i]);
        vm.appointments.push(new Date(item.from_date));
      }
      vm.appointments.sort(function (a, b) {
        return a.getTime() - b.getTime();
      });
      vm.loadService();
    });
  };
  vm.init = function () {
    if (!$stateParams.serviceId) {
      $state.go("user.services");
    }
    vm.loadAppointments();
    Main.setModule(vm.module);
    vm.selectedHour = null;
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.controller("UserAppointmentDetailC", function (
  $scope,
  $stateParams,
  Main,
  Appointment
) {
  var vm = this;
  vm.appointment = null;
  vm.loadAppointmentDetail = function () {
    var id = $stateParams.apptId;
    Appointment.getDetail(
      {
        id: id,
      },
      function (err, response) {
        if (!err) {
          vm.appointment = response;
        }
      }
    );
  };
  vm.init = function () {
    Main.setModule("Detalle de Cita");
    vm.loadAppointmentDetail();
  };
  vm.checkIfIsOpacity = function (date) {
    return new Date(date) < new Date();
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.controller("UserProfileC", function (
  Main,
  Alert,
  CONFIG,
  AuthService,
  Client,
  Session,
  $scope
) {
  var vm = this;
  vm.module = "Información de perfil";
  vm.bounds = {};
  vm.bounds.left = 0;
  vm.bounds.right = 0;
  vm.bounds.top = 0;
  vm.bounds.bottom = 0;
  vm.cropper = {};
  vm.cropper.sourceImage = null;
  vm.cropper.croppedImage = null;
  vm.imageChanged = false;
  vm.isImageChanged = function () {
    return vm.imageChanged;
  };
  vm.getRole = function () {
    return AuthService.getRole();
  };
  vm.module = "Perfil";
  vm.init = function () {
    Main.setModule(vm.module);
    vm.editable = false;
    vm.user = Main.setter(AuthService.getUser());
    vm.editUser = Main.setter(AuthService.getUser());
  };
  vm.isEditable = function () {
    return vm.editable;
  };
  vm.edit = function () {
    vm.editable = true;
  };
  vm.getImage = function (_id) {
    if (_id == undefined) {
      return "img/user-default.png";
    }
    return CONFIG.backend_url + "image?id=" + _id;
  };
  vm.openImage = function () {
    if (vm.isEditable()) {
      $("#ImageFile").click();
      vm.imageChanged = true;
    }
  };
  vm.save = function () {
    var testName = /^[0-9\s]*$/;
    if (!testName.test(vm.editUser.data.phone)) {
      return Alert.error("El teléfono debe contener solo números.");
    }
    if (!vm.editUser.data.full_name) {
      return Alert.error("Ingrese su nombre completo");
    }
    var testName = /^[a-zA-ZÀ-ü\s]*$/;
    if (!testName.test(vm.editUser.data.full_name)) {
      return Alert.error("El nombre debe contener solo letras.");
    }
    if (vm.editUser.data.full_name.replace(" ", "").length < 8) {
      return Alert.error("Su nombre completo debe tener al menos 8 caracteres");
    }
    if (vm.editUser.data.full_name.length > 100) {
      return Alert.error("Su nombre completo excede los 100 caracteres");
    }
    if (!vm.editUser.data.phone) {
      return Alert.error("Ingrese teléfono");
    }
    Client.update(vm.editUser, function (err, resp) {
      if (!err) {
        vm.editable = false;
        vm.imageChanged = false;
        vm.editUser.data = resp.data;
        Alert.setInfo("Datos actualizados correctamente", true, function () {
          Session.setUser(vm.editUser);
        });
        setTimeout(function () {
          vm.imageChanged = false;
          Alert.showInfo();
        }, 700);
      } else {
        Alert.error(resp);
      }
    });
  };
  vm.cancel = function () {
    vm.editUser = AuthService.getUser();
    vm.editable = false;
    vm.imageChanged = false;
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
});
app.controller("UserServicesC", function (Alert, Main, Service, Store, $scope, Admins) {
  var vm = this;
  vm.module = "Servicios";
  vm.cc_services = [];
  vm.counterLoaded = [];
  vm.notifications = [];
  var timer = null;
  vm.search = "";
  vm.type = "";
  vm.msnLocation;
  vm.getServices = function () {
    return vm.cc_services;
  };
  /*vm.getNotifications = function () {
    vm.notifications = [];
    Admins.getNotifications(
      function (err, response) {
        if (!err) {
          
          vm.notifications = response.data;
          
        } else {
          Alert.setInfo("Error de conexión", false);
          setTimeout(function () {
            Alert.showInfo();
          }, 500);
        }
      }
    );
  };*/
  vm.countServices = function () {
    if (vm.getServices()) {
      return vm.getServices().length;
    } else {
      return 0;
    }
  };
  vm.searchServices = function (search) {
    vm.search = search;
    if (timer) {
      clearTimeout(timer);
      timer = null;
    }
    timer = setTimeout(function () {
      vm.loadServices(search, vm.type, vm.sort);
    }, 500);
  };
  vm.loadServices = function (search, type, sort) {
    vm.servicesPage = 1;
    vm.search = "";
    vm.cc_services = [];
    vm.counterLoaded = [];
    var lat = localStorage.getItem("latCurrent");
    var lng = localStorage.getItem("lngCurrent");
    if (Main.loc) {
      lat = Main.lat;
      lng = Main.lng;
    }
    switch (sort) {
      case "Más cercanos":
        sort = "cercanos";
        break;
      case "Más solicitados":
        sort = "solicitados";
        break;
      default:
        sort = "todos";
        break;
    }
    Service.getServices(
      {
        page: vm.servicesPage,
        search: search || "",
        type: type || "",
        sort: sort || "",
        lat: lat || "",
        lng: lng || "",
      },
      function (err, response) {
        if (!err) {
          vm.cc_services = response;
        } else {
          Alert.setInfo("Error de conexión", false);
          setTimeout(function () {
            Alert.showInfo();
          }, 500);
        }
      }
    );
  };
  vm.checkNextServices = function () {
    if (vm.counterLoaded.includes(vm.servicesPage)) {
      return;
    }
    vm.servicesPage++;
    vm.counterLoaded.push(vm.servicesPage);
    var lat = localStorage.getItem("latCurrent");
    var lng = localStorage.getItem("lngCurrent");
    if (Main.loc) {
      lat = Main.lat;
      lng = Main.lng;
    }
    switch (vm.sort) {
      case "Más cercanos":
        vm.sort = "cercanos";
        break;
      case "Más solicitados":
        vm.sort = "solicitados";
        break;
      default:
        vm.sort = "todos";
        break;
    }
    Service.getServices(
      {
        page: vm.servicesPage,
        search: vm.search || "",
        type: vm.type || "",
        sort: vm.sort || "",
        lat: lat || "",
        lng: lng || "",
      },
      function (err, response) {
        if (!err) {
          vm.cc_services = vm.cc_services.concat(response);
        } else {
          Alert.setInfo(response, false);
          setTimeout(function () {
            Alert.showInfo();
          }, 500);
        }
      }
    );
  };
  vm.init = function () {
    //vm.getNotifications();
    
    vm.type = "";
    vm.search = "";
    var lat = localStorage.getItem("latCurrent");
    var lng = localStorage.getItem("lngCurrent");
    vm.sort = lat != undefined ? "Más cercanos" : "Mostrar todo";
    vm.loadServices();
    Main.setModule(vm.module);
  };
  $scope.$watch(
    function () {
      return Main.type;
    },
    function (newValue) {
      vm.type = newValue;
      vm.loadServices(vm.search, vm.type, vm.sort);
    }
  );
  vm.msnLocation = Main.msnLocation;
  $scope.$watch(
    function () {
      return Main.sort;
    },
    function (newValue) {
      vm.sort = newValue;
      vm.loadServices(vm.search, vm.type, vm.sort);
    }
  );
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
  });
  $scope.$on("$destroy", function () {});
});
app.controller("UserScheduleC", function (
  Appointment,
  AuthService,
  Alert,
  Main,
  $scope,
  $state,
  uiCalendarConfig
) {
  var vm = this;
  vm.module = "Agenda";
  vm.appointments = [];
  vm.getAppointments = function () {
    return vm.appointments;
  };
  var convertToMili = 24 * 60 * 60 * 1000;
  vm.dt = new Date();
  vm.monthOptions = {
    showWeeks: false,
    maxMode: "day",
  };
  vm.yearOptions = {
    showWeeks: false,
    datepickerMode: "month",
    minMode: "month",
  };
  var daysOfMonth = function (year, month) {
    return new Date(year, month + 1, 0).getDate();
  };
  vm.getStatusTranslate = function (status) {
    switch (status) {
      case "cancelled":
        return "Cancelada";
      case "accepted":
        return "Aceptada";
    }
  };
  vm.checkIfIsOpacity = function (date) {
    return new Date(date) < new Date();
  };
  vm.setMonth = function (num) {
    var day = 1,
      month = num,
      year = vm.dt.getFullYear();
    vm.dt = new Date(year, month, day);
  };
  vm.prevMonth = function () {
    var prevMonth = new Date(vm.dt.getFullYear(), vm.dt.getMonth(), 0);
    prevMonth.setDate(1);
    vm.dt = prevMonth;
  };
  vm.nextMonth = function () {
    var nextMonth = new Date(vm.dt.getFullYear(), vm.dt.getMonth() + 2, 0);
    nextMonth.setDate(1);
    vm.dt = nextMonth;
  };
  vm.prevYear = function () {
    var year = vm.dt.getFullYear() - 1;
    var days = vm.dt.getDate();
    var month = vm.dt.getMonth();
    vm.dt = new Date(year, month, days);
  };
  vm.nextYear = function () {
    var year = vm.dt.getFullYear() + 1;
    var days = vm.dt.getDate();
    var month = vm.dt.getMonth();
    vm.dt = new Date(year, month, days);
  };
  vm.getPrevMonth = function () {
    var prevMonth = new Date(vm.dt);
    prevMonth.setDate(-1);
    return prevMonth;
  };
  vm.getNextMonth = function () {
    var nextMonth = new Date(vm.dt);
    var days =
      daysOfMonth(vm.dt.getFullYear(), vm.dt.getMonth()) - vm.dt.getDate() + 1;
    nextMonth.setDate(days + vm.dt.getDate());
    return nextMonth;
  };
  vm.getPrevYear = function () {
    return new Date(vm.dt.getFullYear(), 0, 0);
  };
  vm.getNextYear = function () {
    return new Date(vm.dt.getFullYear() + 2, 0, 0);
  };
  vm.getEventCount = function (mont) {
    var year = vm.dt.getFullYear();
    if (vm.appointments[year]) {
      if (vm.appointments[year][mont]) {
        return vm.appointments[year][mont].length;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  };
  vm.getCurrentAppointments = function () {
    var year = vm.dt.getFullYear();
    var mont = vm.dt.getMonth();
    if (vm.appointments[year]) {
      if (vm.appointments[year][mont]) {
        return vm.appointments[year][mont];
      } else {
        return [];
      }
    } else {
      return [];
    }
  };
  vm.getAppointmentDetail = function (_apt) {
    $state.go("user.appointmentDetail", {
      apptId: _apt._id,
    });
  };
  vm.loadAppointments = function () {
    Appointment.get(function (err, data) {
      if (!err) {
        var apts = data;
        for (var i = 0; i < apts.length; i++) {
          var item = Main.setter(apts[i]);
          item.due_date = new Date(item.due_date);
          item.from_date = new Date(item.from_date);
          item.request_date = new Date(item.request_date);
          var year = item.from_date.getFullYear();
          var month = item.from_date.getMonth();
          if (!vm.appointments[year]) {
            vm.appointments[year] = [];
          }
          if (!vm.appointments[year][month]) {
            vm.appointments[year][month] = [];
          }
          vm.appointments[year][month].push(item);
          var eventItem = Main.setter(item);
          
          var eventClass = "appointment";
          switch (eventItem.status) {
            case "accepted":
              eventClass = eventClass + " appointment-success";
              break;
            case "pending":
              eventClass = eventClass + " appointment-warning";
              break;
            case "cancelled":
              eventClass = eventClass + " appointment-event";
              break;
          }
          var newEvent = {
            title: item.service.name,
            start: item.from_date,
            end: item.due_date,
            stick: true,
            className: [eventClass],
            url: "#!/user/appointmentdetail/" + item._id,
          };
          vm.events.push(newEvent);
        }
      } else {
        Alert.setInfo(
          "No se pudieron obtener los datos",
          false,
          function () {}
        );
        setTimeout(function () {
          Alert.showInfo();
        }, 700);
      }
    });
  };
  vm.init = function () {
    vm.loadAppointments();
    Main.setModule(vm.module);
  };
  vm.events = [];
  vm.eventSources = [vm.events];
  vm.uiConfig = {
    calendar: {
      height: 450,
      editable: false,
      header: {
        left: "month,agendaWeek,agendaDay",
        center: "title",
        right: "today prev,next",
      },
    },
  };
  vm.activeCalendar = function () {
    
    setTimeout(function () {
      if (uiCalendarConfig.calendars["calendarioUno"]) {
        uiCalendarConfig.calendars["calendarioUno"].fullCalendar("render");
      }
    });
  };
  $scope.$on("$viewContentLoaded", function () {
    vm.init();
    vm.activeCalendar();
  });
});
app.service("Admins", function (Request, AuthService) {
  var serv = {};
  serv.list = function (appointment, callback) {
    Request.post("providers", appointment, AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.getNotifications = function (callback) {
    Request.get("notifications").then(
      function (response) {
        callback(false, response.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.listadmin = function (callback) {
    Request.get("admin", AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.deleteadmin = function (datas, callback) {
    Request.post("admin/delete", datas, AuthService.getToken()).then(
      function (response) {
        callback(false, response);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.saveadmin = function (datas, callback) {
    Request.post("admin", datas, AuthService.getToken()).then(
      function (response) {
        callback(false, response);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.saveNotification = function (datas, callback) {
    Request.post("notification", datas).then(
      function (response) {
        callback(false, response);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.activateprovider = function (datas, callback) {
    Request.post("activateprovider", datas, AuthService.getToken()).then(
      function (response) {
        callback(false, response);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.getProvider = function (data, callback) {
    Request.get("provider/" + data, AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.paymet = function (payment, callback) {
    Request.post("payment", payment, AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.getMemberships = function (callback) {
    Request.get("memberships").then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  return serv;
});
app.service("Appointment", function (Request, AuthService) {
  var serv = {};
  serv.create = function (appointment, callback) {
    Request.post("appointment", appointment, AuthService.getToken()).then(
      function (resp) {
        var data = resp.data;
        if (data.success) {
          callback(false, data);
        } else {
          callback(true, data.message);
        }
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.get = function (callback) {
    Request.get(
      "appointment/" + AuthService.getUser().id,
      AuthService.getToken()
    ).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.getDetail = function (data, callback) {
    Request.get("appointmentdetail/" + data.id, AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.getProvider = function (callback) {
    Request.get(
      "appointments/" + AuthService.getUser().id,
      AuthService.getToken()
    ).then(
      function (resp) {
        var data = resp.data;
        if (data.success) {
          callback(false, data);
        } else {
          callback(true, data.message);
        }
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.getAllFromProvider = function (data, callback) {
    var query = "";
    if (data.status) {
      query += "status=" + data.status;
    }
    if (data.from_date) {
      if (query.length) query += "&";
      query += "from_date=" + data.from_date;
    }
    if (data.due_date) {
      if (query.length) query += "&";
      query += "" + data.due_date;
    }
    Request.get(
      "providerappointments/" + AuthService.getUser().id + "?" + query,
      AuthService.getToken()
    ).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.getGroupFromProvider = function (data, callback) {
    var query = "";
    if (data.status) {
      query += "status=" + data.status;
    }
    if (data.from_date) {
      if (query.length) query += "&";
      query += "from_date=" + data.from_date;
    }
    if (data.due_date) {
      if (query.length) query += "&";
      query += "" + data.due_date;
    }
    Request.get(
      "providerapptgroup/" + AuthService.getUser().id + "?" + query,
      AuthService.getToken()
    ).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.update = function (appt, callback) {
    Request.put("appointment/" + appt._id, appt, AuthService.getToken()).then(
      function (resp) {
        var data = resp.data;
        if (data.success) {
          callback(false, data);
        } else {
          callback(true, data.message);
        }
      },
      function (err) {
        callback(true, err.data.message);
      }
    );
  };
  serv.getFromService = function (serviceId, callback) {
    Request.get("serviceapts/" + serviceId, AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.blockAppointment = function (appt, callback) {
    Request.post("appointments", appt, AuthService.getToken()).then(
      function (resp) {
        var data = resp.data;
        if (data.success) {
          callback(false, data);
        } else {
          callback(true, data.message);
        }
      },
      function (err) {
        callback(true, err.data.message);
      }
    );
  };
  return serv;
});
app.service("AuthService", function ($http, $state, Session, Request) {
  var service = {};
  service.isLoggedIn = function () {
    return Session.getAccessToken() !== null;
  };
  service.login = function (access, callback) {
    Request.post("authenticate", access).then(
      function (response) {
        var data = response.data;
        if (data.token) {
          Session.setAccessToken(data.token);
          Session.setUser(data.data);
          callback(false, data);
        } else {
          callback(false, data);
        }
      },
      function (err) {
        callback(true, err.data);
      }
    );
  };
  service.checkRoleUser = function (data, callback) {
    Request.post("checkRoleUser", data).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (error) {
        callback(true, error.data);
      }
    );
  };
  service.forgotPassword = function (data, callback) {
    Request.post("forgotPassword", data).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (error) {
        callback(true, error.data);
      }
    );
  };
  service.confirmCode = function (data, callback) {
    Request.post("confirmCode", data).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (error) {
        callback(true, error.data);
      }
    );
  };
  service.resetPassword = function (data, callback) {
    Request.post("resetPassword", data).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (error) {
        callback(true, error.data);
      }
    );
  };
  service.signinClient = function (data, callback) {
    Request.post("createclient", data).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (error) {
        callback(
          true,
          error.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  service.facebookLogin = function (data, callback) {
    Request.post("facebook", data).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (error) {
        callback(
          true,
          error.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  service.sendMailClient = function (data) {
    Request.post("sendclientemail", data).then(
      function (response) {},
      function (error) {}
    );
  };
  service.checkIfExistProvider = function (data, callback) {
    Request.post("checkIfExistProvider", data).then(
      function (response) {
        callback(true, "Correo ya registrado");
      },
      function (error) {
        callback(false, null);
      }
    );
  };
  service.signinProvider = function (data, callback) {
    Request.post("createprovider", data).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (error) {
        callback(
          true,
          error.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  service.signinProviderCreditCard = function (data, callback) {
    Request.post("createprovidercreditcard", data).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (error) {
        callback(
          true,
          error.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  service.signinProviderNoPay = function (data, callback) {
    Request.post("createprovidernopay", data).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (error) {
        callback(
          true,
          error.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  service.signinProviderReference = function (data, callback) {
    Request.post("createproviderreference", data).then(
      function (response) {
        callback(false, response);
      },
      function (error) {
        callback(
          true,
          error.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  service.getRole = function () {
    if (Session.getUser()) {
      return Session.getUser().role;
    } else {
      return null;
    }
  };
  service.getUser = function () {
    return Session.getUser();
  };
  service.getToken = function () {
    return Session.getAccessToken();
  };
  service.getIndex = function () {
    var roles = {
      client: "user.services",
      provider: "provider.services",
      admin: "admin.listProvider",
    };
    roles[service.getRole()];
  };
  service.logout = function () {
    Session.destroy();
  };
  service.canBeHere = function () {
    return $state.current.data.role === service.getRole();
  };
  return service;
});
app.service("Session", function ($log, Store) {
  var serv = {};
  var _user = Store.get("session.user");
  var _accessToken = Store.get("session.accessToken");
  serv.getUser = function () {
    return _user;
  };
  serv.setUser = function (user) {
    _user = user;
    Store.set("session.user", user);
  };
  serv.getAccessToken = function () {
    return _accessToken;
  };
  serv.setAccessToken = function (token) {
    _accessToken = token;
    Store.set("session.accessToken", token);
  };
  serv.destroy = function () {
    _user = null;
    _accessToken = null;
    Store.clear();
  };
  return serv;
});
app.service("Client", function (Request, AuthService) {
  var serv = {};
  serv.update = function (user, callback) {
    Request.put("user/" + user.id, user, AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(true, err.data.message);
      }
    );
  };
  serv.activeClient = function (userId, code, callback) {
    Request.post("activeClient", {
      id: userId,
      code: code,
    }).then(
      function (response) {
        callback(false, response.data.message);
      },
      function (error) {
        callback(
          true,
          error.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  return serv;
});
app.service("EventService", function (Request, AuthService) {
  var service = {};
  service.save = function (newEvent, callback) {
    Request.post("event", newEvent, AuthService.getToken())
      .then(function (response) {
        callback(false, response);
      })
      .catch(function (errorResponse) {
        console.error(errorResponse);
        callback(
          true,
          errorResponse.data.message ||
            "No se pudo conectar al servidor, intente de nuevo más tarde"
        );
      });
  };
  service.getAll = function (callback) {
    Request.get("events", AuthService.getToken())
      .then(function (response) {
        callback(false, response);
      })
      .catch(function (errorResponse) {
        callback(
          true,
          errorResponse.data.message ||
            "No se pudo conectar al servidor, intente de nuevo más tarde"
        );
      });
  };
  service.getAllUser = function (callback, id) {
    Request.get("events2", id)
      .then(function (response) {
        callback(false, response);
      })
      .catch(function (errorResponse) {
        callback(
          true,
          errorResponse.data.message ||
            "No se pudo conectar al servidor, intente de nuevo más tarde"
        );
      });
  };
  return service;
});
app.service("Provider", function (Request, AuthService) {
  var serv = {};
  var standarMessage =
    "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde.";
  serv.update = function (user, callback) {
    Request.put("user/" + user.id, user, AuthService.getToken()).then(
      function (response) {
        return callback(false, response.data.data);
      },
      function (err) {
        return callback(true, err.data.message || standarMessage);
      }
    );
  };
  serv.get = function (providerId, callback) {
    Request.get("provider/" + providerId, AuthService.getToken()).then(
      function (response) {
        return callback(false, response.data);
      },
      function (err) {
        return callback(true, err.data.message || standarMessage);
      }
    );
  };
  serv.deleteCreditCard = function (creditCard, callback) {
    Request.delete(
      "provider/creditcard/" + creditCard.identifier,
      AuthService.getToken()
    ).then(
      function (response) {
        return callback(false, response.data.data);
      },
      function (err) {
        return callback(true, err.data.message || standarMessage);
      }
    );
  };
  serv.saveCreditCard = function (newCreditCard, callback) {
    Request.post(
      "provider/creditcard",
      newCreditCard,
      AuthService.getToken()
    ).then(
      function (response) {
        return callback(false, response.data.data);
      },
      function (error) {
        return callback(true, error.data.message || standarMessage);
      }
    );
  };
  return serv;
});
app.service("Service", function (Request, AuthService) {
  var serv = {};
  serv.create = function (service, callback) {
    Request.post("service", service, AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.update = function (service, callback) {
    Request.put("service/" + service._id, service, AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.delete = function (service, callback) {
    Request.delete("service/" + service._id, AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.getServices = function (data, callback) {
    var query = "page=" + data.page;
    if (data.search) {
      query += "&query=" + data.search.split(" ").join("+");
    }
    if (data.type) {
      query += "&type=" + data.type;
    }
    if (data.sort) {
      query += "&sort=" + data.sort;
    }
    if (data.lat) {
      query += "&lat=" + data.lat;
    }
    if (data.lng) {
      query += "&lng=" + data.lng;
    }
    Request.get("service?" + query, AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.getMyServices = function (data, callback) {
    var query = "page=" + data.page;
    if (data.search) {
      query += "&query=" + data.search.split().join("+");
    }
    if (data.order) {
      query += "&order=" + data.order;
    }
    Request.get(
      "services/" + AuthService.getUser().id + "?" + query,
      AuthService.getToken()
    ).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.getService = function (serviceId, callback) {
    Request.get("service/" + serviceId, AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  serv.getCategories = function (callback) {
    Request.get("category/", AuthService.getToken()).then(
      function (response) {
        callback(false, response.data.data);
      },
      function (err) {
        callback(
          true,
          err.data.message ||
            "Nuestros servidores están experimentando problemas de conexión. Por favor intente mas tarde."
        );
      }
    );
  };
  return serv;
});
