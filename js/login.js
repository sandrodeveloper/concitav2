var tipo = "";
var modal = "";

function login() {

    var hash = md5($("#password").val());
    let datos = {
        email: $("#email").val(),
        pass: hash,
    }
    
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/authenticate',
        data: datos,
        type: 'POST',
        success: function(respuesta) {
            

            if (respuesta.data['role'] == "client") {
                localStorage.setItem("role", respuesta.data['role']);
                localStorage.setItem("id", respuesta.data['id']);
                localStorage.setItem("token", respuesta['token']);
                localStorage.setItem("usuario", respuesta.data.data['full_name']);
                localStorage.setItem("mail", respuesta.data.data['email']);
                localStorage.setItem("phone", respuesta.data.data['phone']);
                localStorage.setItem('fechaR', respuesta.data.data['registration_dates'])
                localStorage.setItem("picture", respuesta.data.data['picture']);
                location.href = "services.html"
            }

            if (respuesta.data['role'] == "provider") {
                localStorage.setItem("role", respuesta.data['role']);
                localStorage.setItem("id", respuesta.data['id']);
                localStorage.setItem("token", respuesta['token']);
                localStorage.setItem("usuario", respuesta.data.data['full_name']);
                localStorage.setItem("mail", respuesta.data.data['email']);
                localStorage.setItem("phone", respuesta.data.data['phone']);
                localStorage.setItem("picture", respuesta.data.data['picture']);
                location.href = "services-proveedor.html"
            }

            if (respuesta.data.client["role"] == "client" || respuesta.data.provider["role"] == "provider") {
                

                var html =
                    `<div class="modal-content" id="myModal" tabindex="-1">
                        <div class="jconfirm jconfirm-light jconfirm-open">
                            <div class="jconfirm-bg" style="transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 0.55, 0.19, 1);"></div>
                            <div class="jconfirm-scrollpane">
                                <div class="jconfirm-row">
                                    <div class="jconfirm-cell">
                                        <div class="jconfirm-holder" style="padding-top: 40px; padding-bottom: 40px;">
                                            <div class="jc-bs3-container container">
                                                <div class="jc-bs3-row row justify-content-md-center justify-content-sm-center justify-content-xs-center justify-content-lg-center">
                                                    <div class="jconfirm-box-container jconfirm-animated col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 jconfirm-no-transition" style="transform: translate(0px, 0px); transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 0.55, 0.19, 1);">
                                                        <div class="jconfirm-box jconfirm-hilight-shake jconfirm-type- jconfirm-type-animated" role="dialog" aria-labelledby="jconfirm-box57078" tabindex="-1" style="transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 0.55, 0.19, 1); transition-property: all, margin;">
                                                            <div class="jconfirm-closeIcon" style="display: none;">×</div>
                                                            <div class="jconfirm-title-c"><span class="jconfirm-icon-c"><i class="fa fa-exclamation-triangle"></i></span><span class="jconfirm-title">Atención</span></div>
                                                            <div class="jconfirm-content-pane no-scroll" style="transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 0.55, 0.19, 1); max-height: 728.844px;">
                                                                <div class="jconfirm-content" id="jconfirm-box57078">
                                                                    <div>Su correo está vinculado a más de un perfil
                                                                        <h5>¿Cuál desea utilizar?</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="jconfirm-buttons"><button type="button" class="btn btn-default" id="usuarioBoton">Usuario</button><button type="button" class="btn btn-default" id="provedorBoton">Proveedor</button><a href="login.html"><button type="button" class="btn btn-red">Cancelar</button></a></div>
                                                            <div class="jconfirm-clear"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;

                modal += html;

                $("#creacionModal").append(modal);

                document.getElementById("usuarioBoton").addEventListener("click", function(event) {

                    var hash = md5($("#password").val());

                    let datosEntradaUsuario = {
                        email: $("#email").val(),
                        pass: hash,
                        role: respuesta.data.client["role"]
                    }
                    
                    $.ajax({
                        url: 'https://www.concita.com.mx:3000/api/v1/authenticate',
                        data: datosEntradaUsuario,
                        type: 'POST',
                        success: function(respuesta) {
                            
                            localStorage.setItem("role", respuesta.data['role']);
                            localStorage.setItem("id", respuesta.data['id']);
                            localStorage.setItem("token", respuesta['token']);
                            localStorage.setItem("usuario", respuesta.data.data['full_name']);
                            localStorage.setItem("mail", respuesta.data.data['email']);
                            localStorage.setItem("phone", respuesta.data.data['phone']);
                            localStorage.setItem("fechaR", respuesta.data.data['registration_dates']);
                            localStorage.setItem("picture", respuesta.data.data['picture']);

                            location.href = "services.html"

                        },
                    });
                });

                document.getElementById("provedorBoton").addEventListener("click", function(event) {

                    var hash = md5($("#password").val());

                    let datosEntradaProveedor = {
                        email: $("#email").val(),
                        pass: hash,
                        role: respuesta.data.provider["role"]
                    }
                    
                    $.ajax({
                        url: 'https://www.concita.com.mx:3000/api/v1/authenticate',
                        data: datosEntradaProveedor,
                        type: 'POST',
                        success: function(respuesta) {
                            localStorage.setItem("role", respuesta.data['role']);
                            localStorage.setItem("userData", JSON.stringify(respuesta.data.data));
                            localStorage.setItem("id", respuesta.data['id']);
                            localStorage.setItem("token", respuesta['token']);
                            localStorage.setItem("usuario", respuesta.data.data['full_name']);
                            localStorage.setItem("mail", respuesta.data.data['email']);
                            localStorage.setItem("phone", respuesta.data.data['phone']);
                            localStorage.setItem("picture", respuesta.data.data['picture']);

                            location.href = "services-proveedor.html"

                        },
                    });
                });

            }


        },
        error: function(er) {
            var json_mensaje = JSON.parse(er.responseText);
            
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: json_mensaje['message']
            })
        }
    });
}

function clienteModal() {
    tipo = "cliente";
}

function provedorModal() {
    tipo = "provedor";
}

function loginAdmin() {
    var hash = md5($("#password").val());

    let datos = {
        email: $("#email").val(),
        pass: hash,
    }
    
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/authenticate',
        data: datos,
        type: 'POST',
        success: function(respuesta) {
            
            if (respuesta.data['id'] == '5d55792da209ef0306e552dc') {
                location.href = "dashboard-admin.html"
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Credenciales incorrectas'
                });
            }
        },
        error: function(er) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Error de usuario o contraseña'
            })
        }
    });
}