$(document).ready(function () {
  datosCitas();
  datosCliente();
});

var servicio = "";
var arregloServicios = [];
var tipo = "";
var servicioDias = "";
var idServicio = "";
var costo = "";
var idSer = "";
var idCita = "";
var fechaCita = "";
var nombreCliente = "";

function datosCitas() {
  let urlParams = new URLSearchParams(window.location.search);
  let token = urlParams.get("idservice");

  var servicioDomicilio = "";
  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/service/" + token,
    type: "GET",
    success: function (respuesta) {
      document.getElementById("imagenServicio").src =
        "https://www.concita.com.mx:3000/api/v1/image?id=" +
        respuesta.data.image;
      $("#pnombreProvedor").text(respuesta.data.provider.data.name);
      $("#propietario").text(respuesta.data.lender);
      $("#nombreServicio").text(respuesta.data.name);
      $("#unicacion").text(respuesta.data.provider.data.address);
      $("#phone").text(respuesta.data.provider.data.phone);
      $("#categoria").text(respuesta.data.categories);
      $("#descripcionServicio").text(respuesta.data.description);
      $("#nombre").text(respuesta.data.name);
      $("#nombreBorrar").text(respuesta.data.name);
      idSer = urlParams.get("idservice");

      respuesta.data.available.forEach(function (dias) {
        $("#detalleDias").text(
          dias.days + " " + dias.start + ":00 " + " - " + dias.end + ":00 horas"
        );

        $("#detalleDias").each(function () {
          var text = $(this).text();
          text = text.replace("Mo", "Lu");
          text = text.replace("Tu", " Ma");
          text = text.replace("We", " Mi");
          text = text.replace("Th", " Ju");
          text = text.replace("Fr", " Vi");
          text = text.replace("Sa", " Sa");
          text = text.replace("Su", " Do");
          $(this).text(text);
        });
      });

      if (respuesta.data.home_service == false) {
        servicioDomicilio = "No";
      } else {
        servicioDomicilio = "Si";
      }
      $("#domicilio").text(servicioDomicilio);

      var tiempo = 0;
      var duracion = "";
      if (parseInt(respuesta.data.duration) % 60 == 0) {
        tiempo = parseInt(respuesta.data.duration) / 60;
        if (tiempo != 1) {
          duracion = tiempo + " horas";
        } else {
          duracion = tiempo + " hora";
        }
      } else {
        tiempo = parseInt(respuesta.data.duration) % 60;
        duracion = tiempo + " minutos";
      }
      $("#duracionServicio").text(duracion);

      if (respuesta.data.priceType == "quotation") {
        costo = "Sujeto a cotización.";
        $("#tituloCosto").hide();
        $("#tar").hide();
        $("#cash").hide();
      }

      if (respuesta.data.priceType == "free") {
        costo = "Gratuito.";
        $("#tituloCosto").hide();
        $("#tar").hide();
        $("#cash").hide();
      }

      if (respuesta.data.priceType == "price") {
        var costo = "$" + respuesta.data.cost + ".00";
        $("#tituloCosto").show();

        if (respuesta.data.payment_methods[0] == "cash") {
          $("#cash").show();
          $("#tar").hide();
        }

        if (respuesta.data.payment_methods[0] == "card") {
          $("#tar").show();
          $("#cash").hide();
        }

        if (
          (respuesta.data.payment_methods[0] == "card" &&
            respuesta.data.payment_methods[1] == "cash") ||
          (respuesta.data.payment_methods[1] == "card" &&
            respuesta.data.payment_methods[0] == "cash")
        ) {
          $("#cash").show();
          $("#tar").show();
        }
      }
      $("#tituloCosto").text();
      $("#pago").text(costo);
    },
  });
}

function datosCliente() {
  let urlParams = new URLSearchParams(window.location.search);
  let token = urlParams.get("idservice");
  var estado = "";
  var botonCancelar = "";
  let clientes = "";
  $("#clientes").empty();
  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/serviceapts/" + token,
    type: "GET",
    success: function (respuesta) {
      respuesta.data.docs.forEach(function (item) {
        if (item.status == "accepted") {
          estado = "ACEPTADA";
          botonCancelar =
            '<div class="d-button">\
                            <button class="btn btn-cancelar ng-scope" onclick="crearCancel(\'' +
            item._id +
            "')\">\
                    Cancelar\
                        </button>\
                        </div>";
        } else {
          estado = "RECHAZADA";
          botonCancelar = "";
        }
        let extras = "";
        if (item.clientNumber != null) {
          extras += `<div class="clientNumber" >
          <p class="t1">Contacto</p>
          <p class="t2 ng-binding">${item.clientNumber}</p>
      </div>`;
        }
        if (item.municipio != null) {
          extras += `<div class="clientNumber" >
          <p class="t1">Municipio</p>
          <p class="t2 ng-binding">${item.municipio}</p>
      </div>`;
        }
        if (item.colonia != null) {
          extras += `<div class="clientNumber" >
          <p class="t1">Colonia</p>
          <p class="t2 ng-binding">${item.colonia}</p>
      </div>`;
        }
        if (item.reason != null) {
          extras += `<div class="clientNumber" >
          <p class="t1">Motivo de cita</p>
          <p class="t2 ng-binding">${item.reason}</p>
      </div>`;
        }
        if (item.mileage != null) {
          extras += `<div class="clientNumber" >
          <p class="t1">Kilometraje Actual</p>
          <p class="t2 ng-binding">${item.mileage}</p>
      </div>`;
        }
        if (item.model != null) {
          extras += `<div class="clientNumber" >
          <p class="t1">Modelo</p>
          <p class="t2 ng-binding">${item.model}</p>
      </div>`;
        }
        if (item.year != null) {
          extras += `<div class="clientNumber" >
          <p class="t1">Año</p>
          <p class="t2 ng-binding">${item.year}</p>
      </div>`;
        }
        if (item.plates != null) {
          extras += `<div class="clientNumber" >
          <p class="t1">Placas</p>
          <p class="t2 ng-binding">${item.plates}</p>
      </div>`;
        }

        if (item.observations != null) {
          extras += `<div class="clientNumber" >
          <p class="t1">Observaciones</p>
          <p class="t2 ng-binding">${item.observations}</p>
      </div>`;
        }
        if(item.client === null || item.client === "" || item.client === undefined){
          item.client = {
            data: JSON.parse(localStorage.getItem('userData'))
          }
          item.client.email = item.client.data.emails[0]
          item.client.data.full_name = item.client.data.name
        }
        console.log(item.client);
        nombreCliente = item.client.data.full_name;
        var format = "YYYY-MM-DDTHH:mm:ss ZZ";
        item.from_date = moment(item.from_date, format)
          .tz("America/Mexico_City")
          .format(format);
        fechaCita =
          item.from_date.substr(0, 10).replace(/-/g, "/") +
          " - " +
          item.from_date.substr(11, 5).replace(/-/g, " / ") +
          " hrs";
        var html = `<div class="d-cita-pro ng-scope opacity" >
                                <div class="d-title ng-scope" >
                                    Cita ${estado}
                                </div>
                                <div class="d-info">
                                    <div class="clearfix d-img-name">
                                        <img src="https://www.concita.com.mx:3000/api/v1/image?id=${
                                          item.client.data.picture
                                        }" onerror="this.src='img/user-default.png'" alt="imgProfileCard" class="imgProfileCard" crossorigin="*" src="img/user-default.png">
                                        <p class="t1">Solicitado por</p>
                                        <p class="t2 ng-binding">${
                                          item.client.data.full_name
                                        }</p>
                                    </div>

                                    <div class="d-datos">
                                        <p class="t1">Correo electrónico</p>
                                        <p class="t2 ng-binding">${
                                          item.client.email
                                        }</p>

                                        <p class="t1">Teléfono</p>
                                        <p class="t2 ng-binding">${
                                          item.client.data.phone
                                        }</p>

                                        <p class="t1">Fecha solicitada</p>
                                        <p class="t2 ng-binding">
                                            ${
                                              item.from_date
                                                .substr(0, 10)
                                                .replace(/-/g, "/") +
                                              " - " +
                                              item.from_date
                                                .substr(11, 5)
                                                .replace(/-/g, " / ") +
                                              " hrs"
                                            }
                                        </p>
                                        ${
                                          extras
                                        }
                                    </div>

                                    <div class="d-button">
                                    </div>
                                    ${botonCancelar}
                                    
                                </div>
                            </div>`;
        clientes += html;
      });

      $("#clientes").append(clientes);
    },
  });
}

function crearCancel(id) {
  idCita = id;
  $("#citaDeleteConfirm").modal("toggle");
}

function eliminar() {
  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/service/" + idSer,
    type: "DELETE",
    success: function (respuesta) {
      setTimeout(function () {
        location.reload();
      }, 2000);
      Swal.fire({
        position: "center",
        icon: "success",
        title: "Se elimino correctamente",
        showConfirmButton: false,
        timer: 1500,
      });
    },
    error: function (er) {
      if (
        er.responseJSON.message ==
        "El servicio cuenta con citas agendadas. En caso de querer eliminarlo es necesario que no cuente con citas"
      ) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Se deben cancelar las citas antes de eliminar el servicio.",
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Error en el servidor",
        });
      }
    },
  });
}

function editarServicio() {
  location.href = "editarServicio.html?id=" + idSer;
}

function confirmar() {
  let datos = {
    status: "cancelled",
    _id: idCita,
  };

  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/appointment/" + idCita,
    data: datos,
    type: "PUT",
    success: function (respuesta) {
      setTimeout(function () {
        location.reload();
      }, 6000);
      Swal.fire({
        position: "center",
        icon: "error",
        title: "Cita rechazada",
        text: "Creado por: " + nombreCliente,
        html: `<div class="info-modal">
                <hr>
                <p class="t1">
                Creado por: <b class="ng-binding">${nombreCliente}</b>
                </p>
                <p class="t1">
                Fecha solicitada:
                <b ng-if="mn.a.appt" class="ng-binding ng-scope">
                    ${fechaCita}</b>
                </p>
                <hr>
                <p class="t-bottom">
                El usuario recibirá una notificación por correo electrónico
                </p>
            </div>`,
        showConfirmButton: true,
      });
    },
    error: function (er) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Error al cancelar la cita.",
      });
    },
  });
}
