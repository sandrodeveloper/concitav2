$(document).ready(function () {
    datosCitas();
});

var servicio = "";
var arregloServicios = [];
var tipo = "";
var servicioDias = "";
var idServicio = "";
var costo = "";
var schedule = moment().tz("America/Mexico_City");
var hoursAvailableToReschedule = 0;
function datosCitas() {

    let urlParams = new URLSearchParams(window.location.search);
    let token = urlParams.get("idservice");
    

    var servicioDomicilio = "";
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/appointmentdetail/' + token,
        type: 'GET',
        success: function (respuesta) {
            console.log(respuesta.data)
            schedule = moment(respuesta.data.from_date).tz("America/Mexico_City")
            hoursAvailableToReschedule = respuesta.data.timeAllowedToReschedule || 0;
            validarHoraPermitidaParaReagendar(schedule, hoursAvailableToReschedule);
            document.getElementById("imagenServicio").src = "https://www.concita.com.mx:3000/api/v1/image?id=" + respuesta.data.service.image;
            $('#pnombreProvedor').text(respuesta.data.provider.data.name);
            $('#propietario').text(respuesta.data.service.lender);
            $('#nombreServicio').text(respuesta.data.service.name);
            $('#unicacion').text(respuesta.data.provider.data.address);
            $('#phone').text(respuesta.data.provider.data.phone);
            $('#categoria').text(respuesta.data.service.categories);
            $('#descripcionServicio').text(respuesta.data.service.description);
            idServicio = respuesta.data.service._id

            respuesta.data.service.available.forEach(function (dias) {
                if (dias.end >= 12 && dias.end <= 23) {
                    horaFormacion = "pm."
                } else {
                    horaFormacion = "am."
                }

                if (dias.start >= 12 && dias.end <= 23) {
                    horaFormacionInicio = "pm."
                } else {
                    horaFormacionInicio = "am."
                }

                $('#detalleDias').text(dias.days + " " + dias.start + ":00 " + horaFormacionInicio + " - " + dias.end + ":00 " + horaFormacion);

                $("#detalleDias").each(function () {
                    var text = $(this).text();
                    text = text.replace("Mo", "Lu");
                    text = text.replace("Tu", " Ma");
                    text = text.replace("We", " Mi");
                    text = text.replace("Th", " Ju");
                    text = text.replace("Fr", " Vi");
                    text = text.replace("Sa", " Sa");
                    text = text.replace("Su", " Do");
                    $(this).text(text);
                });
            });



            if (respuesta.data.service.home_service == false) {
                servicioDomicilio = 'No';
            } else {
                servicioDomicilio = 'Si';
            }
            $('#domicilio').text(servicioDomicilio);

            var tiempo = 0;
            var duracion = "";
            if (parseInt(respuesta.data.service.duration) % 60 == 0) {
                tiempo = parseInt(respuesta.data.service.duration) / 60;
                if (tiempo != 1) {
                    duracion = tiempo + " horas";
                } else {
                    duracion = tiempo + " hora"
                }
            } else {
                tiempo = parseInt(respuesta.data.service.duration) % 60;
                duracion = tiempo + " minutos";
            }
            $('#duracionServicio').text(duracion);

            if (respuesta.data.service.priceType == "quotation") {
                costo = "Sujeto a cotización.";
            };
            if (respuesta.data.service.priceType == "free") {
                costo = "Gratuito.";
            };
            $('#pago').text(costo);

            $('#nombreCliente').text(respuesta.data.client.data.full_name);
            $('#correoCliente').text(respuesta.data.client.data.email);
            $('#telefonoCliente').text(respuesta.data.client.data.phone);
            var format = 'YYYY-MM-DDTHH:mm:ss ZZ';
            respuesta.data.from_date = moment(respuesta.data.from_date, format).tz("America/Mexico_City").format(format);
            due_date = moment(respuesta.data.due_date, format).tz("America/Mexico_City").format(format);
            $('#fechCita').text(respuesta.data.from_date.substr(0, 10).replace(/-/g, "/") + " - " + respuesta.data.from_date.substr(11, 5).replace(/-/g, " / ") + " hrs");
            document.getElementById("imagenCliente").src = "https://www.concita.com.mx:3000/api/v1/image?id=" + respuesta.data.client.data.picture;
            if(respuesta.data.reason!=null){
                $('#reason').text(respuesta.data.reason);
                $('.reason').show();
            }
            if(respuesta.data.municipio!=null){
                $('#municipio').text(respuesta.data.municipio);
                $('.municipio').show();
            }
            if(respuesta.data.colonia!=null){
                $('#colonia').text(respuesta.data.colonia);
                $('.colonia').show();
            }
            if(respuesta.data.plates!=null){
                $('#plates').text(respuesta.data.plates);
                $('.plates').show();
            }
            if(respuesta.data.year!=null){
                $('#year').text(respuesta.data.year);
                $('.year').show();
            }
            if(respuesta.data.model!=null){
                $('#model').text(respuesta.data.model);
                $('.model').show();
            }
            if(respuesta.data.mileage!=null){
                $('#milleage').text(respuesta.data.mileage);
                $('.milleage').show();
            }
            if(respuesta.data.clientNumber!=null){
                $('#clientNumber').text(respuesta.data.clientNumber);
                $('.clientNumber').show();
            }
            if(respuesta.data.observations!=null){
                $('#observations').text(respuesta.data.observations);
                $('.observations').show();
            }
        },
    });
}

function reagendarCita() {
    if (!validarHoraPermitidaParaReagendar(schedule, hoursAvailableToReschedule)) {
        return false;
    }
    let urlParams = new URLSearchParams(window.location.search);
    let idCita = urlParams.get("idservice");
    location.href = 'reAgendarCita.html?idservice='+idServicio+'&idCita=blue'+idCita
}

function validarHoraPermitidaParaReagendar(from_date, hoursAvailableToReschedule = 0) {
    let schedule = from_date;
    let today = moment().tz("America/Mexico_City")
    var duration = moment.duration(schedule.diff(today));
    var hours = duration.asHours();
    if (hoursAvailableToReschedule !== 0 && hours <= hoursAvailableToReschedule) {
        $('#reagendar').css("display", "none");
        $('#errorMessage').css("display", "");
        return false;
    }
    $('#errorMessage').css("display", "none");
    return true;
}