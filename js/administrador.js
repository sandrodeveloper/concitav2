$(document).ready(function() {
    pintarTabla();

});

var numeroPagina = 1;
var clientes = [];
var ink = "";

function cerrarSesion() {
    location.href = "login-admin.html"
}

function pagina(pagNum) {
    numeroPagina = pagNum;
    pintarTabla();
}

function pintarTabla() {
    var numero = 0;
    let datos = {
        offset: numeroPagina,
        query: ink
    };
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/providers',
        data: datos,
        type: 'POST',
        success: function(respuesta) {
            var servicio = "";
            var paginas = "";
            
            numero = respuesta.data.pages;
            var contadorSercicio = 0;
            clientes = respuesta.data.docs;
            respuesta.data.docs.forEach(function(item) {
                clientes[contadorSercicio];

                var membreciaNombre = "";

                if (item.membership.membership == null || item.membership.membership == "" || item.membership.membership == undefined) {
                    var membreciaNombre = "";
                } else {
                     membreciaNombre = item.membership.membership.name;
                }

                var vencida = "Vencido";

                if (item.membership.membership.status === "active") {
                    vencida = "Activo";
                }

                var html = `<tr>
                <td style="width: 10% !important;" class="ng-binding">` + item.data.name + `</td>
                <td style="width: 10% !important;" class="ng-binding">` + membreciaNombre + `</td>
                <td style="width: 10% !important;" class="ng-binding">${item.membership.renovation_date.substr(0,10).replace(/-/g,"/")}</td>
                <td style="width: 10% !important;" class="ng-binding">` + item.membership.expiration_date.substr(0, 10).replace(/-/g, "/") + `</td>
                <td style="width: 10% !important;">${item.membership.position_date.substr(0,10).replace(/-/g,"/")}</td>
                <td style="width: 45% !important;text-align: center;" class="aceptaciones">${vencida}</td>
                <td style="width: 10% !important;" class="ng-binding">$ ${item.membership.membership.cost}</td>
                <td style="width: 10% !important;" class="ng-binding">${item.membership.membership.description}</td>
                <td style="width: 5% !important;"> <button class="botonAdquirir" onclick="detalles(${contadorSercicio})">Detalle</button></td>
                <td style="width: 5% !important;"> <button class="botonAdquirir" onclick="abrirFactura(${contadorSercicio})">Facturar</button></td>
                <td style="width: 5% !important;"><button class="botonAdquirir" onclick="activarProveedor(${contadorSercicio})">Activar</button></td>
                </>
               `;
                servicio += html;
                contadorSercicio += 1;
            });



            for (var i = 1; i < numero; i++) {
                var classeActive = "";
                if (i == respuesta.data.page) {
                    classeActive = "active";
                }
                var html = `
                <a ng-repeat="i in vm.arr" onclick="pagina('${i}')" class="ng-binding ng-scope ${classeActive}">${i}</a>
                `;

                paginas += html;
            }

            $("#page").empty();
            $("#servicios").empty();
            $("#page").append(paginas);
            $("#servicios").append(servicio);

        }
    });
}

function buscador() {
    numeroPagina = 1;
    ink = $('#buscador').val();
    pintarTabla();
}

function ocultar() {
    $("#detalles").hide();
}

function activarProveedor(numero) {
    let datos = {
        id: clientes[numero]._id
    }
    $.ajax({
        url: "https://www.concita.com.mx:3000/api/v1/activateprovider",
        data: datos,
        type: "POST",
        success: function (respuesta) {
          
          Swal.fire({
            icon: "success",
            title: "Éxito",
            text: "Proveedor activado correctamente",
            showCancelButton: false,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Excelente!",
          }).then((result) => {
            location.href = "dashboard-admin.html";
          });
        },
        error: function (er) {
          
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Por favor verifique los campos.",
          });
        },
      });
}

function detalles(numero) {
    $("#detalles").show();
    $('#nombreD').val(clientes[numero].data['name']);
    $('#direccionD').val(clientes[numero].data['address']);
    $('#correoD').val(clientes[numero]['email']);
    $('#telefonoD').val(clientes[numero].data['phone']);
}

function abrirFactura(numCliente) {
    location.href = "facturaAuto.html"
    
}