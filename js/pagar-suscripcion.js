var objectsuscription = {};
$(document).ready(function() {
    objectsuscription = JSON.parse(localStorage.getItem('suscription'));
    paypal.Buttons({
        style: {
            shape: 'rect',
            color: 'gold',
            layout: 'vertical',
            label: 'subscribe',
            
        },
        createSubscription: function(data, actions) {
            return actions.subscription.create({
            'plan_id': objectsuscription.paypal_plan_id_news
            });
        },
        onApprove: function(data, actions) {
            let datos = {
                provider:localStorage.getItem('id'),
                membership: objectsuscription,
                amount: objectsuscription.cost,
                registration_date: JSON.parse(localStorage.getItem('userData')).registration_date,
                positionDays: objectsuscription.positionDays,
                observations: data.subscriptionID,
                fechaexpration: moment().add(366 , 'days')
            }
            $.ajax({
              url: "https://www.concita.com.mx:3000/api/v1/payment",
              data: datos,
              type: "POST",
              success: function (respuesta) {
                
                Swal.fire({
                  icon: "success",
                  title: "Éxito",
                  text: "Suscripción creada correctamente",
                  showCancelButton: false,
                  confirmButtonColor: "#3085d6",
                  cancelButtonColor: "#d33",
                  confirmButtonText: "Excelente!",
                }).then((result) => {
                  location.href = "services-proveedor.html";
                });
              },
              error: function (er) {
                
                Swal.fire({
                  icon: "error",
                  title: "Oops...",
                  text: "Por favor verifique los campos.",
                });
              },
            });
        }
    }).render('#paypal-button-container');
});