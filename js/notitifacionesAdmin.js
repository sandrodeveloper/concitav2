function regresar() {
    location.href = "dashboard-admin.html";
}

let textMensaje = '', tituloMensaje = '';

$(document).ready(function() {
    window.emojioneVersion = "2.1.1";
    notificaciones();
    $("#textMensaje").emojioneArea({
        pickerPosition: "right",
        tonesStyle: "bullet",
      events:{
        keyup: function (editor, event) {
            textMensaje = this.getText();
            enableButton();
        },
        emojibtn_click: function (button, event) {
            textMensaje = this.getText();
            enableButton();
        },
      }
    });

    $("#tituloMensaje").emojioneArea({
        pickerPosition: "right",
    tonesStyle: "bullet",
      events:{
        keyup: function (editor, event) {
            tituloMensaje = this.getText();
            enableButton();
        },
        emojibtn_click: function (button, event) {
            tituloMensaje = this.getText();
            enableButton();
        },
      }
    });
});

var notificacion = "";

function notificaciones() {

    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/notifications',
        type: 'GET',
        success: function(respuesta) {
            respuesta.data.forEach(function(item) {

                var html = `<tr ng-repeat="item in vm.notifications" class="ng-scope">
                    <th scope="row" class="ng-binding">${item.title}</th>
                    <td class="ng-binding">${item.body}</td>
                    <td class="ng-binding">${item.registration_date}</td>
                    <td><i title="Borrar" style="cursor: pointer;" class="fas fa-trash-alt" onclick=" eliminarNotificacion('${item._id}')"></i></td>
                </tr>`;

                notificacion += html;
            });

            $("#notitifacionesLista").append(notificacion);

        },
    });
}

function eliminarNotificacion(idNoti) {
    let datos = {
        _id: idNoti
    }
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/notifications/delete',
        type: 'POST',
        data: datos,
        success: function(respuesta) {
            
            setTimeout(function() {
                location.reload();
            }, 2000);
            Swal.fire({
                icon: 'success',
                title: 'Éxito',
                text: 'Notitifacion eliminada'
            });
        },
        error: function(er) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Error en el servidor.'
            })
        }
    });
}

function crearNotificacion() {
    let datos = {
        title: tituloMensaje,
        body: textMensaje
    }
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/notification',
        data: datos,
        type: 'POST',
        success: function(respuesta) {
            sendPush();
           setTimeout(function() {
                location.reload();
            }, 2000);
            Swal.fire({
                icon: 'success',
                title: 'Éxito',
                text: 'Notitifacion creada'
            });
        },
        error: function(er) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Error en el servidor.'
            })
        }
    });
}

function enableButton() {
    if (tituloMensaje !== '' && textMensaje !== '') {
        $("#sendNotification").removeAttr("disabled");
    } else {
        $("#sendNotification").prop("disabled", true);
    }
}

function sendPush() {
    $.ajax({
        url: `https://searchbexar.000webhostapp.com/push.php?titulo=${tituloMensaje}&mensaje=${textMensaje}`,
        type: 'GET',
        success: function(respuesta) {
            
            console.log("push sended")
        },
        error: function(er) {
           console.log(er)
        }
    });
}