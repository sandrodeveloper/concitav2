var arregloServicios = [];
var tipo = "";
var servicioDias = "";
var idServicio = "";

$(document).ready(function () {
  getData();
  servicios();
  obtener();
});

function pintarServicios() {
  $("#servicios").empty();
  var servicio = "";
  var costo = "";
  var titulo = "";
  var iconoCash = "";
  var iconoTar = "";
  var contadorSercicio = 0;
  arregloServicios.forEach(function (item) {
    costo = "";
    iconoCash = "";
    iconoTar = "";
    titulo = "";

    if (item.priceType == "quotation") {
      costo = "Sujeto a cotización.";
    }
    if (item.priceType == "free") {
      costo = "Gratuito.";
    }
    if (item.priceType == "price") {
      costo = "$" + item.cost + ".00";
      titulo = '<div class="t1 ">Tipo de pago.</div>';
      if (item.payment_methods[0] == "cash") {
        iconoCash = '<i class="far fa-money-bill-alt"></i>';
      }
      if (item.payment_methods[0] == "card") {
        iconoTar = '<i class="far fa-credit-card"></i>';
      }
      if (
        (item.payment_methods[0] == "card" &&
          item.payment_methods[1] == "cash") ||
        (item.payment_methods[1] == "card" && item.payment_methods[0] == "cash")
      ) {
        iconoCash = '<i class="far fa-money-bill-alt"></i>';
        iconoTar = '<i class="far fa-credit-card"></i>';
      }
    }
    var html =
      '<div class="col-lg-4  col-sm-12 " data-toggle="modal" data-target="#exampleModal" onclick="generarModal(' +
      contadorSercicio +
      ')">' +
      '<div id="1" class="item-servicio-usuario " style="cursor: pointer;">' +
      "<div>" +
      '<div class="d-image">' +
      '<img alt="" class="img-responsive" src="https://www.concita.com.mx:3000/api/v1/image?id=' +
      item.image +
      '">' +
      "</div>" +
      '<div class="d-info-servicio-usuario">' +
      '<div class="containerInfo">' +
      '<div class="containerPerfilCard ">' +
      '<p class="t1 binding"><img alt="imgProfileCard" class="imgProfileCard" src="img/user-default.png">' +
      item.provider.data["name"] +
      "</p>" +
      "</div>" +
      '<div class="d2">' +
      '<p class="t1">Categoria</p>' +
      '<p class="t2 binding">' +
      item.categories +
      "</p>" +
      "</div>" +
      '<div class="d2">' +
      '<p class="t1">Servicio</p>' +
      '<p class="t2 binding" title="Nissan Country">' +
      item.name +
      "</p>" +
      "</div>" +
      '<div class="t1 ">Costo</div>' +
      '<div class="t2 binding ">' +
      costo +
      "</div>" +
      titulo +
      iconoTar +
      " " +
      iconoCash +
      "</div>" +
      "</div>" +
      "</div>" +
      '<div class="d3 ">' +
      '<a data-toggle="modal" data-target="#exampleModal" type="button" class="btn btn-realizar-cita">Realizar cita</a>' +
      "</div>" +
      '<div class="d3">' +
      "</div>" +
      "</div>" +
      "</div>";
    servicio += html;
    contadorSercicio += 1;
  });

  $("#servicios").append(servicio);
  $("#totalServicios").text(contadorSercicio);
}

function servicios() {
  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/service?page=1&sort=todos",
    type: "GET",
    success: function (respuesta) {
      arregloServicios = respuesta.data;
      
      pintarServicios();
    },
  });
}

function buscador() {
  var busqueda = $("#key").val();
  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/service?page=1&query=" +
      busqueda +
      "&sort=todos",
    type: "GET",
    success: function (respuesta) {
      arregloServicios = respuesta.data;
      
      pintarServicios();
    },
  });
}

function generarModal(item) {
  servicioDias = "";
  htmldia = "";
  
  $("#horarios").empty();

  if (arregloServicios[item]["home_service"] == true) {
    tipo = "Si";
  } else {
    tipo = "No";
  }

  

  $("#nombreProvedor").text(arregloServicios[item].provider.data["name"]);
  $("#nombreServicio").text(arregloServicios[item]["name"]);
  $("#ubicacion").text(arregloServicios[item].provider.data["address"]);
  $("#tipoServicio").text(tipo);
  document.getElementById("imagenServicio").src =
    "https://www.concita.com.mx:3000/api/v1/image?id=" +
    arregloServicios[item]["image"];
  $("#phoneServicio").text(arregloServicios[item].provider.data["phone"]);
  $("#categoriaServicio").text(arregloServicios[item]["categories"]);
  $("#descripcionServicio").text(arregloServicios[item]["description"]);

  arregloServicios[item].available.forEach(function (dias) {
    if (dias.end >= 12 && dias.end <= 24) {
      horaFormacion = "pm.";
    } else {
      horaFormacion = "am.";
    }

    if (dias.start >= 12 && dias.end <= 24) {
      horaFormacionInicio = "pm.";
    } else {
      horaFormacionInicio = "am.";
    }
    var htmldia = `<div class="ng-scope">
                        <div id="diasModal" class="title ng-binding">
                            ${dias.days}
                        </div>
                        <div class="title ng-binding">
                            de ${dias.start}:00 ${horaFormacionInicio} a ${dias.end}:00 ${horaFormacion}
                        </div>
                        <br>
                    </div>`;
    servicioDias += htmldia;



  });

  $("#horarios").append(servicioDias);

  $("#diasModal").each(function () {
    var text = $(this).text();
    text = text.replace("Mo", "Lu");
    text = text.replace("Tu", " Ma");
    text = text.replace("We", " Mi");
    text = text.replace("Th", " Ju");
    text = text.replace("Fr", " Vi");
    text = text.replace("Sa", " Sa");
    text = text.replace("Su", " Do");
    $(this).text(text);
  });

  var tiempo = 0;
  var duracion = "";
  if (parseInt(arregloServicios[item]["duration"]) % 60 == 0) {
    tiempo = parseInt(arregloServicios[item]["duration"]) / 60;
    if (tiempo != 1) {
      duracion = tiempo + " horas";
    } else {
      duracion = tiempo + " hora";
    }
  } else {
    tiempo = parseInt(arregloServicios[item]["duration"]) % 60;
    duracion = tiempo + " minutos";
  }
  $("#duracionServicio").text(duracion);
  if (arregloServicios[item]["priceType"] == "quotation") {
    $(".paguitos").empty().append("Sujeto a cotización.");
    $(".pagos").hide();
    $(".paguitos").show();
  }
  if (arregloServicios[item]["priceType"] == "free") {
    $(".paguitos").empty().append("Gratuito.");
    $(".pagos").hide();
    $(".paguitos").show();
  }
  if (arregloServicios[item]["priceType"] == "price") {
    $(".pagos").show();
    $(".paguitos").hide();

    if (
      (arregloServicios[item]["payment_methods"][0] == "card" &&
        arregloServicios[item]["payment_methods"][1] == "card") ||
      (arregloServicios[item]["payment_methods"][1] == "card" &&
        arregloServicios[item]["payment_methods"][0] == "card")
    ) {
      /*document.getElementById("classPago").className = "far fa-credit-card";
      document.getElementById("classPagos").className = "far fa-money-bill-alt";*/
      $(".paguitos").empty().append("$" + arregloServicios[item]["cost"] + ".00");
      $(".pagos").hide();
      $(".paguitos").show();
    }

    if (arregloServicios[item]["payment_methods"][0] == "cash") {
      //document.getElementById("classPago").className = "far fa-money-bill-alt";
      $(".paguitos").empty().append("$" + arregloServicios[item]["cost"] + ".00");
      $(".pagos").hide();
      $(".paguitos").show();
    }

    if (arregloServicios[item]["payment_methods"][0] == "card") {
      //document.getElementById("classPago").className = "far fa-credit-card";
      $(".paguitos").empty().append("$" + arregloServicios[item]["cost"] + ".00");
      $(".pagos").hide();
      $(".paguitos").show();
    }
  }

  idServicio = arregloServicios[item]._id;
  localStorage.setItem("idService", idServicio);
}

function generarCita() {
  location.href = "generar-cita.html";
}

function obtener() {
  navigator.geolocation.getCurrentPosition(mostrar, gestionarErrores);
}

function mostrar(posicion) {
  localStorage.setItem("longitud", posicion.coords.longitude);
  localStorage.setItem("latitud", posicion.coords.latitude);
}

function gestionarErrores(error) {
  alert(
    "Error: " +
    error.code +
    " " +
    error.message +
    "\n\nPor favor compruebe que está conectado " +
    "a internet y habilite la opción permitir compartir ubicación física"
  );
}
