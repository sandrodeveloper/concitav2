function validarCampos(){
    if ($("#nombre").val() == "" || $("#nombre").val() == null || $("#nombre").val() == undefined) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Completar nombre"
        })
        return false;
    }
    if ($("#email").val() == "" || $("#email").val() == null || $("#email").val() == undefined) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Completar correo"
        })
        return false;
    }
    if ($("#phone").val() == "" || $("#phone").val() == null || $("#phone").val() == undefined) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Completar telefono"
        })
        return false;
    }
    if ($("#direccion").val() == "" || $("#direccion").val() == null || $("#direccion").val() == undefined) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Completar direccion"
        })
        return false;
    }
    if ($("#pass").val() == "" || $("#pass").val() == null || $("#pass").val() == undefined) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Completar contraseña"
        })
        return false;
    }
    if ($("#rfcin").val() == "" || $("#rfcin").val() == null || $("#rfcin").val() == undefined) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Completar rfc"
        })
        return false;
    }
    if ($("#direccionFiscal").val() == "" || $("#direccionFiscal").val() == null || $("#direccionFiscal").val() == undefined) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Completar direccion fiscal"
        })
        return false;
    }
    if ($("#nombreFiscal").val() == "" || $("#nombreFiscal").val() == null || $("#nombreFiscal").val() == undefined) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Completar nombre fiscal"
        })
        return false;
    }
    
    return true;
}
function crearProveedor() {

    let datos = {
        email: $("#email").val()
    }
    if(validarCampos()){
        $.ajax({
            url: 'https://www.concita.com.mx:3000/api/v1/checkIfExistProvider',
            data: datos,
            type: 'POST',
            success: function(respuesta) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: "El correo ya esta registrado"
                })
            },
            error: function(er) {
                completar()
            }
        });
    }
}

function completar() {
    var hash = md5($("#pass").val());

    let datos = {
        creditCard: "",
        provider: {
            name: $("#nombre").val(),
            email: $("#email").val(),
            password: $("pass").val(),
            phone: $("#phone").val(),
            address: $("#direccion").val(),
            addressfiscal: $("#direccionFiscal").val(),
            namefiscal: $("#nombreFiscal").val(),
            rfc: $("#rfcin").val(),
            pass: hash
        }
    }

    

    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/createprovidernopay',
        data: datos,
        type: 'POST',
        success: function(respuesta) {
            Swal.fire({
                icon: "success",
                title: "Éxito",
                text: "Proveedor creado correctamente",
                showCancelButton: false,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Excelente!",
              }).then((result) => {
                location.href = "dashboard-admin.html";
              });
        },
        error: function(er) {
            var json_mensaje = JSON.parse(er.responseText);
            
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: json_mensaje['message']
            })
        }
    });
}