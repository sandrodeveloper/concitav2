$(document).ready(function () {
  cate();
  nuevoHorario();
});
var vanilla;
let categoriesIds = [];
let categoriaSelect = [];
var categoriasDos = [];
var categoriasEditar = [];
var contIno = 0;
var domicilio = false;
var precio = "";
var contadorUno = 0;
var contadorDos = 0;
var esTipoTarjeta = "";
var esTipoDinero = "";
var contCat = 0;
var contadorHoras = 1;
var horarios = "";
var contenedorHorario = [];
var days = [];
var hours = [];
var carreglos = 0;

var catet = [];
var imag = "";

function cate() {
  $("#divCosto").hide();
  var categoriasPintar = "";

  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/category/",
    type: "GET",
    success: function (respuesta) {
      

      var colores = [
        "#B8B8B8",
        "#2B2A2A",
        "#C01533",
        "#FCBABE",
        "#FA757E",
        "#F8313E",
      ];

      var contadorColor = 0;
      
      respuesta.data.forEach(function (item) {
        var itemTemp = item;

        if (
          item.name != "Servicio de mantenimiento automotriz" &&
          item.name != "Citas Médicas"
        ) {
          if (item.name == "Nissan Guadalajara") {
            itemTemp = {
              icono: "",
              appname: item.name,
              nombre: item.name,
              color: colores[contadorColor],
              class: "d-item-modal-nissan",
            };
          } else {
            itemTemp = {
              icono: "fas " + item.icon,
              appname: item.name,
              nombre: item.name,
              color: colores[contadorColor],
              class: "",
            };
          }

          if (contadorColor < 5) {
            contadorColor++;
          } else {
            contadorColor = 0;
          }
        } else {
          if (item.name == "Servicio de mantenimiento automotriz") {
            itemTemp = {
              icono: "",
              appname: "Automotriz",
              nombre: "Servicio de mantenimiento automotriz",
              class: "automotriz",
            };
          } else {
            itemTemp = {
              icono: "",
              appname: "Citas Médicas",
              nombre: "Citas Médicas",
              class: "citas-medicas",
            };
          }
        }

        var html = `<div class="col-lg-6 col-md-6 ng-scope" onclick="cat('${item.name}', '${item._id}')">
                                 <div class="d-menu">
                                     <div id="${item._id}" class="item d-1 ${itemTemp.class}" style="background-color: ${itemTemp.color}">
                                         <i class="fas ${itemTemp.icono}"></i>
                                     </div>
                                     <p class="t1 ng-binding ng-scope">
                                     ${item.name}
                                 </div>
                             </div>`;
        categoriasPintar += html;
      });
      $("#categoriaIn").empty();
      $("#categoriaIn").append(categoriasPintar);
      respuesta.data.forEach(function (item2) {
        if (categoriasEditar.indexOf(item2.name) > -1) {
          cat(item2.name, item2._id);
          
        }
      });
    },
  });
}

function cat(nombre, id) {
  //catet.push(nombre);
  const index = categoriaSelect.indexOf(nombre);
  if (index > -1) {
    categoriaSelect.splice(index, 1);
    $(`#${id}`).removeClass("d-dia-active2");
    const index2 = categoriesIds.indexOf(id);
    categoriesIds.splice(index2, 1);
  } else {
    if (categoriaSelect.length < 3) {
      categoriaSelect.push(nombre);
      categoriesIds.push(id);
      $(`#${id}`).addClass("d-dia-active2");
    } else {
      categoriaSelect.shift();
      $(`#${categoriesIds.shift()}`).removeClass("d-dia-active2");
      categoriaSelect.push(nombre);
      categoriesIds.push(id);
      $(`#${id}`).addClass("d-dia-active2");
    }
  }
}

function siIn() {
  domicilio = true;
}

function noIn() {
  domicilio = false;
}

function gratis() {
  precio = "free";
  $("#divCosto").hide();
  $("#uno").addClass("d-dia-active");
  $("#dos").removeClass("d-dia-active");
  $("#tres").removeClass("d-dia-active");
}

function costo() {
  precio = "price";
  $("#divCosto").show();
  $("#dos").addClass("d-dia-active");
  $("#uno").removeClass("d-dia-active");
  $("#tres").removeClass("d-dia-active");
}

function cotizacion() {
  precio = "quotation";
  $("#divCosto").hide();
  $("#tres").addClass("d-dia-active");
  $("#dos").removeClass("d-dia-active");
  $("#uno").removeClass("d-dia-active");
}

function efectivoIn() {
  contadorUno++;
  if (contadorUno % 2 == 0) {
    $("#cashUno").removeClass("d-dia-active");
    esTipoDinero = "";
  } else {
    $("#cashUno").addClass("d-dia-active");
    esTipoDinero = "cash";
  }
}

function tarjetaIn() {
  contadorDos++;
  if (contadorDos % 2 == 0) {
    $("#tarjetaUno").removeClass("d-dia-active");
    esTipoTarjeta = "";
  } else {
    $("#tarjetaUno").addClass("d-dia-active");
    esTipoTarjeta = "card";
  }
}

function construct_service() {
  let newCost = isNaN($("#costoIn").val()) ? 0 : parseInt($("#costoIn").val());

  if (precio == "price") {
    if (esTipoDinero == "cash" && esTipoTarjeta == "card") {
      
      datos = {
        name: $("#name").val(),
        lender: $("#prestador").val(),
        description: $("#descripcion").val(),
        duration: parseInt($("#duracion").val()),
        space: parseInt($("#espacio").val()),
        timeAllowedToReschedule: parseInt($("#timeAllowedToReschedule").val()),
        home_servicedisable: false,
        spacedisable: false,
        home_service: domicilio,
        provider_id: localStorage.getItem("id"),
        priceType: precio,
        cost: newCost,
        payment_methods: ["card", "cash"],
        categories: categoriaSelect,
        available: contenedorHorario,
        minStart: Math.min(hours[0], hours[1]),
        maxEnd: Math.max(hours[0], hours[1]),
        image: imag,
      };
    } else {
      if (esTipoDinero == "cash") {
        
        datos = {
          name: $("#name").val(),
          lender: $("#prestador").val(),
          description: $("#descripcion").val(),
          duration: parseInt($("#duracion").val()),
          space: parseInt($("#espacio").val()),
          timeAllowedToReschedule: parseInt($("#timeAllowedToReschedule").val()),
          home_servicedisable: false,
          spacedisable: false,
          home_service: domicilio,
          provider_id: localStorage.getItem("id"),
          priceType: precio,
          cost: newCost,
          payment_methods: ["cash"],
          categories: categoriaSelect,
          available: contenedorHorario,
          minStart: Math.min(hours[0], hours[1]),
          maxEnd: Math.max(hours[0], hours[1]),
          image: imag,
        };
      }

      if (esTipoTarjeta == "card") {
        
        datos = {
          name: $("#name").val(),
          lender: $("#prestador").val(),
          description: $("#descripcion").val(),
          duration: parseInt($("#duracion").val()),
          space: parseInt($("#espacio").val()),
          timeAllowedToReschedule: parseInt($("#timeAllowedToReschedule").val()),
          home_servicedisable: false,
          spacedisable: false,
          home_service: domicilio,
          provider_id: localStorage.getItem("id"),
          priceType: precio,
          cost: newCost,
          payment_methods: ["card"],
          categories: categoriaSelect,
          available: contenedorHorario,
          minStart: Math.min(hours[0], hours[1]),
          maxEnd: Math.max(hours[0], hours[1]),
          image: imag,
        };
      }
    }
  } else {
    datos = {
      name: $("#name").val(),
      lender: $("#prestador").val(),
      description: $("#descripcion").val(),
      duration: parseInt($("#duracion").val()),
      space: parseInt($("#espacio").val()),
      timeAllowedToReschedule: parseInt($("#timeAllowedToReschedule").val()),
      home_servicedisable: false,
      spacedisable: false,
      home_service: domicilio,
      provider_id: localStorage.getItem("id"),
      priceType: precio,
      cost: newCost,
      categories: categoriaSelect,
      available: contenedorHorario,
      minStart: Math.min(hours[0], hours[1]),
      maxEnd: Math.max(hours[0], hours[1]),
      image: imag,
    };
  }

  
  var token = localStorage.getItem("token");
  var urlParams = new URLSearchParams(window.location.search);
  id = urlParams.get("id");
  let update_url = "";
  let type = "POST";
  if (id != null) {
    update_url = "/" + id;
    type = "PUT";
  }
  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/service" + update_url,
    data: datos,
    headers: {
      Authorization: token
    },
    type: type,
    success: function (respuesta) {
      
      Swal.fire({
        icon: "success",
        title: "Éxito",
        text: "Datos guardados correctamente",
        showCancelButton: false,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Excelente!",
      }).then((result) => {
        location.href = "services-proveedor.html";
      });
    },
    error: function (er) {
      
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Por favor verifique los campos.",
      });
    },
  });
}

function formularioValido() {

  if ($("#name").val() == "" || $("#name").val() == null) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Complete nombre del servicio",
    });
    return false;
  }
  if ($("#name").val().length < 5) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "El nombre del servicio debe contener 5 caracteres como mínimo",
    });
    return false;
  }
  if ($("#prestador").val() == "" || $("#prestador").val() == null) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Complete nombre del prestador",
    });
    return false;
  }
  if ($("#prestador").val().length < 5) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "El nombre del prestador debe contener 5 caracteres como mínimo",
    });
    return false;
  }
  if ($("#descripcion").val() == "" || $("#descripcion").val() == null) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Complete descripcion",
    });
    return false;
  }
  if ($("#descripcion").val().length < 10) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "La descripcion debe contener 10 caracteres como mínimo",
    });
    return false;
  }
  if ($("#espacio").val() == "" || $("#espacio").val() == null) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Complete cupo",
    });
    return false;
  }
  if ($("#timeAllowedToReschedule").val() == "" || $("#timeAllowedToReschedule").val() == null) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Complete reagendaciones",
    });
    return false;
  }
  if ($("#timeAllowedToReschedule").val() < 1) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "La cantidad de reagendaciones debe ser mayor que 0",
    });
    return false;
  }
  if ($("#duracion").val() == "" || $("#duracion").val() == null) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Complete duracion",
    });
    return false;
  }
  if (precio == "" || precio == null) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Complete costo de servicio",
    });
    return false;
  }
  if (precio == "price") {
    if (esTipoDinero == "" && esTipoTarjeta == "") {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Seleccione tipo de pago",
      });
      return false;
    }
    if ($("#costoIn").val() == "" || $("#costoIn").val() == null) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Complete el precio de servicio",
      });
      return false;
    }
  }

  return true;
}

function crearSer() {
  var datos = {};
  if (imag != "") {
    vanilla.result("base64").then(function (base64) {
      imag = base64;
      if (categoriaSelect.length > 0) {
        if (validarHorarioVacio()) {
          if (formularioValido()) {
            construct_service();
          }
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Complete selección de horario",
          });
        }
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Seleccione una categoria",
        });
      }
    });
  } else {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Seleccione una imagen",
    });
  }
}

function validarHorarioVacio() {
  var flag = true;
  contenedorHorario.forEach((element) => {
    if (element.days.length === 0) {
      flag = false;
    }
    if (element.hours.length < 2) {
      flag = false;
    }
  });
  return flag;
}

function imagen() {
  var input = document.getElementById("img");
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      imag = e.target.result;

      vanilla.bind({
        url: imag,
        orientation: 4,
      });
      //on button click
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function activarImput() {
  document.getElementById("img").click();
}

function diasSelect(dia, ndia, numero) {
  var existe = false;
  var eliminarExistente = 0;
  days = contenedorHorario[numero].days;

  days.forEach((element, index) => {
    if (element === dia) {
      existe = true;
      eliminarExistente = index;
    }
  });
  
  
  if (existe) {
    days.splice(eliminarExistente, 1);
  } else {
    days.push(dia);
  }
  
  contenedorHorario[numero] = {
    days: days,
    hours: hours,
    removable: false,
    start: Math.min(hours[0], hours[1]),
    end: Math.max(hours[0], hours[1]),
  };
  pintarDiasSeleccionados(days, numero);

  $("span[id^='days']").each(function () {
    var text = $(this).text();
    text = text.replace("Mo", "Lu");
    text = text.replace("Tu", "Ma");
    text = text.replace("We", "Mi");
    text = text.replace("Th", "Ju");
    text = text.replace("Fr", "Vi");
    text = text.replace("Sa", "Sa");
    text = text.replace("Su", "Do");
    $(this).text(text);
  });
}

function pintarDiasSeleccionados(days, numero) {
  $("#days" + numero).empty();
  $("#dMo" + numero).removeClass("d-dia-active");
  $("#dTu" + numero).removeClass("d-dia-active");
  $("#dWe" + numero).removeClass("d-dia-active");
  $("#dTh" + numero).removeClass("d-dia-active");
  $("#dFr" + numero).removeClass("d-dia-active");
  $("#dSa" + numero).removeClass("d-dia-active");
  $("#dSu" + numero).removeClass("d-dia-active");
  days.forEach((dia, index) => {
    $("#days" + numero).append(dia + " ");
    $("#d" + dia + numero).addClass("d-dia-active");
  });
}

function horaSelect(hora, numero) {
  if (hours.length == 2) {
    $("#hora" + hours[0] + "_" + numero).removeClass("d-dia-active");
    hours.splice(0, 1);
  }

  hours.push(hora);
  $("#hora" + hora + "_" + numero).addClass("d-dia-active");
  contenedorHorario[numero] = {
    days: days,
    hours: hours,
    removable: false,
    start: Math.min(hours[0], hours[1]),
    end: Math.max(hours[0], hours[1]),
  };
  if (hours.length == 2) {
    let minimo = Math.min(hours[0], hours[1]);
    let maximo = Math.max(hours[0], hours[1]);
    let hora_type = "am";
    if (minimo >= 12) {
      hora_type = "pm";
    }
    $("#inicio" + numero)
      .empty()
      .append("de: " + verify_two(check_number(minimo)) + ":00 " + hora_type);
    if (maximo >= 12) {
      hora_type = "pm";
    }
    $("#final" + numero)
      .empty()
      .append(" a: " + verify_two(check_number(maximo)) + ":00 " + hora_type);
  } else {
    let hora_type = "am";
    if (hours[0] >= 12) {
      hora_type = "pm";
    }
    $("#inicio" + numero)
      .empty()
      .append(verify_two(check_number(hours[0])) + ":00 " + hora_type);
    $("#final" + numero)
      .empty()
      .append("");
  }
  
}

function check_number(numero) {
  if (numero > 12) {
    return numero - 12;
  } else {
    return numero;
  }
}

function verify_two(numero) {
  if (numero >= 10) {
    return numero;
  } else {
    return "0" + numero;
  }
}

function otroHorario() {
  carreglos++;
  days = [];
  hours = [];
  

  nuevoHorario();
  crearBotonEliminar();
}

function crearBotonEliminar() {
  $("#cracionHoras").append(`<div class="row tableHorariosExtras"">
                                <button id="delBut${carreglos}" type="button" class="btn d-dia" style="width: 100% !important;" onclick="eliminarHorario(${carreglos})">
                                  Eliminar horario
                                </button>
                            </div>`);
}

function eliminarHorario(numero) {
  
  $(`#delBut${numero}`).remove();
  $(`#tableHorario${carreglos}`).remove();
  contenedorHorario.splice(numero, 1);
  
  pintarHorariosFromArray();
  carreglos--;
}

function ajustarArregloHorarioVisual() {}

function nuevoHorario() {
  //$("#cracionHoras").empty();
  contenedorHorario[carreglos] = {
    days: days,
    hours: hours,
    removable: false,
    start: Math.min(hours[0], hours[1]),
    end: Math.max(hours[0], hours[1]),
  };
  var classDetele = "";
  if (carreglos > 0) {
    classDetele = "tableHorariosExtras";
  }
  var htmlHora = ` <div class="${classDetele}" id="tableHorario${carreglos}">
        <span class="detailSelectHour ng-hide" id="days${carreglos}">
        </span>
        <span class="detailSelectHour ng-binding ng-hide" id="inicio${carreglos}">
            de 12:00 am
        </span>
        <span class="detailSelectHour marginBottom1em ng-binding ng-hide" id="final${carreglos}">
            a 12:00 am
        </span>

        <div class="row dias">
            <div id="dMo${carreglos}" onclick="diasSelect('Mo',1,${carreglos})" class="col d-dia ng-binding ng-scope">
                Lu
            </div>

            <div id="dTu${carreglos}" onclick="diasSelect('Tu',2,${carreglos})" class="col d-dia ng-binding ng-scope">
                Ma
            </div>

            <div id="dWe${carreglos}" onclick="diasSelect('We',3,${carreglos})" class="col d-dia ng-binding ng-scope">
                Mi
            </div>

            <div id="dTh${carreglos}" onclick="diasSelect('Th',4,${carreglos})" class="col d-dia ng-binding ng-scope">
                Ju
            </div>

            <div id="dFr${carreglos}" onclick="diasSelect('Fr',5,${carreglos})" class="col d-dia ng-binding ng-scope">
                Vi
            </div>

            <div id="dSa${carreglos}" onclick="diasSelect('Sa',6,${carreglos})" class="col d-dia ng-binding ng-scope">
                Sa
            </div>

            <div id="dSu${carreglos}" onclick="diasSelect('Su',7,${carreglos})" class="col d-dia ng-binding ng-scope">
                Do
            </div>
        </div>

        <ul class="filtros hour">
            <li id="hora1_${carreglos}" onclick="horaSelect('1',${carreglos})" class="ng-binding ng-scope">
                01:00 am
            </li>
            <li  id="hora2_${carreglos}" onclick="horaSelect('2',${carreglos})" class="ng-binding ng-scope">
                02:00 am
            </li>
            <li  id="hora3_${carreglos}" onclick="horaSelect('3',${carreglos})" class="ng-binding ng-scope">
                03:00 am
            </li>
            <li id="hora4_${carreglos}" onclick="horaSelect('4',${carreglos})" class="ng-binding ng-scope">
                04:00 am
            </li>
            <li id="hora5_${carreglos}" onclick="horaSelect('5',${carreglos})" class="ng-binding ng-scope">
                05:00 am
            </li>
            <li id="hora6_${carreglos}" onclick="horaSelect('6',${carreglos})" class="ng-binding ng-scope">
                06:00 am
            </li>
            <li id="hora7_${carreglos}" onclick="horaSelect('7',${carreglos})" class="ng-binding ng-scope">
                07:00 am
            </li>
            <li id="hora8_${carreglos}" onclick="horaSelect('8',${carreglos})" class="ng-binding ng-scope">
                08:00 am
            </li>
            <li id="hora9_${carreglos}" onclick="horaSelect('9',${carreglos})" class="ng-binding ng-scope">
                09:00 am
            </li>
            <li id="hora10_${carreglos}" onclick="horaSelect('10',${carreglos})" class="ng-binding ng-scope">
                10:00 am
            </li>
            <li id="hora11_${carreglos}" onclick="horaSelect('11',${carreglos})" class="ng-binding ng-scope">
                11:00 am
            </li>
            <li id="hora12_${carreglos}" onclick="horaSelect('12',${carreglos})" class="ng-binding ng-scope">
                12:00 pm
            </li>
            <li id="hora13_${carreglos}" onclick="horaSelect('13',${carreglos})" class="ng-binding ng-scope">
                01:00 pm
            </li>
            <li id="hora14_${carreglos}" onclick="horaSelect('14',${carreglos})" class="ng-binding ng-scope">
                02:00 pm
            </li>
            <li id="hora15_${carreglos}" onclick="horaSelect('15',${carreglos})" class="ng-binding ng-scope">
                03:00 pm
            </li>
            <li id="hora16_${carreglos}" onclick="horaSelect('16',${carreglos})" class="ng-binding ng-scope">
                04:00 pm
            </li>
            <li id="hora17_${carreglos}" onclick="horaSelect('17',${carreglos})" class="ng-binding ng-scope">
                05:00 pm
            </li>
            <li id="hora18_${carreglos}" onclick="horaSelect('18',${carreglos})" class="ng-binding ng-scope">
                06:00 pm
            </li>
            <li id="hora19_${carreglos}"  onclick="horaSelect('19',${carreglos})" class="ng-binding ng-scope">
                07:00 pm
            </li>
            <li id="hora20_${carreglos}" onclick="horaSelect('20',${carreglos})" class="ng-binding ng-scope">
                08:00 pm
            </li>
            <li id="hora21_${carreglos}"  onclick="horaSelect('21',${carreglos})" class="ng-binding ng-scope">
                09:00 pm
            </li>
            <li  id="hora22_${carreglos}" onclick="horaSelect('22',${carreglos})" class="ng-binding ng-scope">
                10:00 pm
            </li>
            <li id="hora23_${carreglos}" onclick="horaSelect('23',${carreglos})" class="ng-binding ng-scope">
                11:00 pm
            </li>
            <li  id="hora24_${carreglos}" onclick="horaSelect('24',${carreglos})" class="ng-binding ng-scope">
                12:00 am
            </li>
        </ul>
    </div>
       `;
  //horarios += htmlHora;

  $("#cracionHoras").append(htmlHora);
}

function pintarHorariosFromArray() {
  $(".tableHorariosExtras").remove();
  contenedorHorario.forEach((element, index) => {
    if (index !== 0) {
      var htmlHora = ` <div class="tableHorariosExtras" id="tableHorario${index}">
            <span class="detailSelectHour ng-hide" id="days${index}">
            </span>
            <span class="detailSelectHour ng-binding ng-hide" id="inicio${index}">
                de 12:00 am
            </span>
            <span class="detailSelectHour marginBottom1em ng-binding ng-hide" id="final${index}">
                a 12:00 am
            </span>

            <div class="row dias">
                <div id="dMo${index}" onclick="diasSelect('Mo',1,${index})" class="col d-dia ng-binding ng-scope">
                    Lu
                </div>

                <div id="dTu${index}" onclick="diasSelect('Tu',2,${index})" class="col d-dia ng-binding ng-scope">
                    Ma
                </div>

                <div id="dWe${index}" onclick="diasSelect('We',3,${index})" class="col d-dia ng-binding ng-scope">
                    Mi
                </div>

                <div id="dTh${index}" onclick="diasSelect('Th',4,${index})" class="col d-dia ng-binding ng-scope">
                    Ju
                </div>

                <div id="dFr${index}" onclick="diasSelect('Fr',5,${index})" class="col d-dia ng-binding ng-scope">
                    Vi
                </div>

                <div id="dSa${index}" onclick="diasSelect('Sa',6,${index})" class="col d-dia ng-binding ng-scope">
                    Sa
                </div>

                <div id="dSu${index}" onclick="diasSelect('Su',7,${index})" class="col d-dia ng-binding ng-scope">
                    Do
                </div>
            </div>

            <ul class="filtros hour">
                <li id="hora1_${index}" onclick="horaSelect('1',${index})" class="ng-binding ng-scope">
                    01:00 am
                </li>
                <li  id="hora2_${index}" onclick="horaSelect('2',${index})" class="ng-binding ng-scope">
                    02:00 am
                </li>
                <li  id="hora3_${index}" onclick="horaSelect('3',${index})" class="ng-binding ng-scope">
                    03:00 am
                </li>
                <li id="hora4_${index}" onclick="horaSelect('4',${index})" class="ng-binding ng-scope">
                    04:00 am
                </li>
                <li id="hora5_${index}" onclick="horaSelect('5',${index})" class="ng-binding ng-scope">
                    05:00 am
                </li>
                <li id="hora6_${index}" onclick="horaSelect('6',${index})" class="ng-binding ng-scope">
                    06:00 am
                </li>
                <li id="hora7_${index}" onclick="horaSelect('7',${index})" class="ng-binding ng-scope">
                    07:00 am
                </li>
                <li id="hora8_${index}" onclick="horaSelect('8',${index})" class="ng-binding ng-scope">
                    08:00 am
                </li>
                <li id="hora9_${index}" onclick="horaSelect('9',${index})" class="ng-binding ng-scope">
                    09:00 am
                </li>
                <li id="hora10_${index}" onclick="horaSelect('10',${index})" class="ng-binding ng-scope">
                    10:00 am
                </li>
                <li id="hora11_${index}" onclick="horaSelect('11',${index})" class="ng-binding ng-scope">
                    11:00 am
                </li>
                <li id="hora12_${index}" onclick="horaSelect('12',${index})" class="ng-binding ng-scope">
                    12:00 pm
                </li>
                <li id="hora13_${index}" onclick="horaSelect('13',${index})" class="ng-binding ng-scope">
                    01:00 pm
                </li>
                <li id="hora14_${index}" onclick="horaSelect('14',${index})" class="ng-binding ng-scope">
                    02:00 pm
                </li>
                <li id="hora15_${index}" onclick="horaSelect('15',${index})" class="ng-binding ng-scope">
                    03:00 pm
                </li>
                <li id="hora16_${index}" onclick="horaSelect('16',${index})" class="ng-binding ng-scope">
                    04:00 pm
                </li>
                <li id="hora17_${index}" onclick="horaSelect('17',${index})" class="ng-binding ng-scope">
                    05:00 pm
                </li>
                <li id="hora18_${index}" onclick="horaSelect('18',${index})" class="ng-binding ng-scope">
                    06:00 pm
                </li>
                <li id="hora19_${index}"  onclick="horaSelect('19',${index})" class="ng-binding ng-scope">
                    07:00 pm
                </li>
                <li id="hora20_${index}" onclick="horaSelect('20',${index})" class="ng-binding ng-scope">
                    08:00 pm
                </li>
                <li id="hora21_${index}"  onclick="horaSelect('21',${index})" class="ng-binding ng-scope">
                    09:00 pm
                </li>
                <li  id="hora22_${index}" onclick="horaSelect('22',${index})" class="ng-binding ng-scope">
                    10:00 pm
                </li>
                <li id="hora23_${index}" onclick="horaSelect('23',${index})" class="ng-binding ng-scope">
                    11:00 pm
                </li>
                <li  id="hora24_${index}" onclick="horaSelect('24',${index})" class="ng-binding ng-scope">
                    12:00 am
                </li>
            </ul>
        </div>
        <div class="row ng-scope"">
            <button id="delBut${index}" type="button" class="btn d-dia" style="width: 100% !important;" onclick="eliminarHorario(${index})">
              Eliminar Horario
            </button>
        </div>  
        `;
      //horarios += htmlHora;

      $("#cracionHoras").append(htmlHora);
      pintarDiasSeleccionados(element.days, index);
      element.hours.forEach((hora) => {
        $(`#hora${hora}_${index}`).addClass("d-dia-active");
      });
    }
  });
}
