// Dummy logging calls (ie, if tests are run in IE)
window.console = window.console || {};
window.
window.console.debug = window.console.debug || function(){};
window.console.info = window.console.info || function(){};
window.console.warn = window.console.warn || function(){};
window.console.error = window.console.error || function(){};

(function() {
   var modName, testName;

   //arg: { name }
    QUnit.testStart = function(t) {
        modName = t.module;
        testName = t.name;
    };

    //arg: { name, failed, passed, total }
    QUnit.testDone = function(t) {
        if (t.failed)
            
    };

    //{ result, actual, expected, message }
    QUnit.log = function(t) {
        if (!t.result)
            
    };
}());
