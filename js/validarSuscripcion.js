var membrecias = {
    "5cb5bee1e108670bda24bd37": {
        "image": null,
        "device_limit": 1,
        "status": "active",
        "_id": "5cb5bee1e108670bda24bd37",
        "name": "Básico",
        "code": "BASIC001",
        "description": "Basic package",
        "cost": 279,
        "positionDays": 1,
        "multiAppointment": false,
        "servicesMax": 10,
        "subAccount": 2,
        "openpayplan_id": "ptnfma4wtnhjchqbbnof",
        "paypal_plan_id_news": "P-0N835525CT311464RL5C26MI",
        "__v": 0
    },
    "5cb5bee1e108670bda24bd3f":{
        "image": null,
        "device_limit": 1,
        "status": "active",
        "_id": "5cb5bee1e108670bda24bd3f",
        "name": "Gold",
        "code": "GOLD001",
        "description": "Gold package",
        "cost": 579,
        "positionDays": 5,
        "multiAppointment": true,
        "servicesMax": 30,
        "subAccount": 10,
        "openpayplan_id": "ps0kuijvqllyzxntgtf2",
        "paypal_plan_id_news": "P-0N835525CT311464RL5C26MI",
        "__v": 0
    },
    "5cb5bee1e108670bda24bd3b":{
        "image": null,
        "device_limit": 1,
        "status": "active",
        "_id": "5cb5bee1e108670bda24bd3b",
        "name": "Premium",
        "code": "PREMIUM001",
        "description": "Premium package",
        "cost": 449,
        "positionDays": 3,
        "multiAppointment": true,
        "servicesMax": 20,
        "subAccount": 5,
        "openpayplan_id": "prvllutuguaaq3yziscl",
        "paypal_plan_id_news": "P-0N835525CT311464RL5C26MI",
        "__v": 0
    }
}

$(document).ready(function() {
    if(localStorage.getItem("id")!=null && localStorage.getItem('role')==='provider'){
        var idUser = localStorage.getItem("id");
        $.ajax({
          url: "https://www.concita.com.mx:3000/api/v1/provider/" + idUser,
          headers: {
            Authorization: localStorage.getItem("token"),
          },
          type: "GET",
          success: function (respuesta) {
            if(moment().isAfter(respuesta.data.membership.expiration_date)){
                if(respuesta.data.data.observations != null && respuesta.data.data.observations != ""){
                    var settings = {
                        "async": true,
                        "crossDomain": true,
                        "url": "https://api.paypal.com/v1/billing/subscriptions/"+respuesta.data.data.observations,
                        "method": "GET",
                        "headers": {
                          "authorization": "Basic "+btoa(userPaypal + ":" + passPaypal),
                          "cache-control": "no-cache"
                        }
                      }
                      $.ajax(settings).done(function (response) {
                        var desde = moment(respuesta.data.membership.expiration_date).subtract(1, 'months');
                        var ultimoPago = moment(response.billing_info.last_payment.time);
                        if(ultimoPago>desde){
                            actualizarExpiracion(respuesta.data.membership.membership,respuesta.data.data.observations,ultimoPago);
                        }else{
                            window.location.replace("seleccionar-suscripcion.html");
                        }
                      });
                }else{
                    window.location.replace("seleccionar-suscripcion.html");
                }
            }
          },
        });
    }else{
        window.location.replace("index.html");
    }
});

function actualizarExpiracion(id, observations,ultimoPago){
    var expi = ultimoPago.add(366 , 'days');
    let datos = {
        provider:localStorage.getItem('id'),
        membership: membrecias[id],
        amount: membrecias[id].cost,
        registration_date: JSON.parse(localStorage.getItem('userData')).registration_date,
        positionDays: membrecias[id].positionDays,
        observations: observations,
        fechaexpration: new Date(expi)
    }
    $.ajax({
        url: "https://www.concita.com.mx:3000/api/v1/payment",
        data: datos,
        type: "POST",
        success: function (respuesta) {
        },
        error: function (er) {
        },
      });

}