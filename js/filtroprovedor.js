var proveedor = "";
var servicioDias = "";
var az = "";
var za = "";
var general = "";
var urlProv = "";

function recien() {
    idProv = localStorage.getItem('id');
    urlProv = "https://www.concita.com.mx:3000/api/v1/services/" + idProv + "?page=1&order=-registration_date";
}

function azfil() {
    idProv = localStorage.getItem('id');
    urlProv = "https://www.concita.com.mx:3000/api/v1/services/" + idProv + "?page=1&order=name";
}

function zafil() {
    idProv = localStorage.getItem('id');
    urlProv = "https://www.concita.com.mx:3000/api/v1/services/" + idProv + "?page=1&order=-name";
}

function provedor() {
    var myId = localStorage.getItem('id');
    servicioBusquedproveedor = "";
    proveedor = "";
    $("#servicios").empty();
    $.ajax({
        url: urlProv,
        type: 'GET',
        success: function(respuesta) {
            
            var costo = "";
            var contadorSercicio = 0;
            arregloServicios = respuesta.data;

            

            respuesta.data.forEach(function(item) {
                arregloServicios[contadorSercicio];

                if (item.priceType == "quotation") {
                    costo = "Sujeto a cotización.";
                };

                if (item.priceType == "free") {
                    costo = "Gratuito.";
                };

                if (item.priceType == "price") {
                    var costo = "$" + item.cost + ".00";
                    titulo = '<div class="t1 ">Tipo de pago.</div>';

                    if (item.payment_methods[0] == 'cash') {
                        iconoCash = '<i class="far fa-money-bill-alt"></i>';
                    }

                    if (item.payment_methods[0] == 'card') {
                        iconoTar = '<i class="far fa-credit-card"></i>';
                    }

                    if (item.payment_methods[0] == 'card' && item.payment_methods[1] == 'cash' || item.payment_methods[1] == 'card' && item.payment_methods[0] == 'cash') {
                        iconoCash = '<i class="far fa-money-bill-alt"></i>';
                        iconoTar = '<i class="far fa-credit-card"></i>';
                    }

                }

                var html = `<div class="col-lg-4 col-sm-12 ng-scope">
                                <div class="item-servicio-usuario " style="cursor: pointer;">
                                    <div onclick="detalleProveedor('${item._id}')">
                                        <div class="d-image">
                                            <img alt="" class="img-responsive" crossorigin="*" src="https://www.concita.com.mx:3000/api/v1/image?id=${item.image}">
                                        </div>
                                        <div class="d-info-servicio-usuario">
                                            <div class="containerInfo">
                                                <div class="d2">
                                                    <p class="t1">Categoria</p>
                                                    <p class="t2 ng-binding">
                                                        ${item.categories}
                                                    </p>
                                                </div>
                                                <div class="d2">
                                                    <p class="t1">Servicio</p>
                                                    <p class="t2">
                                                        ${item.name}
                                                    </p>
                                                </div>
                                                <div class="t1">
                                                    Costo
                                                </div>
                                                <div class="t2 ">
                                                    ${costo}
                                                </div>
                                                <div class="t1">
                                                ${titulo}
                                            </div>
                                            <div class="t2 ">
                                            ${iconoCash}
                                            ${iconoTar}
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d3">
                                            <button  onclick="editars('${item._id}')" class="btn btn-realizar-cita ng-scope">
                                        Editar
                                        </button>
                                        <button type="button" class="btn btn-eliminar ng-scope" data-toggle="modal" onclick="modalGenerar('${item._id}','${item.name}')" data-target="#serviceDeleteConfirm">
                                        Eliminar
                                        </button>
                                    </div>
                                </div>
                            </div>`;
                proveedor += html;
                contadorSercicio += 1;
            });

            $("#servicios").append(proveedor);
            $('#totalServicios').text(contadorSercicio);

        },
    });

}