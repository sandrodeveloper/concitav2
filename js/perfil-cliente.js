let posibleChanges=false;
$(document).ready(function() {
    pintarPerfil();
});
var imag = "";

// function iniciar_address() {
//     var autocomplete;
//     autocomplete = new google.maps.places.Autocomplete(
//       document.getElementById("direccion"),
//       {
//         types: ["geocode"],
//       }
//     );
  
//     google.maps.event.addListener(autocomplete, "place_changed", function () {
//       var near_place = autocomplete.getPlace();
//       latitude = near_place.geometry.location.lat();
//       longitude = near_place.geometry.location.lng();
//       //
//     });
// }
function pintarPerfil() {
    imag = localStorage.getItem('picture')
    console.log(imag);
    if(imag === null || imag === undefined || imag ==='' || imag === 'undefined') {
        
    }else{
        $("#imagenMostrar").attr("src", "https://www.concita.com.mx:3000/api/v1/image?id="+imag);
    }
    //$("#imagenMostrar").attr("src", e.target.result);
    $('#name').val(localStorage.getItem('usuario'));
    $('#phone').val(localStorage.getItem('phone'))
}

function botonEditar() {
    $('#divEditar').hide();
    $('#divGuardado').show();
    $('#name').prop("disabled", false);
    $('#phone').prop("disabled", false);
    $('#imgText').prop("disabled", false);
    posibleChanges=true;
    //iniciar_address();
}

function botonCancelar() {
    $('#divEditar').show();
    $('#divGuardado').hide();
    $('#name').prop("disabled", true);
    $('#phone').prop("disabled", true);
    $('#imgText').prop("disabled", true);
    posibleChanges=false;
}

function imagen() {
    var input = document.getElementById("img");
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            
            imag = e.target.result.split(',')[1];
            $("#imagenMostrar").attr("src", e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function activarImput() {
    if(posibleChanges){
        document.getElementById("img").click();
        }
}

function actualizarPerfil() {
    var idUser = localStorage.getItem('id');

    let datos = {
        id: idUser,
        role: 'client',
        data: {
            full_name: $('#name').val(),
            email: localStorage.getItem('mail'),
            phone: $('#phone').val(),
            picture: imag,
        }
    }

    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/user/' + idUser,
        data: datos,
        type: 'PUT',
        success: function(respuesta) {
            
            localStorage.setItem("usuario", respuesta.data.data['full_name']);
            localStorage.setItem("phone", respuesta.data.data['phone']);
            localStorage.setItem("picture", respuesta.data.data['picture']);
            pintarPerfil();
            setTimeout(function() {
                // location.reload();
            }, 2000);
            Swal.fire({
                icon: 'success',
                title: 'Éxito',
                text: 'Datos guardados correctamente'
            });
        },
        error: function(er) {
            var json_mensaje = JSON.parse(er.responseText);
            
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: json_mensaje['message']
            })
        }
    });
}