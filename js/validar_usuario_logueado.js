$(document).ready(function () {
  let urlParams = window.location.href;
  if (urlParams.includes("activate")) {
    urlParams = urlParams.split("?code=");
    urlParams = urlParams[1].split("&id=");
    var userId = urlParams[1];
    var code = urlParams[0];
    if (userId != null) {
      let datos = { id: userId, code: code };
      $.ajax({
        url: "https://www.concita.com.mx:3000/api/v1/activeClient",
        data: datos,
        type: "POST",
        success: function (respuesta) {
            Swal.fire({
                icon: 'success',
                title: 'Éxito',
                text: 'Se ha validado tu cuenta con éxito.'
            });
        },
        error: function (er) {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Error en el servidor",
          });
        },
      });
    }
  }
});
