function registroClientes() {

    var hash = md5($("#password").val());

    let datos = {
        name: $("#name").val(),
        email: $("#email").val(),
        pass: hash,
        phone: $("#phone").val()
    }

    

    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/createclient',
        data: datos,
        type: 'POST',
        success: function(respuesta) {
            location.href = "activacion.html"
        },
        error: function(er) {
            var json_mensaje = JSON.parse(er.responseText);
            
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: json_mensaje['message']
            })
        }
    });
}