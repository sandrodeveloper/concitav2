var proveedor = "";
var servicioDias = "";
var titulo = "";
var iconoTar = "";
var iconoCash = "";
var servicioBusquedproveedor = "";
var idUsuario = "";
var costo = "";
var idSer = "";
var nombreSer = "";
$(document).ready(function() {
    servicios();
});


function servicios() {
    idUsuario = localStorage.getItem('id');
    var token = localStorage.getItem('token');
    $.ajax({
        url: `https://www.concita.com.mx:3000/api/v1/providerapptgroup/${idUsuario}?status=accepted`,
        headers: { 'Authorization': token },
        data: {},
        type: 'GET',
        success: function(respuesta) {
            var serviciosConCitasArray = respuesta.data;
            $.ajax({
                url: 'https://www.concita.com.mx:3000/api/v1/services/' + idUsuario + '?page=1',
                type: 'GET',
                success: function(respuesta) {
                    
                    costo = "";
                    var contadorSercicio = 0;
                    arregloServicios = respuesta.data;
        
                    
                    if (respuesta.data == null || respuesta.data == undefined || respuesta.data == [] || respuesta.data == "") {
                        var html = ``;
                        proveedor += html;
                    } else {
                        respuesta.data.forEach(function(item) {
                            arregloServicios[contadorSercicio];
        
                            if (item.priceType == "quotation") {
                                costo = "Sujeto a cotización.";
                            };
        
                            if (item.priceType == "free") {
                                costo = "Gratuito.";
                            };
        
                            if (item.priceType == "price") {
                                var costo = "$" + item.cost + ".00";
                                titulo = '<div class="t1 ">Tipo de pago.</div>';
        
                                if (item.payment_methods[0] == 'cash') {
                                    iconoCash = '<i class="far fa-money-bill-alt"></i>';
                                }
        
                                if (item.payment_methods[0] == 'card') {
                                    iconoTar = '<i class="far fa-credit-card"></i>';
                                }
        
                                if (item.payment_methods[0] == 'card' && item.payment_methods[1] == 'cash' || item.payment_methods[1] == 'card' && item.payment_methods[0] == 'cash') {
                                    iconoCash = '<i class="far fa-money-bill-alt"></i>';
                                    iconoTar = '<i class="far fa-credit-card"></i>';
                                }
        
                            }
        
                            var html = `<div class="col-lg-4 col-sm-12 ng-scope">
                                            <div class="item-servicio-usuario " style="cursor: pointer;">
                                                <div onclick="detalleProveedor('${item._id}')">`;
                                var citasReservadasObjeto = _.find(serviciosConCitasArray, {_id:item._id})
                                
                                if(citasReservadasObjeto != null && citasReservadasObjeto != undefined){
                                    html +=         `<div class="warning ng-binding">
                                                        <i class="fa fa-user" aria-hidden="true"></i>(${citasReservadasObjeto.count})
                                                    </div>`
                                }

                                    html +=         `<div class="d-image">
                                                        <img alt="" class="img-responsive" crossorigin="*" src="https://www.concita.com.mx:3000/api/v1/image?id=${item.image}">
                                                    </div>
                                                    <div class="d-info-servicio-usuario">
                                                        <div class="containerInfo">
                                                            <div class="d2">
                                                                <p class="t1">Categoria</p>
                                                                <p class="t2 ng-binding">
                                                                    ${item.categories}
                                                                </p>
                                                            </div>
                                                            <div class="d2">
                                                                <p class="t1">Servicio</p>
                                                                <p class="t2" title="VW Lizen Patria (Frente Andares)">
                                                                    ${item.name}
                                                                </p>
                                                            </div>
                                                            <div class="t1">
                                                                Costo
                                                            </div>
                                                            <div class="t2 ">
                                                                ${costo}
                                                            </div>
                                                            <div class="t1">
                                                            ${titulo}
                                                        </div>
                                                        <div class="t2 ">
                                                        ${iconoCash}
                                                        ${iconoTar}
                                                    </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d3">
                                                    <button type="button" onclick="generarCitaProveedor('${item._id}')" class="btn btn-realizar-cita ng-scope">
                                                        Agendar
                                                    </button>
                                                    <button type="button" onclick="editars('${item._id}')" class="btn btn-realizar-cita ng-scope">
                                                        Editar
                                                    </button>
                                                </div>
                                                <div class="d3">
                                                    <button type="button" class="btn btn-eliminar ng-scope" data-toggle="modal" onclick="modalGenerar('${item._id}','${item.name}')" data-target="#serviceDeleteConfirm">
                                                        Eliminar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>`;
                            proveedor += html;
                            contadorSercicio += 1;
                        });
                    }
                    $("#servicios").append(proveedor);
                    $('#totalServicios').text(contadorSercicio);
        
                },
            });
        },
        error: function(er) {
        }
    });
}

function detalleProveedor(id) {
    location.href = "detalle-proveedor.html?idservice=" + id;
}

function generarCitaProveedor(idService) {
    localStorage.setItem("idService", idService);
    location.href = "generar-cita-proveedor.html?type=prov";
}

function buscador() {
    servicioBusquedproveedor = "";
    var busqueda = $('#key').val();
    $("#servicios").empty();
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/services/' + idUsuario + '?page=1&query=' + busqueda,
        type: 'GET',
        success: function(respuesta) {
            
            costo = "";
            var contadorSercicio = 0;
            arregloServicios = respuesta.data;

            

            respuesta.data.forEach(function(item) {
                arregloServicios[contadorSercicio];
                if (item.priceType == "quotation") {
                    costo = "Sujeto a cotización.";
                };

                if (item.priceType == "free") {
                    costo = "Gratuito.";
                };

                if (item.priceType == "price") {
                    var costo = "$" + item.cost + ".00";
                    titulo = '<div class="t1 ">Tipo de pago.</div>';

                    if (item.payment_methods[0] == 'cash') {
                        iconoCash = '<i class="far fa-money-bill-alt"></i>';
                    }

                    if (item.payment_methods[0] == 'card') {
                        iconoTar = '<i class="far fa-credit-card"></i>';
                    }

                    if (item.payment_methods[0] == 'card' && item.payment_methods[1] == 'cash' || item.payment_methods[1] == 'card' && item.payment_methods[0] == 'cash') {
                        iconoCash = '<i class="far fa-money-bill-alt"></i>';
                        iconoTar = '<i class="far fa-credit-card"></i>';
                    }

                }

                var html = `<div class="col-lg-4 col-sm-12 ng-scope">
                                <div class="item-servicio-usuario " style="cursor: pointer;">
                                    <div onclick="detalleProveedor('${item._id}')">
                                        <div class="d-image">
                                            <img alt="" class="img-responsive" crossorigin="*" src="https://www.concita.com.mx:3000/api/v1/image?id=${item.image}">
                                        </div>
                                        <div class="d-info-servicio-usuario">
                                            <div class="containerInfo">
                                                <div class="d2">
                                                    <p class="t1">Categoria</p>
                                                    <p class="t2 ng-binding">
                                                        ${item.categories}
                                                    </p>
                                                </div>
                                                <div class="d2">
                                                    <p class="t1">Servicio</p>
                                                    <p class="t2">
                                                        ${item.name}
                                                    </p>
                                                </div>
                                                <div class="t1">
                                                    Costo
                                                </div>
                                                <div class="t2 ">
                                                    ${costo}
                                                </div>
                                                <div class="t1">
                                                ${titulo}
                                            </div>
                                            <div class="t2 ">
                                            ${iconoCash}
                                            ${iconoTar}
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d3">
                                            <button  onclick="editars('${item._id}')" class="btn btn-realizar-cita ng-scope">
                                        Editar
                                        </button>
                                        <button type="button" class="btn btn-eliminar ng-scope" data-toggle="modal" onclick="modalGenerar('${item._id}','${item.name}')" data-target="#serviceDeleteConfirm">
                                        Eliminar
                                        </button>
                                    </div>
                                </div>
                            </div>`;
                servicioBusquedproveedor += html;
                contadorSercicio += 1;
            });

            $("#servicios").append(servicioBusquedproveedor);
            $('#totalServicios').text(contadorSercicio);

        },
    });
}

function editars(idServicio) {
    location.href = "editarServicio.html?id=" + idServicio;
}

function modalGenerar(id, nombre) {
    
    $('#nombre').text(nombre);
    idSer = id;

}

function eliminar() {
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/service/' + idSer,
        type: 'DELETE',
        success: function(respuesta) {
            
            setTimeout(function() {
                location.reload();
            }, 2000);
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Se elimino correctamente',
                showConfirmButton: false,
                timer: 1500
            });
        },
        error: function(er) {
            if (er.responseJSON.message == "El servicio cuenta con citas agendadas. En caso de querer eliminarlo es necesario que no cuente con citas") {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Se deben cancelar las citas antes de eliminar el servicio.'
                })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Error en el servidor'
                })
            }
        }
    });

}