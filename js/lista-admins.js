$(document).ready(function() {
    pintarTabla();

});

var admins = [];

function pintarTabla() {
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/admin',
        data: {},
        type: 'GET',
        success: function(respuesta) {
            var servicio = "";
            var contadorSercicio = 0;
            admins = respuesta.data;
            admins.forEach(function(item) {

                var ultimaSesion = "NA";

                if (item.lastSession != null && item.lastSession != "" && item.lastSession != undefined) {
                    ultimaSesion = item.lastSession.substr(0,10).replace(/-/g,"/");
                }

                var html = `<tr>
                <td style="width: 10% !important;" class="ng-binding">` + item.name + `</td>
                <td style="width: 10% !important;" class="ng-binding">` + item.email + `</td>
                <td style="width: 10% !important;" class="ng-binding">${ultimaSesion}</td>
                <td style="width: 5% !important;"><button class="botonAdquirir" onclick="eliminarAdmin(${contadorSercicio})">Eliminar</button></td>
                </tr>
               `;
                servicio += html;
                contadorSercicio += 1;
            });

            $("#servicios").empty();
            $("#servicios").append(servicio);

        }
    });
}

function eliminarAdmin(numero) {
    let datos = {
        id: admins[numero]._id
    }
    $.ajax({
        url: "https://www.concita.com.mx:3000/api/v1/admin/delete",
        data: datos,
        type: "POST",
        success: function (respuesta) {
          
          Swal.fire({
            icon: "success",
            title: "Éxito",
            text: "Usuario eliminado correctamente",
            showCancelButton: false,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Excelente!",
          }).then((result) => {
          });
          pintarTabla()
        },
        error: function (er) {
          
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Por favor verifique los campos.",
          });
        },
      });
}