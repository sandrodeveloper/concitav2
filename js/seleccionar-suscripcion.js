var arraySuscriptions = []
$(document).ready(function() {
    var sus = ""
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/memberships',
        type: 'GET',
        success: function(respuesta) {
            
            arraySuscriptions = respuesta.data.sort(function(a,b){ 
                return a.cost - b.cost;
            });

            arraySuscriptions.forEach(function(item,index) {
                var domicilio="Una cita a la vez a domicilio";
                var days="";

                if (item.multiAppointment) {
                    domicilio = "Opción de cita multiple y a domicilio"
                }
                if (item.positionDays > 1) {
                    days ="s"
                }

                var html =
                    `<div class="col-lg-4 col-md-12 col-12 wow fadeInUp" data-wow-delay=".2s">
                        <div class="div-item-paquetes">
                            <div class="title-item-paquetes">
                                <p class="p1">${item.name}</p>
                                <p class="p2">$ ${item.cost}.<sup>00</sup></p>
                                <p class="p3">Por año + IVA</p>
                                <hr>
                            </div>

                            <div class="info-item-paquetes">
                                <p class="p1">${item.subAccount} Usuarios como proveedor<br>${item.servicesMax} diferentes opciones de servicio
                                    <br>${domicilio}<br>Posicionamiento por ${item.positionDays} día${days}</p>
                                <a class="btn" href="#" role="button" onclick="selectPackage(${index})">Comprar</a>
                            </div>

                        </div>
                    </div>`;

                    if (item.status==='active') {
                        sus += html;
                    }
            });

            $("#suscriptions-list").append(sus);

        },
    });
});

function selectPackage(index) {
    localStorage.setItem("suscription", JSON.stringify(arraySuscriptions[index]))
    location.href = "pagar-suscripcion.html"
}