var latitude = 0;
var longitude = 0;
let posibleChanges=false;
var suscripcion = "";
$(document).ready(function () {
  pintarPerfil();
});

var imag = "";
var idUser = localStorage.getItem("id");
var jsonIn = [];

function pintarPerfil() {
  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/provider/" + idUser,
    headers: {
      Authorization: localStorage.getItem("token"),
    },
    type: "GET",
    success: function (respuesta) {
      
      jsonIn = respuesta;
      $("#name").val(respuesta.data.data.name);
      $("#phone").val(respuesta.data.data.phone);
      $("#email").val(respuesta.data.email);
      $("#direccion").val(respuesta.data.data.address);
      suscripcion=respuesta.data.data.observations;
      localStorage.setItem("usuario", respuesta.data.data["name"]);
      localStorage.setItem("phone", respuesta.data.data["phone"]);
      localStorage.setItem("address", respuesta.data.data["address"]);
      localStorage.setItem("picture", respuesta.data.data['picture']);
      imag = localStorage.getItem('picture')
      if(imag === null || imag === undefined || imag ==='' || imag === 'undefined') {
          
      }else{
          $("#imagenMostrar").attr("src", "https://www.concita.com.mx:3000/api/v1/image?id="+imag);
      }
    },
  });
}
function iniciar_address() {
  var autocomplete;
  autocomplete = new google.maps.places.Autocomplete(
    document.getElementById("direccion"),
    {
      types: ["geocode"],
    }
  );

  google.maps.event.addListener(autocomplete, "place_changed", function () {
    var near_place = autocomplete.getPlace();
    latitude = near_place.geometry.location.lat();
    longitude = near_place.geometry.location.lng();
    //
  });
}
function botonEditar() {
  $("#divEditar").hide();
  $("#divGuardado").show();
  $("#name").prop("disabled", false);
  $("#phone").prop("disabled", false);
  $("#direccion").prop("disabled", false);
  $("#imgText").prop("disabled", false);
  posibleChanges=true;
  iniciar_address();
}

function botonCancelar() {
  $("#divEditar").show();
  $("#divGuardado").hide();
  $("#name").prop("disabled", true);
  $("#phone").prop("disabled", true);
  $("#direccion").prop("disabled", true);
  $("#imgText").prop("disabled", true);
  posibleChanges=false;
}

function imagen() {
  var input = document.getElementById("img");
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      
      imag = e.target.result.split(',')[1];
      $("#imagenMostrar").attr("src", e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function activarImput() {
  if(posibleChanges){
  document.getElementById("img").click();
  }
}

function actualizarPerfil() {
  jsonIn.data.data.name = $("#name").val();
  jsonIn.data.data.phone = $("#phone").val();
  jsonIn.data.data.address = $("#direccion").val();
  jsonIn.data.role = "provider";
  jsonIn.data.data.picture = imag;
  jsonIn.data.data.loc = [latitude, longitude]
  

  $.ajax({
    url: "https://www.concita.com.mx:3000/api/v1/user/" + idUser,
    data: jsonIn.data,
    headers: {
      Authorization: localStorage.getItem("token"),
    },
    type: "PUT",
    success: function (respuesta) {
      
      localStorage.setItem("usuario", respuesta.data.data["name"]);
      localStorage.setItem("phone", respuesta.data.data["phone"]);
      localStorage.setItem("address", respuesta.data.data["address"]);
      localStorage.setItem("picture", respuesta.data.data['picture']);
      pintarPerfil();
      setTimeout(function () {
        location.reload();
      }, 2000);
      Swal.fire({
        icon: "success",
        title: "Éxito",
        text: "Datos guardados correctamente",
      });
    },
    error: function (er) {
      setTimeout(function () {
        location.reload();
      }, 2000);
      Swal.fire({
        icon: "success",
        title: "Éxito",
        text: "Datos guardados correctamente",
      });
    },
  });
}

function cancelarSuscripcion(){
  $.ajax({
    async: true,
    crossDomain: true,
    url: "https://api.paypal.com/v1/billing/subscriptions/"+suscripcion+'/cancel',
    data: "{\r\n  \"reason\": \"Finalizar mi suscripcion\"\r\n}",
    headers: {
      "authorization": "Basic "+btoa(userPaypal + ":" + passPaypal),
      "content-type": "application/json",
      "cache-control": "no-cache"
    },
    "processData": false,
    type: "POST",
    success: function (respuesta) {
      Swal.fire({
          icon: 'success',
          title: 'Exito',
          text: "Suscripcion cancelada"
      })
    },
    error: function (er) {
      Swal.fire({
          icon: 'success',
          title: 'Exito',
          text: "Suscripcion cancelada"
      })
    },
  });
}
