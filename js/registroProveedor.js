$(document).ready(function() {
    mostrar();
});

function comprovacion() {

    let datos = {
        email: $("#email").val()
    }

    

    if ($("#email").val() == "" || $("#email").val() == null || $("#email").val() == undefined) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Completar todos los campos"
        })
    } else {

        $.ajax({
            url: 'https://www.concita.com.mx:3000/api/v1/checkIfExistProvider',
            data: datos,
            type: 'POST',
            success: function(respuesta) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: "El correo ya esta registrado"
                })
            },
            error: function(er) {
                localStorage.setItem('mailProvedor', $("#email").val());
                localStorage.setItem('nombreProvedor', $("#empresaName").val());
                location.href = "detalles-registro-proveedor.html"
            }
        });
    }
}

function completar() {
    var hash = md5($("#pass").val());

    let datos = {
        creditCard: "",
        provider: {
            name: $("#nombre").val(),
            email: $("#email").val(),
            password: $("pass").val(),
            phone: $("#phone").val(),
            address: $("#direccion").val(),
            addressfiscal: $("#direccionFiscal").val(),
            namefiscal: $("#nombreFiscal").val(),
            rfc: $("#rfcin").val(),
            pass: hash
        }
    }

    

    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/createprovidernopay',
        data: datos,
        type: 'POST',
        success: function(respuesta) {
            console.log(respuesta);
            suscripcionInicial();
        },
        error: function(er) {
            var json_mensaje = JSON.parse(er.responseText);
            
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: json_mensaje['message']
            })
        }
    });
}

function mostrar() {
    
    


    $("#nombre").val(localStorage.getItem('nombreProvedor'));
    $("#email").val(localStorage.getItem('mailProvedor'));
}

function suscripcionInicial(){
    var hash = md5($("#pass").val());
    let datos = {
        email: $("#email").val(),
        pass: hash,
    }
    var membrecia = {
        "image": null,
        "device_limit": 1,
        "status": "inactive",
        "_id": "5d617c5312886305273e249a",
        "name": "Gold package",
        "code": "GOLD002",
        "description": "Gold package",
        "cost": 579,
        "positionDays": 5,
        "multiAppointment": true,
        "servicesMax": 30,
        "subAccount": 10,
        "openpayplan_id": "pglsijefouymmmrz8n4o",
        "paypal_plan_id_news": "P-0N835525CT311464RL5C26MI",
        "__v": 0
    }
    
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/authenticate',
        data: datos,
        type: 'POST',
        success: function(respuesta) {
            var provoviderId = ""
            if(respuesta.data.provider !== undefined){
                provoviderId = respuesta.data.provider.id
            }
            if (respuesta.data['role'] == "provider") {
                provoviderId = respuesta.data.id
            }
            var expi = moment().add(3, 'M');
            let datos = {
                provider:provoviderId,
                membership: membrecia,
                amount: membrecia.cost,
                registration_date: new Date(),
                positionDays: membrecia.positionDays,
                observations: '',
                fechaexpration: new Date(expi)
            }
            $.ajax({
                url: "https://www.concita.com.mx:3000/api/v1/payment",
                data: datos,
                type: "POST",
                success: function (respuesta) {
                    location.href = "login.html"
                }
            });
        }
    });
}