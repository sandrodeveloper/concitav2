function eventoCrear() {
    if ($('#name').val() == "" || $('#tipo').val() == "" || $('#descripcion').val() == "" || new Date($('#fin').val()) == "Invalid Date" || new Date($('#inicio').val()) == "Invalid Date") {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Llenar todos los campos.'
        })
    } else {
        var token = localStorage.getItem('token');

        let datos = {
            name: $('#name').val(),
            type: $('#tipo').val(),
            description: $('#descripcion').val(),
            from_date: new Date($('#inicio').val()),
            due_date: new Date($('#fin').val())
        }
        
        $.ajax({
            url: 'https://www.concita.com.mx:3000/api/v1/event',
            headers: { 'Authorization': token },
            data: datos,
            type: 'POST',
            success: function(respuesta) {
                
                setTimeout(function() {
                    location.href = "agenda-proveedor.html";
                }, 2000);
                Swal.fire({
                    icon: 'success',
                    title: 'Éxito',
                    text: 'Horario creado correctamente.'
                });
            },
            error: function(er) {
                if (er.message = "Ya existe un evento en las fechas indicadas") {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Ya existe un evento en las fechas indicadas'
                    })
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Error en el servidor.'
                    })
                }

            }
        });
    }
}