$(document).ready(function() {
    categoria();
});
var categorias = "";
var urlIn = "https://www.concita.com.mx:3000/api/v1/service?page=1&sort=todos";
var lon = "";
var lat = "";
var catSelect = "";
var colores = [
    "#FCBABE",
    "#FA757E",
    "#F8313E",
    "#2B2A2A",
    "#B8B8B8",
    "#F8313E",
    "#FCBABE",
    "#FA757E",
]

function categoria() {
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/category/',
        type: 'GET',
        success: function(respuesta) {
            
            var contadorColor = 0;
            respuesta.data.forEach(function(item,index) {
                var itemTemp = item;
                if (item.name != "Servicio de mantenimiento automotriz" && item.name != "Citas Médicas") {
                    if (item.name == "Nissan Guadalajara") {
                        itemTemp = { icono: '', appname: item.name, nombre: item.name, color: colores[contadorColor], class: 'd-item-modal-nissan' };
                    } else {
                        itemTemp = { icono: 'fas ' + item.icon, appname: item.name, nombre: item.name, color: colores[contadorColor], class: '' };
                    }
                } else {
                    if (item.name == "Servicio de mantenimiento automotriz") {
                        itemTemp = { icono: "", appname: "Automotriz", nombre: "Servicio de mantenimiento automotriz", class: 'automotriz' };
                    } else {
                        itemTemp = { icono: "", appname: "Citas Médicas", nombre: "Citas Médicas", class: 'citas-medicas' };
                    }
                }
                var html = `<div class="col-lg-3 col-md-3">
                                    <div class="d-menu">
                                        <div class="item d-1 ${itemTemp.class}" id="categoria-${index}" style="background-color: ${itemTemp.color}" onclick="cat('${item.name}','categoria-${index}')">
                                            <i class="fas ${itemTemp.icono}"></i>
                                        </div>
                                        <p class="t1">${item.name}</p>
                                    </div>
                                </div>`;
                categorias += html;
                if (contadorColor < 7) {
                    contadorColor++;
                } else {
                    contadorColor = 0;
                }
            });

            $("#cataegoriaTotal").append(categorias);
        },
    });
}

function cat(prop,item) {
    var lon = localStorage.getItem('longitud');
    var lat = localStorage.getItem('latitud');
    $(".d-dia-active2").each(function() {
        $(this).removeClass("d-dia-active2");
    });
    if(catSelect == prop){
        catSelect = "";
    }else{
        catSelect = prop;
        $('#'+item).addClass("d-dia-active2");
    }
    urlIn = "https://www.concita.com.mx:3000/api/v1/service?page=1&type=" + catSelect + "&sort=todos&lat=" + lat + "&lng=" + lon;
}

function todos() {
    var lon = localStorage.getItem('longitud');
    var lat = localStorage.getItem('latitud');
    if (catSelect == "" || catSelect == null || catSelect == false) {
        urlIn = "https://www.concita.com.mx:3000/api/v1/service?page=1&sort=todos&lat=" + lat + "&lng=" + lon;
    } else {
        urlIn = "https://www.concita.com.mx:3000/api/v1/service?page=1&type=" + catSelect + "&sort=todos&lat=" + lat + "&lng=" + lon;
    }
    limpiarFiltos();
    $('#todos').addClass("active");
}

function populares() {
    var lon = localStorage.getItem('longitud');
    var lat = localStorage.getItem('latitud');
    if (catSelect == "" || catSelect == null || catSelect == false) {
        urlIn = "https://www.concita.com.mx:3000/api/v1/service?page=1&sort=solicitados&lat=" + lat + "&lng=" + lon;
    } else {
        urlIn = "https://www.concita.com.mx:3000/api/v1/service?page=1&type=" + catSelect + "&sort=solicitados&lat=" + lat + "&lng=" + lon;
    }
    limpiarFiltos();
    $('#populares').addClass("active");

}

function cercanos() {
    var lon = localStorage.getItem('longitud');
    var lat = localStorage.getItem('latitud');
    if (catSelect == "" || catSelect == null || catSelect == false) {
        urlIn = "https://www.concita.com.mx:3000/api/v1/service?page=1&sort=cercanos&lat=" + lat + "&lng=" + lon;
    } else {
        urlIn = "https://www.concita.com.mx:3000/api/v1/service?page=1&type=" + catSelect + "&sort=cercanos&lat=" + lat + "&lng=" + lon;
    }
    limpiarFiltos();
    $('#cercanos').addClass("active");

}

function limpiarFiltos(){
    $('#cercanos').removeClass("active");
    $('#populares').removeClass("active");
    $('#todos').removeClass("active");
}

function filtrar() {
    var busqueda = $('#key').val();
    $.ajax({
        url: urlIn,
        type: 'GET',
        success: function(respuesta) {
            arregloServicios = respuesta.data;
            
            pintarServicios();
            $('#modalVerCategorias').modal('hide');
        },
    });

}