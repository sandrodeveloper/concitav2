$(document).ready(function() {
    citasPendientes();
});

var notificacion = "";

function citasPendientes() {
    var role = localStorage.getItem("role");
    if(role === 'provider'){
        var user_id = localStorage.getItem("id");
        var token = localStorage.getItem('token');
        $.ajax({
            url: `https://www.concita.com.mx:3000/api/v1/providerapptgroup/${user_id}?status=accepted`,
            headers: { 'Authorization': token },
            data: {},
            type: 'GET',
            success: function(respuesta) {
                
                var contadorCitasPendientes = 0;

                respuesta.data.forEach(citaPendiente => {
                    contadorCitasPendientes +=citaPendiente.count
                });
                var html =
                        `<a style="text-decoration:none" href="agenda-proveedor.html">
                            <div class="row welcomeTitle dangerContainer cursorPointer">
                                <div class="container">Tienes (${contadorCitasPendientes}) citas pendientes</div>
                            </div>
                        </a>`;

                $(".content:first").prepend(html);
            },
            error: function(er) {
            }
        });
    }
}