function eventoCrear() {
    var horarioValido = false;
    var exitos = 0
    var errores = 0
    var arregloDePeticiones = []
    var token = localStorage.getItem('token');
    
    $('[name="horarios"]').each(function() {
        if(this.checked){
            horarioValido =true;
            var value = parseInt(this.value);
            let datos = {
                name: 'Bloqueo de horario',
                type: 'other',
                description: 'Bloqueo de horario',
                from_date: new Date(moment($('#inicio').val()).add(value, 'hours')),
                due_date: new Date(moment($('#inicio').val()).add(value+1, 'hours'))
            }
            arregloDePeticiones.push(datos);
        }
        
    });
    if (new Date($('#inicio').val()) == "Invalid Date") {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Seleccione fecha.'
        })
        return;
    }
    if (!horarioValido) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Seleccione una hora.'
        })
    } else{
        arregloDePeticiones.forEach((element,index) => {
            
            $.ajax({
                url: 'https://www.concita.com.mx:3000/api/v1/event',
                headers: { 'Authorization': token },
                data: element,
                type: 'POST',
                success: function(respuesta) {
                    exitos++;
                    if(index == (arregloDePeticiones.length-1)){
                        if (exitos>0) {
                            setTimeout(function() {
                                location.href = "agenda-proveedor.html";
                            }, 2000);
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito',
                                text: 'Bloqueo creado correctamente.'
                            });
                        }else{
                            if(errores>0){
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Ya existe un evento en las fechas indicadas'
                                })
                            }
                        }
                    }
                },
                error: function(er) {
                    errores++;
                    if(index == (arregloDePeticiones.length-1)){
                        if (exitos>0) {
                            setTimeout(function() {
                                location.href = "agenda-proveedor.html";
                            }, 2000);
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito',
                                text: 'Bloqueo creado correctamente.'
                            });
                        }else{
                            if(errores>0){
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Ya existe un evento en las fechas indicadas'
                                })
                            }
                        }
                    }
                }
            });
        });
    }
}

function citasPorFecha(fecha){
    
    var idUser = localStorage.getItem('id');
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/providerappointments/' + idUser,
        type: 'GET',
        success: function (respuesta) {
            var events = _.filter(respuesta.data, function(item) {
                return moment(item.from_date) > moment(fecha) && moment(item.from_date) < moment(fecha).add(1, 'days')
            })
            var eventosHtml = ""
            events.forEach(element => {
                if(element.client === null || element.client === "" || element.client === undefined){
                    element.client = {
                    data: JSON.parse(localStorage.getItem('userData'))
                  }
                  element.client.data.email = element.client.data.emails[0]
                  element.client.data.full_name = element.client.data.name
                }
                var html =  `<div class="d-cita-pro ng-scope opacity">
                                <div class="d-title ng-scope">
                                    Cita ACEPTADA
                                </div>
                                <div class="d-info">
                                    <div class="clearfix d-img-name">
                                        <img src="https://www.concita.com.mx:3000/api/v1/image?id=${element.client.data.picture}"
                                            onerror="this.src='img/user-default.png'" alt="imgProfileCard"
                                            class="imgProfileCard" crossorigin="*">
                                        <p class="t1">Solicitado por</p>
                                        <p class="t2 ng-binding">${element.client.data.full_name}</p>
                                    </div>
                                    <div class="d-datos">
                                        <p class="t1">Correo electrónico</p>
                                        <p class="t2 ng-binding">${element.client.data.email}</p>
                                        <p class="t1">Teléfono</p>
                                        <p class="t2 ng-binding">${element.client.data.phone}</p>
                                        <p class="t1">Fecha solicitada</p>
                                        <p class="t2 ng-binding">
                                            ${moment(element.from_date).format('YYYY/MM/DD hh:mm')} hrs
                                        </p>
                                    </div>
                                    <div class="d-button">
                                    </div>
                                </div>
                            </div>`;
                eventosHtml+=html;
            });
            $("#clientes").empty();
            $("#clientes").append(eventosHtml);
        },
    });
}