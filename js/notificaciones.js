$(document).ready(function() {
    notoficaciones();
    getData();
});

var notificacion = "";

function notoficaciones() {

    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/notifications',
        type: 'GET',
        success: function(respuesta) {
            

            respuesta.data.forEach(function(item) {

                var html =
                    `<div class="card" style="width: 300px;min-height: auto;paddibottom: 20px; padding-bottom: 40px;">
                        <div class="card-body ">
                            <h5 class="card-title binding ">${item.title}</h5>
                            <h6 class="card-subtitle mb-2 text-muted binding ">${item.registration_date}</h6>
                            <p class="card-text binding ">${item.body}</p>
                        </div>
                    </div>`;

                notificacion += html;
            });

            $("#notitifaciones").append(notificacion);

        },
    });
}

function getData() {
    var nombre = localStorage.getItem("usuario");

    $('#headerNombre').text(nombre);
}

function cerrarSesion() {
    localStorage.clear();
    location.href = "index.html"
}