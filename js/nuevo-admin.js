function crearAdmin(){
    var name = $("#name").val();
    var email = $("#email").val();
    var password = $("#password").val();
    if(name === null || name === "" || name===undefined){
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Por ingrese nombre.",
        });
        return;
    }
    if(email === null || email === "" || email===undefined){
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Por ingrese correo.",
        });
        return;
    }
    if(password === null || password === "" || password===undefined){
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Por ingrese contraseña.",
        });
        return;
    }
    let datos = {
        name:name,
        email:email,
        password:md5(password)
    };
    $.ajax({
        url: "https://www.concita.com.mx:3000/api/v1/admin",
        data: datos,
        type: "POST",
        success: function (respuesta) {
          
          Swal.fire({
            icon: "success",
            title: "Éxito",
            text: "Administrador creado correctamente",
            showCancelButton: false,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Excelente!",
          }).then((result) => {
            location.href = "lista-admins.html";
          });
        },
        error: function (er) {
          
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Por favor verifique los campos.",
          });
        },
      });
}