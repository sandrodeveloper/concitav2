var modal = "";

$(document).ready(function() {
    pintarCorrero();
});

function correo() {
    if ($("#email").val() == null || $("#email").val() == undefined || $("#email").val() == "") {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Ingresar un correo valido'
        })
    } else {
        let datos = {
            email: $("#email").val(),
        }
        $.ajax({
            url: 'https://www.concita.com.mx:3000/api/v1/checkRoleUser',
            data: datos,
            type: 'POST',
            success: function(respuesta) {
                

                if (respuesta.data.roles == null) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'El correo no existe'
                    })
                } else {

                    if (respuesta.data.roles[0] == "client" && respuesta.data.roles[1] == 'provider') {
                        
                        var html =
                            `<div class="modal-content" id="myModal" tabindex="-1">
                                <div class="jconfirm jconfirm-light jconfirm-open">
                                    <div class="jconfirm-bg" style="transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 0.55, 0.19, 1);"></div>
                                    <div class="jconfirm-scrollpane">
                                        <div class="jconfirm-row">
                                            <div class="jconfirm-cell">
                                                <div class="jconfirm-holder" style="padding-top: 40px; padding-bottom: 40px;">
                                                    <div class="jc-bs3-container container">
                                                        <div class="jc-bs3-row row justify-content-md-center justify-content-sm-center justify-content-xs-center justify-content-lg-center">
                                                            <div class="jconfirm-box-container jconfirm-animated col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 jconfirm-no-transition" style="transform: translate(0px, 0px); transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 0.55, 0.19, 1);">
                                                                <div class="jconfirm-box jconfirm-hilight-shake jconfirm-type- jconfirm-type-animated" role="dialog" aria-labelledby="jconfirm-box57078" tabindex="-1" style="transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 0.55, 0.19, 1); transition-property: all, margin;">
                                                                    <div class="jconfirm-closeIcon" style="display: none;">×</div>
                                                                    <div class="jconfirm-title-c"><span class="jconfirm-icon-c"><i class="fa fa-exclamation-triangle"></i></span><span class="jconfirm-title">Atención</span></div>
                                                                    <div class="jconfirm-content-pane no-scroll" style="transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.36, 0.55, 0.19, 1); max-height: 728.844px;">
                                                                        <div class="jconfirm-content" id="jconfirm-box57078">
                                                                            <div>Su correo está vinculado a más de un perfil
                                                                                <h5>¿Cuál desea utilizar?</h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="jconfirm-buttons"><button type="button" class="btn btn-default" id="usuarioBoton">Usuario</button><button type="button" class="btn btn-default" id="provedorBoton">Proveedor</button><a href="login.html"><button type="button" class="btn btn-red">Cancelar</button></a></div>
                                                                    <div class="jconfirm-clear"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;

                        modal += html;

                        $("#creacionModal").append(modal);

                        document.getElementById("usuarioBoton").addEventListener("click", function(event) {

                            let datosEntradaUsuario = {
                                email: $("#email").val(),
                                role: "client"
                            }
                            $.ajax({
                                url: 'https://www.concita.com.mx:3000/api/v1/forgotPassword',
                                data: datosEntradaUsuario,
                                type: 'POST',
                                success: function(respuesta) {
                                    
                                    localStorage.setItem('correoR', $("#email").val());
                                    location.href = "correoRecuperar.html";
                                },
                            });
                        });

                        document.getElementById("provedorBoton").addEventListener("click", function(event) {

                            let datosEntradaProveedor = {
                                email: $("#email").val(),
                                role: "provider"
                            }
                            $.ajax({
                                url: 'https://www.concita.com.mx:3000/api/v1/forgotPassword',
                                data: datosEntradaProveedor,
                                type: 'POST',
                                success: function(respuesta) {
                                    
                                    localStorage.setItem('correoR', $("#email").val());
                                    location.href = "correoRecuperar.html";
                                },
                            });
                        });

                    } else {
                        //location.href = "correoRecuperar.html";
                    }

                }


            },
            error: function(er) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Error en el servidor'
                })
            }
        });
    }
}

function pintarCorrero() {
    $('#pintarCorreo').text(localStorage.getItem('correoR'));
}

function codigo() {
    var valor1 = $('#inta').val();
    var valor2 = $('#intb').val();
    var valor3 = $('#intc').val();
    var valor4 = $('#intd').val();

    var valor0 = valor1 + valor2 + valor3 + valor4;
    

    if (valor1 == "" || valor2 == "" || valor3 == "" || valor4 == "") {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Llenar todos los campos'
        })
    } else {
        let datosCodigo = {
            code: valor0
        }
        $.ajax({
            url: 'https://www.concita.com.mx:3000/api/v1/confirmCode',
            data: datosCodigo,
            type: 'POST',
            success: function(respuesta) {
                
                localStorage.setItem('code', valor0);
                location.href = "cambioContrasena.html";
            },
            error: function(er) {
                if (er.responseJSON.message == "bad.code.expired") {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Codigo incorrecto.'
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Error de servidor.'
                    });
                }

            }
        });

    }

}

function confirmar() {

    if ($('#password1').val() == "" || $('#password2').val() == "") {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Llenar todos los campos.'
        });
    } else {
        if ($('#password1').val() != $('#password2').val()) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Las contraseñas no coinciden.'
            });
        } else {
            var hash = md5($("#password1").val());

            let datosPass = {
                password: hash,
                code: localStorage.getItem('code')
            }
            $.ajax({
                url: 'https://www.concita.com.mx:3000/api/v1/resetPassword',
                data: datosPass,
                type: 'POST',
                success: function(respuesta) {
                    
                    setTimeout(function() {
                        location.href = "index.html"
                        localStorage.clear('code');
                    }, 2000);
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'La contraseña se actualizo correctamente',
                        showConfirmButton: false,
                        timer: 1500
                    });
                },
                error: function(er) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Error en el servidor.'
                    });
                }
            });
        }
    }
}