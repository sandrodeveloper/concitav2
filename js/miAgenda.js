var estado = "";
let currentDate;
let nextDate;
let prevDate;
let dates = [];

$(document).ready(function() {
    moment.lang('es', {
        months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
        monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
        weekdays: 'Domingo_Lunes_Martes_Miércoles_Jueves_Viernes_Sabado'.split('_'),
        weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
        weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
      }
    );
    currentDate = moment();
    nextDate = moment().add(1, 'month');
    prevDate = moment().subtract(1, 'month');
    getMisCitas();
    let nextMonth = nextDate.format('MMMM YYYY');
    let prevMonth = prevDate.format('MMMM YYYY');
    $('#prevMonth').text(prevMonth);
    $('#nextMonth').text(nextMonth);

});

function getMisCitas() {
    var userid = localStorage.getItem('id');
    let currentMonth = moment().format('MMMM YYYY');
    $('#currentMonth').text(currentMonth);
    $.ajax({
        url: 'https://www.concita.com.mx:3000/api/v1/appointment/' + userid,
        type: 'GET',
        success: function(respuesta) {
            
            dates = groupByMonth(respuesta.data);
            showServicesByMonth(currentMonth);

        },
    });
}
function compare(a, b) {
    const bandA = a.from_date;
    const bandB = b.from_date;
  
    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison;
  }
function showServicesByMonth(month = '') {
    let result = dates.filter(obj => {
        return obj.date === month
    });
    $("#citass").html("");
    let agendas = "";
    
    if (result.length > 0) {
        result[0].services.sort(compare);
        result[0].services.forEach(function(item) {
            var format = 'YYYY-MM-DDTHH:mm:ss ZZ';
            from_date=moment(item.from_date,format).tz("America/Mexico_City").format(format);
            due_date=moment(item.due_date,format).tz("America/Mexico_City").format(format);

            if (item.status == "accepted") {
                estado = "ACEPTADO";
            } else {
                estado = "RECHAZADO";
            }

            var html =
                `<div class="d-item-citas timeline-item" ng-class="{'timeline-item':true, 'opacity':vm.checkIfIsOpacity(_item.from_date)}">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-4" style="margin-left: 1rem;">
                        <p class="t1 ng-binding">${moment(item.from_date).format('dddd DD')}</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="clearfix d-img-name d-text-cita ng-scope" ng-if="mn.isClient()">
                            <img class="rounded" ng-src="https://www.concita.com.mx:3000/api/v1/image?id=${item.client.data.picture}" onerror="this.src='img/user-default.png'" crossorigin="*" src="img/user-default.png">
                            <p class="t1 ng-binding"> ${item.provider.data.name}</p>
                            <p class="t2">Estado: <b class="accepted">${estado}</b></p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="d-text-cita ng-scope" ng-if="mn.isClient()">
                            <p class="t1">Teléfono</p>
                            <p class="t2 ng-binding">${item.provider.data.phone}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                    </div>
                    <div class="col-lg-4 col-md-4">
                    </div>
                    <div class="col-lg-4 col-md-4">
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="d-text-cita">
                            <p class="t1">Nombre del servicio</p>
                            <p class="t2 ng-binding">${item.service.name}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="d-text-cita">
                            <p class="t1">Ubicación</p>
                            <p class="t2 ng-binding">${item.provider.data.address}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="d-text-cita">
                            <p class="t1">Inicio</p>
                            <p class="t2 ng-binding">${from_date.substr(11, 5)}</p>
                        </div>
                    </div>
                    <div class="col-md-offset-8 col-lg-offset-8 col-lg-4 col-md-4">
                        <div class="d-text-cita">
                            <p class="t1">Fin</p>
                            <p class="t2 ng-binding">${due_date.substr(11, 5)}</p>
                        </div>
                    </div>
                </div>
            </div>`;

            agendas += html;
        });

        $("#citass").append(agendas);
    }
}

function groupByMonth(dates = []) {
    let data = dates;
    // this gives an object with dates as keys
    const groups = data.reduce((groups, service) => {
    const date = moment(service.from_date).format('MMMM YYYY');
    if (!groups[date]) {
        groups[date] = [];
    }
    groups[date].push(service);
    return groups;
    }, {});
    
    // Edit: to add it in the array format instead
    const groupArrays = Object.keys(groups).map((date) => {
    return {
        date,
        services: groups[date]
    };
    });
    
    return groupArrays;
}

function prevMonth() {
    nextDate = currentDate;
    let nextMonth = currentDate.format('MMMM YYYY');
    $('#nextMonth').text(nextMonth);

    currentDate = moment(prevDate);
    let currentMonth = prevDate.format('MMMM YYYY');
    $('#currentMonth').text(currentMonth);
    
    prevDate = prevDate.subtract(1, 'month');
    let prevMonth = prevDate.format('MMMM YYYY');
    $('#prevMonth').text(prevMonth);

    showServicesByMonth(currentMonth);
}

function nextMonth() {
    prevDate = moment(currentDate);
    let prevMonth = prevDate.format('MMMM YYYY');
    $('#prevMonth').text(prevMonth);

    currentDate = moment(nextDate);
    let currentMonth = currentDate.format('MMMM YYYY');
    $('#currentMonth').text(currentMonth);
    
    nextDate = nextDate.add(1, 'month');
    let nextMonth = nextDate.format('MMMM YYYY');
    $('#nextMonth').text(nextMonth);

    showServicesByMonth(currentMonth);
}